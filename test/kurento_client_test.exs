defmodule KurentoClientTest do
  use ExUnit.Case

  alias Kurento.Remote.{MediaPipeline, ServerManager, PassThrough}
  alias Kurento.RPCSocket.Subscription

  defp via(name) do
    {:via, Registry, {Kurento.Registry, name}}
  end

  setup do
    client_name = "test"
    start_supervised!({Kurento.Client, client_name: client_name})
    %{table: table} = :sys.get_state(via(client_name))
    {:ok, client: client_name, table: table}
  end

  test "ping", %{client: client} do
    assert {:ok, "pong"} = Kurento.Client.ping(client)
  end

  test "reconnects on failed connection", %{client: client} do
    %{conn: conn} = :sys.get_state(via(client))
    {:ok, session_id} = GenServer.call(via(client), :get_session_id)

    # Kurento won't let us reconnect to a session without resources
    {:ok, _pipe} = MediaPipeline.create(client)

    :gun.close(conn)
    # Give it a moment for the failure to propogate
    Process.sleep(10)

    assert {:ok, ^session_id} = GenServer.call(via(client), :get_session_id)
  end

  test "getting a property works", %{client: client} do
    {:ok, pipe} = MediaPipeline.create(client)
    id = pipe.id

    try do
      {:ok, name} = MediaPipeline.get_name(pipe)

      assert ^id = name
    after
      :ok = MediaPipeline.release(pipe)
    end
  end

  test "setting a property works", %{client: client} do
    {:ok, pipe} = MediaPipeline.create(client)

    try do
      :ok = MediaPipeline.set_name(pipe, "hello")
      {:ok, name} = MediaPipeline.get_name(pipe)

      assert "hello" = name
    after
      :ok = MediaPipeline.release(pipe)
    end
  end

  test "invoking a method works", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    assert :ok = ServerManager.add_tag(manager, "mytag", "myval")
    assert {:ok, "myval"} = ServerManager.get_tag(manager, "mytag")
  end

  test "subscribing to an event works", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    assert :ok = ServerManager.subscribe_object_created(manager)

    {:ok, pipe} = MediaPipeline.create(client)

    try do
      assert_receive {:object_created, %Kurento.Event.ObjectCreated{}}
    after
      MediaPipeline.release(pipe)
    end
  end

  test "being the second subscriber works", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)
    parent = self()

    task =
      Task.async(fn ->
        :ok = ServerManager.subscribe_object_created(manager)
        send(parent, :subscribed)

        receive do
          {:object_created, event} -> event
        end
      end)

    # make sure we're the second
    :ok =
      receive do
        :subscribed -> :ok
      after
        :timer.seconds(1) -> :error
      end

    :ok = ServerManager.subscribe_object_created(manager)

    {:ok, pipe} = MediaPipeline.create(client)

    try do
      assert_receive {:object_created, %Kurento.Event.ObjectCreated{}}
      assert %Kurento.Event.ObjectCreated{} = Task.await(task)
    after
      MediaPipeline.release(pipe)
    end
  end

  test "unsubscribing works", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_object_created(manager)

    :ok = ServerManager.unsubscribe_object_created(manager)

    {:ok, pipe} = MediaPipeline.create(client)

    try do
      receive do
        {:object_created, _} -> assert(false, "recieved object created event")
      after
        100 -> assert true
      end
    after
      MediaPipeline.release(pipe)
    end
  end

  test "the test spy works, detects events that shouldn't be recieved", %{
    client: client,
    table: table
  } do
    :ok = GenServer.call(via(client), :set_spy)
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_object_created(manager)
    event = {manager.id, "ObjectCreated"}

    # Remove knowledge of the subscription
    Subscription.remove_subscription(table, event)

    {:ok, pipe} = MediaPipeline.create(client)
    MediaPipeline.release(pipe)

    receive do
      {:unsubscribed_event, _} -> assert true
    after
      1000 -> assert false, "Client should've recieved an unregonized event"
    end
  end

  test "being the last to unsubscribe unsubscribes the client", %{
    client: client,
    table: table
  } do
    :ok = GenServer.call(via(client), :set_spy)
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_object_created(manager)
    event = {manager.id, "ObjectCreated"}

    sub = Subscription.get_subscription(table, event)

    assert MapSet.member?(sub.pids, self()),
           "subscriptions don't work as expected, test needs modified"

    :ok = ServerManager.unsubscribe_object_created(manager)
    sub = Subscription.get_subscription(table, event)
    assert is_nil(sub)

    {:ok, pipe} = MediaPipeline.create(client)
    MediaPipeline.release(pipe)

    receive do
      {:unsubscribed_event, _} -> assert false, "client recieved event"
    after
      100 -> assert true
    end
  end

  test "returns error with bad object", %{client: client} do
    pipe = %MediaPipeline{id: "hello", client: client}

    {:error, %{code: _, message: _}} = MediaPipeline.set_name(pipe, "hello")
    {:error, %{code: _, message: _}} = MediaPipeline.get_name(pipe)
    {:error, %{code: _, message: _}} = MediaPipeline.add_tag(pipe, "mytag", "myval")
  end

  test "releasing an object removes it's subscriptions", %{
    client: client,
    table: table
  } do
    {:ok, pipe} = MediaPipeline.create(client)

    MediaPipeline.subscribe_error(pipe)

    MediaPipeline.release(pipe)

    event = {pipe.id, "Error"}

    sub = Subscription.get_subscription(table, event)

    assert is_nil(sub)
  end

  test "a child's subscriptions are removed when parent is released", %{
    client: client,
    table: table
  } do
    :ok = GenServer.call(via(client), :set_spy)
    {:ok, pipe} = MediaPipeline.create(client)
    {:ok, child} = PassThrough.create(pipe)

    MediaPipeline.subscribe_error(pipe)
    PassThrough.subscribe_error(child)
    MediaPipeline.release(pipe)

    pipe_event = {pipe.id, "Error"}
    pipe_subs = Subscription.get_subscription(table, pipe_event)
    assert is_nil(pipe_subs)

    child_event = {child.id, "Error"}
    child_subs = Subscription.get_subscription(table, child_event)
    assert is_nil(child_subs)

    receive do
      {:unsubscribed_event, _} -> assert false, "client recieved event"
    after
      100 -> assert true
    end
  end

  test "A PID's subscriptions are removed when it exits", %{
    client: client,
    table: table
  } do
    manager = Kurento.Client.get_server_manager(client)

    ServerManager.subscribe_object_created(manager)

    child =
      spawn(fn ->
        ServerManager.subscribe_object_created(manager)
      end)

    ref = Process.monitor(child)

    receive do
      # give it a moment for the client to recieve and act on the notification
      {:DOWN, ^ref, :process, _object, _reason} -> Process.sleep(10)
    end

    event = {manager.id, "ObjectCreated"}
    sub = Subscription.get_subscription(table, event)

    assert !MapSet.member?(sub.pids, child)
  end

  test "If the last listening PID dies, the client is unsubscribed", %{
    client: client,
    table: table
  } do
    :ok = GenServer.call(via(client), :set_spy)

    manager = Kurento.Client.get_server_manager(client)

    child =
      spawn(fn ->
        ServerManager.subscribe_object_created(manager)
      end)

    ref = Process.monitor(child)

    receive do
      # give it a moment for the client to recieve and act on the notification
      {:DOWN, ^ref, :process, _object, _reason} -> Process.sleep(10)
    end

    event = {manager.id, "ObjectCreated"}
    sub = Subscription.get_subscription(table, event)

    assert is_nil(sub)

    {:ok, pipe} = MediaPipeline.create(client)
    MediaPipeline.release(pipe)

    receive do
      {:unsubscribed_event, _} -> assert false, "client recieved event"
    after
      100 -> assert true
    end
  end

  test "subscribe_all subscribes to all available events", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_all(manager)

    {:ok, pipe} = MediaPipeline.create(client)
    :ok = MediaPipeline.release(pipe)

    assert_receive({:object_created, _})
    assert_receive({:object_destroyed, _})
  end

  test "unsubcribing from all, when not subscribed to all is not an error", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_object_created(manager)
    assert(:ok = ServerManager.unsubscribe_all(manager))
  end

  test "subscribing multiple times does not result in multiple notifications", %{client: client} do
    manager = Kurento.Client.get_server_manager(client)

    :ok = ServerManager.subscribe_object_created(manager)
    :ok = ServerManager.subscribe_object_created(manager)

    {:ok, pipe} = MediaPipeline.create(client)
    :ok = MediaPipeline.release(pipe)

    assert_receive({:object_created, _})

    receive do
      {:object_created, _} -> assert(false, "received duplicate notification")
    after
      100 -> assert true
    end
  end
end
