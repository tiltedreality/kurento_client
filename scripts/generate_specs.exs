defmodule CodeGen do
  require EEx

  defmodule Struct do
    require EEx

    EEx.function_from_file(:def, :render_from_prop_map, "templates/struct/from_prop_map.ex", [
      :prop
    ])

    EEx.function_from_file(:def, :render_to_prop_map, "templates/struct/to_prop_map.ex", [
      :prop
    ])

    def document_prop(%{"name" => name, "doc" => doc, "type" => type}, types) do
      t = types[type]
      doc = CodeGen.format_doc(doc)

      mod =
        if t do
          CodeGen.module_for(types[type])
        else
          type
        end

      "* `#{name}:` `#{mod}` - #{doc}"
    end

    def document_props(doc, names, props, types) do
      [name | names] = names

      prop_docs = Enum.map(props[name], &document_prop(&1, types)) |> Enum.join("\n")

      doc =
        if prop_docs !== "" do
          """
          #{doc}

          #{prop_docs}
          """
        else
          doc
        end

      if !Enum.empty?(names) do
        document_props(doc, names, props, types)
      else
        doc
      end
    end

    def document_props(names, props, types) do
      hier_doc = Enum.join(names, " > ")
      document_props(hier_doc, names, props, types)
    end

    def gather_props(names, props, %{"name" => name, "properties" => properties} = type, types) do
      extends = type["extends"]
      names = [name | names]

      props = Map.put(props, name, properties)

      if(extends) do
        gather_props(names, props, types[extends], types)
      else
        {names, props}
      end
    end
  end

  defmodule Remote do
    require EEx

    EEx.function_from_file(:def, :render_constructor, "templates/remote/constructor.ex", [
      :constructor
    ])

    EEx.function_from_file(:def, :render_method, "templates/remote/method.ex", [
      :method
    ])

    EEx.function_from_file(:def, :render_to_param_map, "templates/remote/to_param_map.ex", [
      :param
    ])

    def get_constructor(type, types) do
      if type["constructor"] do
        type["constructor"]
        |> Map.put("type", type["name"])
        |> Map.put("module", CodeGen.module_for(type))
        |> Map.update("params", [], fn params ->
          Enum.map(params, &add_param_attributes(&1, types))
        end)
        |> generate_optional_variants()
        |> Enum.map(&expand_method_doc(&1, types, true))
      else
        []
      end
    end

    def collect_methods(type, methods, types) do
      extends = type["extends"]
      type_methods = Map.get(type, "methods", []) |> Enum.map(&Map.put(&1, "method", "invoke"))
      props = Map.get(type, "properties", [])

      getters =
        Enum.map(props, fn prop ->
          name = prop["name"]

          prop
          |> Map.put("params", [])
          |> Map.put("return", %{"doc" => "", "type" => prop["type"]})
          |> Map.put("method", "invoke")
          |> Map.put("name", "get#{Macro.camelize(name)}")
        end)

      setters =
        Enum.filter(props, &(!(&1["readOnly"] || &1["final"])))
        |> Enum.map(fn prop ->
          name = prop["name"]

          prop
          |> Map.put("params", [%{"name" => name, "doc" => "", "type" => prop["type"]}])
          |> Map.put("name", "set#{Macro.camelize(name)}")
          |> Map.put("method", "invoke")
        end)

      events =
        Map.get(type, "events", [])
        |> Enum.flat_map(fn event ->
          [
            %{
              "params" => [],
              "method" => "subscribe",
              "name" => "subscribe#{event}",
              "doc" => "subscribe to :#{CodeGen.to_snake_case(event)} events",
              "eventType" => event
            },
            %{
              "params" => [],
              "method" => "unsubscribe",
              "name" => "unsubscribe#{event}",
              "doc" => "unsubscribe from :#{CodeGen.to_snake_case(event)} events",
              "eventType" => event
            }
          ]
        end)

      methods = Enum.concat([getters, setters, type_methods, methods, events])

      if extends do
        collect_methods(types[extends], methods, types)
      else
        methods
      end
    end

    def add_param_attributes(param, types) do
      type = types[param["type"]]
      defaultValue = param["defaultValue"]
      optional? = param["optional"]

      param = param
      |> CodeGen.add_module(types)
      |> Map.put("snake", CodeGen.to_snake_case(param["name"]))
      |> CodeGen.add_typespec()

      param =
        case {defaultValue, optional?, type} do
          {nil, _, _} ->
            param

          {_, true, _} ->
            # If the argument is optional, prefer not sending it to using the default
            Map.delete(param, "defaultValue")

          {val, _, nil} ->
            Map.put(param, "defaultValue", " \\\\ #{inspect(val)}")

          {val, _, %{"typeFormat" => "ENUM"}} ->
            Map.put(param, "defaultValue", " \\\\ :#{Macro.underscore(val)}")

          {_val, _, %{"typeFormat" => "REGISTER"}} ->
            Map.put(param, "defaultValue", " \\\\ %#{param["module"]}{}")

          _ ->
            raise "Unexpected defaultValue #{param}"
        end

      param
    end

    def generate_optional_variants(method) do
      opt_params = Enum.filter(method["params"], fn param -> param["optional"] end)
      req_params = Enum.filter(method["params"], fn param -> !param["optional"] end)
      cnt_opt = Enum.count(opt_params)

      Enum.map(0..cnt_opt, fn opts ->
        params = req_params ++ Enum.take(opt_params, opts)
        Map.put(method, "params", params)
      end)
    end

    def document_return(nil, _) do
      ""
    end

    def document_return(return, types) do
      return_module =
        if types[return["type"]] do
          CodeGen.module_for(types[return["type"]])
        else
          return["type"]
        end

      if return["doc"] && return["doc"] !== "" do
        "Returns: `#{return_module}` - #{return["doc"]}"
      else
        "Returns: `#{return_module}`"
      end
    end

    def document_param(param, types) do
      param_module =
        if types[param["type"]] do
          CodeGen.module_for(types[param["type"]])
        else
          param["type"]
        end

      if param["doc"] && param["doc"] !== "" do
        "* `#{param["name"]}`: `#{param_module}` - #{param["doc"]}"
      else
        "* `#{param["name"]}`: `#{param_module}`"
      end
    end

    def expand_method_doc(method, types, constructor \\ false) do
      doc = Map.get(method, "doc", "")
      params = Map.get(method, "params", [])

      kobject_param = %{
        "name" => method["module_snake"],
        "type" => method["module"],
        "doc" => "The `#{method["module"]}` to operate on."
      }

      client_param = %{
        "name" => "client",
        "type" => "Kurento.Client",
        "doc" => "The `Kurento.Client` to use"
      }

      params =
        cond do
          !constructor -> [kobject_param | params]
          Enum.empty?(params) -> [client_param | params]
          true -> params
        end

      params_doc =
        params
        |> Enum.map(&document_param(&1, types))
        |> Enum.join("\n")

      params_doc = "## Params\n\n#{params_doc}"

      returns_doc = document_return(method["return"], types)

      doc =
        [doc, params_doc, returns_doc]
        |> Enum.filter(&(!(&1 == "")))
        |> Enum.join("\n\n")

      Map.put(method, "doc", doc)
    end
  end

  def to_snake_case(str) do
    str |> Macro.underscore() |> String.replace(~r/r_t_c/, "rtc")
  end

  def enum_kw_list(values) do
    Enum.map(values, fn str ->
      atom = ":#{Macro.underscore(str)}"

      {atom, str}
    end)
  end

  def module_for(type) do
    name = type["name"]

    class =
      case type do
        %{"typeFormat" => "REGISTER", "event" => _} -> "Event"
        %{"typeFormat" => "REGISTER"} -> "Struct"
        %{"typeFormat" => "ENUM"} -> "Enum"
        %{"typeFormat" => "REMOTE"} -> "Remote"
      end

    "Kurento.#{class}.#{name}"
  end

  def format_doc(doc) do
    # The HTML in the documentation is a mess. I've spent hours trying to clean it to a point
    # that earmark is happy with it. Instead we try to convert it to a markdown that reasonably
    # conveys it's intent. This function hurts my soul.
    if doc do
      doc
      # remove newlines and indentation
      |> String.replace(~r/\n\s*/, " ")
      |> String.replace("<b>", "**")
      |> String.replace("</b>", "**")
      |> String.replace("<blockquote>", ">")
      |> String.replace("</blockquote>", "\n\n")
      |> String.replace("<code>", "`")
      |> String.replace("</code>", "`")
      |> String.replace("<dd>", "\n\n")
      |> String.replace("</dd>", "\n\n")
      |> String.replace("<dl>", "\n\n")
      |> String.replace("</dl>", "\n\n")
      |> String.replace("<dt>", "\n\n")
      |> String.replace("</dt>", "\n\n")
      |> String.replace("<em>", "*")
      |> String.replace("</em>", "*")
      |> String.replace("<h2>", "##")
      |> String.replace("</h2>", "")
      |> String.replace("<h4>", "####")
      |> String.replace("</h4>", "")
      |> String.replace("<i>", "*")
      |> String.replace("</i>", "*")
      |> String.replace("<li>", "* ")
      |> String.replace("</li>", "\n")
      |> String.replace("<ol>", "\n\n")
      |> String.replace("</ol>", "\n\n")
      |> String.replace("<p>", "\n\n")
      |> String.replace("</p>", "\n\n")
      |> String.replace("<stron>", "**")
      |> String.replace("</stron>", "**")
      |> String.replace("<strong>", "**")
      |> String.replace("</strong>", "**")
      |> String.replace("<ul>", "\n\n")
      |> String.replace("</ul>", "\n\n")
      # Condense excessive blank lines
      |> String.replace(~r/\n\s*\n\s*[\n|\s]*/, "\n\n")
      # Cleanup any blank lines at the end of the documentation
      |> String.replace(~r/[\n|\s]*\z/, "")
    else
      ""
    end
  end

  EEx.function_from_file(:def, :render_enum, "templates/enum.ex", [:module_name, :doc, :values])

  EEx.function_from_file(:def, :render_struct, "templates/struct.ex", [
    :module_name,
    :doc,
    :snake_name,
    :props
  ])

  EEx.function_from_file(:def, :render_remote, "templates/remote.ex", [
    :module_name,
    :doc,
    :snake_name,
    :constructors,
    :methods
  ])

  EEx.function_from_file(:def, :render_event_parser, "templates/event_parser.ex", [
    :events
  ])

  def typespec_for("int64") do
    "integer()"
  end

  def typespec_for("int") do
    "integer()"
  end

  def typespec_for("double") do
    "float()"
  end

  def typespec_for("float") do
    "float()"
  end

  def typespec_for("boolean") do
    "boolean()"
  end

  def typespec_for(base_type) do
    "#{base_type}.t()"
  end

  def add_typespec(term) do
    module = term["module"]
    base_type = term["base_type"]
    is_array? = term["is_array?"]

    spec = case [module, is_array?] do
      [nil, false] -> typespec_for(base_type)
      [nil, true] -> "[#{typespec_for(base_type)}]"
      [_, false] -> "#{module}.t()"
      [_, true] -> "[#{module}.t()]"
    end

    Map.put(term, "spec", spec)
  end

  def add_module(term, types) do
    type = term["type"]
    is_array? = String.ends_with?(type, "[]")
    base_type = type
    |> String.replace("[]", "")
    |> String.replace("<>", "")

    type = types[base_type]

    if type do
      module = module_for(type)

      term
      |> Map.put("module", module)
      |> Map.put("base_type", base_type)
      |> Map.put("is_array?", is_array?)
    else
      term
      |> Map.put("base_type", base_type)
      |> Map.put("is_array?", is_array?)

    end
  end

  def render_type(%{"typeFormat" => "ENUM"} = type, _types) do
    snake = to_snake_case(type["name"])
    filename = "lib/gen/enums/#{snake}.ex"
    module_name = module_for(type)
    values = enum_kw_list(type["values"])
    doc = format_doc(type["doc"])

    code = render_enum(module_name, doc, values) |> Code.format_string!()

    File.write!(filename, code)
  end

  def render_type(%{"typeFormat" => "REGISTER"} = type, types) do
    snake = to_snake_case(type["name"])

    dir =
      if type["event"] do
        "lib/gen/events"
      else
        "lib/gen/structs"
      end

    filename = "#{dir}/#{snake}.ex"
    module_name = module_for(type)

    doc = format_doc(type["doc"])
    {names, props} = Struct.gather_props([], %{}, type, types)
    prop_docs = Struct.document_props(names, props, types)

    props =
      Enum.flat_map(names, fn name ->
        props[name]
      end)
      |> Enum.map(&add_module(&1, types))
      |> Enum.map(fn prop -> Map.put(prop, "snake", to_snake_case(prop["name"])) end)
      |> Enum.map(fn prop -> Map.put(prop, "parent_snake", snake) end)
      |> Enum.map(&add_typespec/1)

    doc = """
    #{doc}

    #{prop_docs}
    """

    code = render_struct(module_name, doc, snake, props) |> Code.format_string!()

    File.write!(filename, code)
  end

  def render_type(%{"typeFormat" => "REMOTE"} = type, types) do
    snake = to_snake_case(type["name"])
    filename = "lib/gen/remote/#{snake}.ex"
    module_name = module_for(type)
    doc = format_doc(type["doc"])
    constructors = Remote.get_constructor(type, types)

    methods = Remote.collect_methods(type, [], types)

    methods =
      Enum.flat_map(methods, fn method ->
        method =
          if method["return"] do
            method
            |> Map.update!("return", &add_module(&1, types))
            |> Map.update!("return", &add_typespec/1)
          else
            method
          end

        method
        |> Map.update("params", [], fn params ->
          Enum.map(params, &Remote.add_param_attributes(&1, types))
        end)
        |> Map.put("module", module_name)
        |> Map.put("module_snake", snake)
        |> Map.update("doc", "", &format_doc/1)
        |> Remote.generate_optional_variants()
        |> Enum.map(&Remote.expand_method_doc(&1, types))
      end)

    code =
      render_remote(module_name, doc, snake, constructors, methods)
      |> Code.format_string!()

    File.write!(filename, code)
  end

  def render_type(type, _types) do
    IO.puts(type["typeFormat"])
  end

  def render_events(types) do
    events =
      types
      |> Enum.map(fn {_, type} -> type end)
      |> Enum.filter(fn type -> type["event"] end)

    code = render_event_parser(events) |> Code.format_string!()

    File.write!("lib/gen/event_parser.ex", code)
  end
end

spec_files = Path.wildcard("specs/*.kmd.json")

types =
  Enum.reduce(spec_files, %{}, fn spec_file, types ->
    specs =
      spec_file
      |> File.read!()
      |> Jason.decode!()

    complex = Map.get(specs, "complexTypes", [])
    events = Map.get(specs, "events", [])
    remote = Map.get(specs, "remoteClasses", [])

    add_type = &Map.put(&2, &1["name"], &1)
    types = Enum.reduce(complex, types, add_type)

    types =
      Enum.reduce(events, types, fn event, acc ->
        event = event |> Map.put("typeFormat", "REGISTER") |> Map.put("event", true)
        Map.put(acc, event["name"], event)
      end)

    Enum.reduce(remote, types, fn event, acc ->
      event = Map.put(event, "typeFormat", "REMOTE")
      Map.put(acc, event["name"], event)
    end)
  end)

type_names = Map.keys(types)

Enum.each(type_names, &CodeGen.render_type(types[&1], types))
CodeGen.render_events(types)
