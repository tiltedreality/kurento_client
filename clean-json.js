const dJSON = require('dirty-json');
const fs = require('fs');
const path = require('path');


var src = process.argv[2]
var dest = process.argv[3]

files = fs.readdirSync(src)

specs = files.filter(file => file.endsWith(".kmd.json"))

specs.forEach(spec => {
    src_path = path.join(src, spec)
    dest_path = path.join(dest, spec)

    console.log(src_path)

    dirty = fs.readFileSync(src_path).toString()
    parsed = dJSON.parse(dirty)
    clean = JSON.stringify(parsed, null, 4)

    fs.writeFileSync(dest_path, clean)
});