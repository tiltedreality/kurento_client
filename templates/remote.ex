<% subscribe_events = Enum.filter(methods, &(&1["method"] == "subscribe")) %>
<% unsubscribe_events = Enum.filter(methods, &(&1["method"] == "unsubscribe")) %>
<% events = Enum.map(subscribe_events, &(&1["eventType"])) %>

defmodule <%= module_name %> do
  @moduledoc """
  <%= doc %>
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  <%= for constructor <- constructors do %>
  <%= CodeGen.Remote.render_constructor(constructor) %>
  <% end %>

  <%= for method <- methods do %>
    <%= CodeGen.Remote.render_method(method) %>
  <% end %>

  @doc """
  Subscribe to all events from this `<%= module_name %>`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  <%= events |> Enum.map(&("* `Kurento.Event.#{&1}`")) |> Enum.join("\n") %>

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(<%= snake_name %>) do
    results = [
      <%= subscribe_events |> Enum.map(&("#{CodeGen.to_snake_case(&1["name"])}(#{snake_name})")) |> Enum.join(",") %>
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `<%= module_name %>`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  <%= events |> Enum.map(&("* `Kurento.Event.#{&1}`")) |> Enum.join("\n") %>

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(<%= snake_name %>) do
    results = [
      <%= unsubscribe_events |> Enum.map(&("#{CodeGen.to_snake_case(&1["name"])}(#{snake_name})")) |> Enum.join(",") %>
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %<%= module_name %>{client: client, id: param}
  end

  @doc false
  def to_param(<%= snake_name %>) do
    <%= snake_name %>.id
  end

  @doc "Release the Kurento Object"
  def release(<%= snake_name %>) do
    params = %{object: <%= module_name %>.to_param(<%= snake_name %>)}
    client = <%= snake_name %>.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end
