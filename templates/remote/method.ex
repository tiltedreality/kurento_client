<% method_snake = CodeGen.to_snake_case(method["name"]) %>
<% module_snake = method["module_snake"] %>
<% params_list = method["params"] |> Enum.map(fn param -> "#{param["snake"]}#{param["defaultValue"]}" end) %>
<% params_list = [module_snake | params_list] %>
<% params_list = Enum.join(params_list, ", ") %>
<% params_spec = method["params"] |> Enum.map(fn param -> param["spec"] end) %>
<% params_spec = ["#{method["module"]}.t()" | params_spec] %>
<% params_spec = Enum.join(params_spec, ", ") %>
<% return_spec = if method["return"] do "{:ok, #{method["return"]["spec"]}}" else ":ok" end %>
<% invoke_method = method["method"] %>

@spec <%= method_snake %>(<%= params_spec %>) :: <%= return_spec %> | {:error, Kurento.CallError.t()}
@doc """
<%= Map.get(method, "doc", "") %>
"""
def <%= method_snake %>(<%= params_list %>) do
  client = <%= module_snake %>.client

  <%= if method["eventType"] do %>
  params = %{
    object: <%= method["module"] %>.to_param(<%= module_snake %>),
    type: "<%= method["eventType"] %>"
  }
  <% else %>
  operation_params = %{
    <%= Enum.map(method["params"], &CodeGen.Remote.render_to_param_map/1) |> Enum.map(&String.trim/1) |> Enum.join(",\n") %>
  }

  params = %{
    object: <%= method["module"] %>.to_param(<%= module_snake %>),
    operation: "<%= method["name"] %>",
    operationParams: operation_params
  }
  <% end %>

  request = Kurento.RPCRequest.create("<%= invoke_method %>", params)
  result = Kurento.Client.invoke(client, request)

  case(result) do
    {:ok, value} ->
      _ = value
      <%= cond do %>
        <% method["return"] && method["return"]["module"] && method["return"]["is_array?"] -> %> {:ok, Enum.map(value["value"], &<%= method["return"]["module"] %>.from_param(client, &1))}
        <% method["return"] && method["return"]["module"] -> %>  {:ok, <%= method["return"]["module"] %>.from_param(client, value["value"])}
        <% method["return"] -> %> {:ok, value["value"]}
        <% true -> %> :ok
      <% end %>

    {:error, err} ->
      {:error, Kurento.CallError.from_map(err)}
  end
end
