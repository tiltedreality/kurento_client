<% needs_client? = Enum.empty?(constructor["params"]) %>
<% params_list = constructor["params"] |> Enum.map(fn param -> "#{param["snake"]}#{param["defaultValue"]}" end) %>
<% params_list = if needs_client? do ["client" | params_list] else params_list end %>
<% params_list = Enum.join(params_list, ", ") %>
<% params_spec = constructor["params"] |> Enum.map(fn param -> param["spec"] end) %>
<% params_spec = if needs_client? do ["Kurento.Client.client()" | params_spec] else params_spec end %>
<% params_spec = Enum.join(params_spec, ", ") %>
<% type = constructor["type"] %>
<% module = constructor["module"] %>

@spec create(<%= params_spec %>) :: {:ok, <%= constructor["module"] %>.t()} | {:error, Kurento.CallError.t()}
@doc """
<%= constructor["doc"] %>
"""
def create(<%= params_list %>) do
  constructor_params = %{
    <%= Enum.map(constructor["params"], &CodeGen.Remote.render_to_param_map/1) |> Enum.map(&String.trim/1) |> Enum.join(",\n") %>
  }
  <%= if !needs_client? do %>
  client = <%= List.first(constructor["params"])["snake"] %>.client
  <% end %>

  params = %{type: "<%= type %>", constructorParams: constructor_params, properties: %{}}
  request = Kurento.RPCRequest.create("create", params)
  result = Kurento.Client.invoke(client, request)

  case(result) do
    {:ok, value} ->
      {:ok, <%= module %>.from_param(client, value["value"])}

    {:error, err} ->
      {:error, Kurento.CallError.from_map(err)}
  end
end
