<% param_name = param["name"] %>
<% module = param["module"] %>
<% is_array? = param["is_array?"] %>
<%= cond do %>
  <% module && is_array? -> %> <%= "#{param_name}:" %> Enum.map(<%= param["snake"] %>, &<%= module %>.to_param(&1))
  <% module -> %> <%= "#{param_name}:" %> <%= module %>.to_param(<%= param["snake"] %>)
  <% true -> %> <%= "#{param_name}:" %> <%= param["snake"] %>
<% end %>
