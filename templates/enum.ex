<% values_list = values |> Enum.map(fn {a, _} -> a end) |> Enum.join(", ") %>
<% values_spec = values |> Enum.map(fn {a, _} -> a end) |> Enum.join("|") %>

defmodule <%= module_name %> do
  @moduledoc """
  <%= doc %>
  """

  @type t :: <%= values_spec %>

  <%= for {atom, _} <- values do %>
  @spec <%= String.trim_leading(atom, ":") %> :: <%= atom %>
  def <%= String.trim_leading(atom, ":") %>() do
    <%= atom %>
  end
  <% end %>

  @spec values :: [<%= module_name %>.t()]
  def values() do
    [ <%= values_list %> ]
  end

  <%= for {atom, str} <- values do %>
  @doc false
  def to_param(<%= atom %>) do
    "<%= str %>"
  end
  <% end %>

  <%= for {atom, str} <- values do %>
  @doc false
  def from_param(_client, "<%= str %>") do
    <%= atom %>
  end
  <% end %>
end
