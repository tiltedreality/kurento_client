<% parent_snake = prop["parent_snake"] %>
<% prop_name = prop["name"] %>
<% module = prop["module"] %>
<% is_array? = prop["is_array?"] %>
<%= cond do %>
  <% module && is_array? -> %> "<%= prop_name %>" => Enum.map(<%= parent_snake %>.<%= prop["snake"] %>, &<%= module %>.to_param(&1))
  <% module -> %> "<%= prop_name %>" => <%= module %>.to_param(<%= parent_snake %>.<%= prop["snake"] %>)
  <% true -> %> "<%= prop_name %>" => <%= parent_snake %>.<%= prop["snake"] %>
<% end %>
