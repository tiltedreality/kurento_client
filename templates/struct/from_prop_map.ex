<% prop_name = prop["name"] %>
<% module = prop["module"] %>
<% is_array? = prop["is_array?"] %>
<%= cond do %>
  <% module && is_array? -> %> <%= "#{prop["snake"]}:" %> Enum.map(params["<%= prop_name %>"], &<%= module %>.from_param(client, &1))
  <% module -> %> <%= "#{prop["snake"]}:" %> <%= module %>.from_param(client, params["<%= prop_name %>"])
  <% true -> %> <%= "#{prop["snake"]}:" %> params["<%= prop_name %>"]
<% end %>
