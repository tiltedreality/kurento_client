defmodule Kurento.EventParser do
  @moduledoc false

  <%= for event <- events do %>
  def parse_event("<%= event["name"] %>", client, param) do
    <%= CodeGen.module_for(event) %>.from_param(client, param)
  end
  <% end %>

  <%= for event <- events do %>
  def event_name("<%= event["name"] %>") do
    :<%= CodeGen.to_snake_case(event["name"]) %>
  end
  <% end %>
end
