<% properties_list = props |> Enum.map(fn prop -> ~s(:#{prop["snake"]}) end) |> Enum.join(", ") %>
defmodule <%= module_name %> do
  @moduledoc """
  <%= doc %>
  """
  defstruct [<%= properties_list %>]

  @type t :: %{
    optional(:__struct__) => atom(),
    <%= for %{"snake" => snake, "spec" => spec} <- props do %>
      <%= snake %>: <%= spec %>,
    <% end %>
  }

  @doc false
  def from_param(client, params) do
    _ = client
    %<%= module_name %>{
      <%= Enum.map(props, &CodeGen.Struct.render_from_prop_map/1) |> Enum.map(&String.trim/1) |> Enum.join(",\n")%>
    }
  end

  @doc false
  def to_param(<%= snake_name %>) do
    %{
      <%= Enum.map(props, &CodeGen.Struct.render_to_prop_map/1) |> Enum.map(&String.trim/1) |> Enum.join(",\n")%>
    }
  end
end
