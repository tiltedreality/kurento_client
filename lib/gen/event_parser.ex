defmodule Kurento.EventParser do
  @moduledoc false

  def parse_event("ElementDisconnected", client, param) do
    Kurento.Event.ElementDisconnected.from_param(client, param)
  end

  def parse_event("Error", client, param) do
    Kurento.Event.Error.from_param(client, param)
  end

  def parse_event("MediaTranscodingStateChange", client, param) do
    Kurento.Event.MediaTranscodingStateChange.from_param(client, param)
  end

  def parse_event("Media", client, param) do
    Kurento.Event.Media.from_param(client, param)
  end

  def parse_event("MediaSessionTerminated", client, param) do
    Kurento.Event.MediaSessionTerminated.from_param(client, param)
  end

  def parse_event("EndOfStream", client, param) do
    Kurento.Event.EndOfStream.from_param(client, param)
  end

  def parse_event("MediaSessionStarted", client, param) do
    Kurento.Event.MediaSessionStarted.from_param(client, param)
  end

  def parse_event("ObjectCreated", client, param) do
    Kurento.Event.ObjectCreated.from_param(client, param)
  end

  def parse_event("UriEndpointStateChanged", client, param) do
    Kurento.Event.UriEndpointStateChanged.from_param(client, param)
  end

  def parse_event("CodeFound", client, param) do
    Kurento.Event.CodeFound.from_param(client, param)
  end

  def parse_event("IceComponentStateChange", client, param) do
    Kurento.Event.IceComponentStateChange.from_param(client, param)
  end

  def parse_event("RaiseBase", client, param) do
    Kurento.Event.RaiseBase.from_param(client, param)
  end

  def parse_event("IceGatheringDone", client, param) do
    Kurento.Event.IceGatheringDone.from_param(client, param)
  end

  def parse_event("OnKeySoftLimit", client, param) do
    Kurento.Event.OnKeySoftLimit.from_param(client, param)
  end

  def parse_event("OnIceGatheringDone", client, param) do
    Kurento.Event.OnIceGatheringDone.from_param(client, param)
  end

  def parse_event("MediaStateChanged", client, param) do
    Kurento.Event.MediaStateChanged.from_param(client, param)
  end

  def parse_event("OnDataChannelClosed", client, param) do
    Kurento.Event.OnDataChannelClosed.from_param(client, param)
  end

  def parse_event("DataChannelClose", client, param) do
    Kurento.Event.DataChannelClose.from_param(client, param)
  end

  def parse_event("MediaFlowOutStateChange", client, param) do
    Kurento.Event.MediaFlowOutStateChange.from_param(client, param)
  end

  def parse_event("OnIceCandidate", client, param) do
    Kurento.Event.OnIceCandidate.from_param(client, param)
  end

  def parse_event("OnDataChannelOpened", client, param) do
    Kurento.Event.OnDataChannelOpened.from_param(client, param)
  end

  def parse_event("Paused", client, param) do
    Kurento.Event.Paused.from_param(client, param)
  end

  def parse_event("DataChannelOpen", client, param) do
    Kurento.Event.DataChannelOpen.from_param(client, param)
  end

  def parse_event("IceCandidateFound", client, param) do
    Kurento.Event.IceCandidateFound.from_param(client, param)
  end

  def parse_event("Stopped", client, param) do
    Kurento.Event.Stopped.from_param(client, param)
  end

  def parse_event("ElementConnected", client, param) do
    Kurento.Event.ElementConnected.from_param(client, param)
  end

  def parse_event("ConnectionStateChanged", client, param) do
    Kurento.Event.ConnectionStateChanged.from_param(client, param)
  end

  def parse_event("NewCandidatePairSelected", client, param) do
    Kurento.Event.NewCandidatePairSelected.from_param(client, param)
  end

  def parse_event("MediaFlowInStateChange", client, param) do
    Kurento.Event.MediaFlowInStateChange.from_param(client, param)
  end

  def parse_event("OnIceComponentStateChanged", client, param) do
    Kurento.Event.OnIceComponentStateChanged.from_param(client, param)
  end

  def parse_event("Recording", client, param) do
    Kurento.Event.Recording.from_param(client, param)
  end

  def parse_event("ObjectDestroyed", client, param) do
    Kurento.Event.ObjectDestroyed.from_param(client, param)
  end

  def event_name("ElementDisconnected") do
    :element_disconnected
  end

  def event_name("Error") do
    :error
  end

  def event_name("MediaTranscodingStateChange") do
    :media_transcoding_state_change
  end

  def event_name("Media") do
    :media
  end

  def event_name("MediaSessionTerminated") do
    :media_session_terminated
  end

  def event_name("EndOfStream") do
    :end_of_stream
  end

  def event_name("MediaSessionStarted") do
    :media_session_started
  end

  def event_name("ObjectCreated") do
    :object_created
  end

  def event_name("UriEndpointStateChanged") do
    :uri_endpoint_state_changed
  end

  def event_name("CodeFound") do
    :code_found
  end

  def event_name("IceComponentStateChange") do
    :ice_component_state_change
  end

  def event_name("RaiseBase") do
    :raise_base
  end

  def event_name("IceGatheringDone") do
    :ice_gathering_done
  end

  def event_name("OnKeySoftLimit") do
    :on_key_soft_limit
  end

  def event_name("OnIceGatheringDone") do
    :on_ice_gathering_done
  end

  def event_name("MediaStateChanged") do
    :media_state_changed
  end

  def event_name("OnDataChannelClosed") do
    :on_data_channel_closed
  end

  def event_name("DataChannelClose") do
    :data_channel_close
  end

  def event_name("MediaFlowOutStateChange") do
    :media_flow_out_state_change
  end

  def event_name("OnIceCandidate") do
    :on_ice_candidate
  end

  def event_name("OnDataChannelOpened") do
    :on_data_channel_opened
  end

  def event_name("Paused") do
    :paused
  end

  def event_name("DataChannelOpen") do
    :data_channel_open
  end

  def event_name("IceCandidateFound") do
    :ice_candidate_found
  end

  def event_name("Stopped") do
    :stopped
  end

  def event_name("ElementConnected") do
    :element_connected
  end

  def event_name("ConnectionStateChanged") do
    :connection_state_changed
  end

  def event_name("NewCandidatePairSelected") do
    :new_candidate_pair_selected
  end

  def event_name("MediaFlowInStateChange") do
    :media_flow_in_state_change
  end

  def event_name("OnIceComponentStateChanged") do
    :on_ice_component_state_changed
  end

  def event_name("Recording") do
    :recording
  end

  def event_name("ObjectDestroyed") do
    :object_destroyed
  end
end