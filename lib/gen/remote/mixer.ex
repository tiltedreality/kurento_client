defmodule Kurento.Remote.Mixer do
  @moduledoc """
  A :rom:cls:`Hub` that allows routing of video between arbitrary port pairs and mixing of audio among several ports
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.Mixer.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`Mixer` belonging to the given pipeline.

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the Mixer belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{type: "Mixer", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.Mixer.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.Mixer.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.Mixer.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.Mixer.t()) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `String`
  """
  def get_id(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.Mixer.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.Mixer.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.Mixer.t()) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `String`
  """
  def get_name(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.Mixer.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.Mixer.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `int`
  """
  def get_creation_time(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.Mixer.t(), String.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `name`: `String`
  """
  def set_name(mixer, name) do
    client = mixer.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.Mixer.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(mixer, send_tags_in_events) do
    client = mixer.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.Mixer.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(mixer, key, value) do
    client = mixer.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.Mixer.t(), String.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(mixer, key) do
    client = mixer.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.Mixer.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(mixer, key) do
    client = mixer.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.Mixer.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Mixer.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(mixer) do
    client = mixer.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Mixer.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(mixer, details) do
    client = mixer.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.Mixer.t(),
          Kurento.Enum.MediaType.t(),
          Kurento.Remote.HubPort.t(),
          Kurento.Remote.HubPort.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects each corresponding :rom:enum:`MediaType` of the given source port with the sink port.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `media`: `Kurento.Enum.MediaType` - The sort of media stream to be connected
  * `source`: `Kurento.Remote.HubPort` - Source port to be connected
  * `sink`: `Kurento.Remote.HubPort` - Sink port to be connected
  """
  def connect(mixer, media, source, sink) do
    client = mixer.client

    operation_params = %{
      media: Kurento.Enum.MediaType.to_param(media),
      source: Kurento.Remote.HubPort.to_param(source),
      sink: Kurento.Remote.HubPort.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.Mixer.t(),
          Kurento.Enum.MediaType.t(),
          Kurento.Remote.HubPort.t(),
          Kurento.Remote.HubPort.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disonnects each corresponding :rom:enum:`MediaType` of the given source port from the sink port.

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  * `media`: `Kurento.Enum.MediaType` - The sort of media stream to be disconnected
  * `source`: `Kurento.Remote.HubPort` - Audio source port to be disconnected
  * `sink`: `Kurento.Remote.HubPort` - Audio sink port to be disconnected
  """
  def disconnect(mixer, media, source, sink) do
    client = mixer.client

    operation_params = %{
      media: Kurento.Enum.MediaType.to_param(media),
      source: Kurento.Remote.HubPort.to_param(source),
      sink: Kurento.Remote.HubPort.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.Mixer.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  """
  def subscribe_error(mixer) do
    client = mixer.client

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.Mixer.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `mixer`: `Kurento.Remote.Mixer` - The `Kurento.Remote.Mixer` to operate on.
  """
  def unsubscribe_error(mixer) do
    client = mixer.client

    params = %{
      object: Kurento.Remote.Mixer.to_param(mixer),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.Mixer`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(mixer) do
    results = [
      subscribe_error(mixer)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.Mixer`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(mixer) do
    results = [
      unsubscribe_error(mixer)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.Mixer{client: client, id: param}
  end

  @doc false
  def to_param(mixer) do
    mixer.id
  end

  @doc "Release the Kurento Object"
  def release(mixer) do
    params = %{object: Kurento.Remote.Mixer.to_param(mixer)}
    client = mixer.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end