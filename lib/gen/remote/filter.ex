defmodule Kurento.Remote.Filter do
  @moduledoc """
  Base interface for all filters. 

  This is a certain type of :rom:cls:`MediaElement`, that processes media injected through its sinks, and delivers the outcome through its sources.
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec get_media_pipeline(Kurento.Remote.Filter.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.Filter.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.Filter.t()) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `String`
  """
  def get_id(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.Filter.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.Filter.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.Filter.t()) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `String`
  """
  def get_name(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.Filter.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.Filter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `int`
  """
  def get_creation_time(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.Filter.t(), String.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `name`: `String`
  """
  def set_name(filter, name) do
    client = filter.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.Filter.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(filter, send_tags_in_events) do
    client = filter.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.Filter.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(filter, key, value) do
    client = filter.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.Filter.t(), String.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(filter, key) do
    client = filter.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.Filter.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(filter, key) do
    client = filter.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.Filter.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_ouput_bitrate(Kurento.Remote.Filter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `int`
  """
  def get_min_ouput_bitrate(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_output_bitrate(Kurento.Remote.Filter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `int`
  """
  def get_min_output_bitrate(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_ouput_bitrate(Kurento.Remote.Filter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `int`
  """
  def get_max_ouput_bitrate(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_output_bitrate(Kurento.Remote.Filter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `int`
  """
  def get_max_output_bitrate(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_ouput_bitrate(Kurento.Remote.Filter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `minOuputBitrate`: `int`
  """
  def set_min_ouput_bitrate(filter, min_ouput_bitrate) do
    client = filter.client

    operation_params = %{
      minOuputBitrate: min_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_output_bitrate(Kurento.Remote.Filter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `minOutputBitrate`: `int`
  """
  def set_min_output_bitrate(filter, min_output_bitrate) do
    client = filter.client

    operation_params = %{
      minOutputBitrate: min_output_bitrate
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_ouput_bitrate(Kurento.Remote.Filter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `maxOuputBitrate`: `int`
  """
  def set_max_ouput_bitrate(filter, max_ouput_bitrate) do
    client = filter.client

    operation_params = %{
      maxOuputBitrate: max_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_output_bitrate(Kurento.Remote.Filter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `maxOutputBitrate`: `int`
  """
  def set_max_output_bitrate(filter, max_output_bitrate) do
    client = filter.client

    operation_params = %{
      maxOutputBitrate: max_output_bitrate
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.Filter.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t(), String.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(filter, media_type, description) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.Filter.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t(), String.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(filter, media_type, description) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(Kurento.Remote.Filter.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  """
  def connect(filter, sink) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def connect(filter, sink, media_type) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(filter, sink, media_type, source_media_description) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(filter, sink, media_type, source_media_description, sink_media_description) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(Kurento.Remote.Filter.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  """
  def disconnect(filter, sink) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def disconnect(filter, sink, media_type) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(filter, sink, media_type, source_media_description) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.Filter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(filter, sink, media_type, source_media_description, sink_media_description) do
    client = filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_audio_format(Kurento.Remote.Filter.t(), Kurento.Struct.AudioCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the audio stream. 

  MediaElements that do not support configuration of audio capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception. 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `caps`: `Kurento.Struct.AudioCaps` - The format for the stream of audio
  """
  def set_audio_format(filter, caps) do
    client = filter.client

    operation_params = %{
      caps: Kurento.Struct.AudioCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setAudioFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_video_format(Kurento.Remote.Filter.t(), Kurento.Struct.VideoCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the video stream. 

  MediaElements that do not support configuration of video capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `caps`: `Kurento.Struct.VideoCaps` - The format for the stream of video
  """
  def set_video_format(filter, caps) do
    client = filter.client

    operation_params = %{
      caps: Kurento.Struct.VideoCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setVideoFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Filter.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Filter.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(filter, details) do
    client = filter.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_output_bitrate(Kurento.Remote.Filter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  @deprecated Allows change the target bitrate for the media output, if the media is encoded using VP8 or H264. This method only works if it is called before the media starts to flow.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `bitrate`: `int` - Configure the enconding media bitrate in bps
  """
  def set_output_bitrate(filter, bitrate) do
    client = filter.client

    operation_params = %{
      bitrate: bitrate
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "setOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.Filter.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(filter) do
    client = filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t(), String.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sinkMediaDescription`: `String` - Description of the sink

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(filter, media_type, sink_media_description) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t(), String.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sourceMediaDescription`: `String` - Description of the source

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(filter, media_type, source_media_description) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(filter, media_type) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(Kurento.Remote.Filter.t(), Kurento.Enum.MediaType.t(), String.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `binName`: `String` - Internal name of the processing bin, as previously given by ``MediaTranscodingStateChange``.

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(filter, media_type, bin_name) do
    client = filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      binName: bin_name
    }

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_connected(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_connected events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_element_connected(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_connected(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_connected events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_element_connected(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_disconnected(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_disconnected events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_element_disconnected(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_disconnected(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_disconnected events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_element_disconnected(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_out_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_out_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_media_flow_out_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_out_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_out_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_media_flow_out_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_in_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_in_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_media_flow_in_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_in_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_in_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_media_flow_in_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_transcoding_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_transcoding_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_media_transcoding_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_transcoding_state_change(Kurento.Remote.Filter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_transcoding_state_change events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_media_transcoding_state_change(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.Filter.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def subscribe_error(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.Filter.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `filter`: `Kurento.Remote.Filter` - The `Kurento.Remote.Filter` to operate on.
  """
  def unsubscribe_error(filter) do
    client = filter.client

    params = %{
      object: Kurento.Remote.Filter.to_param(filter),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.Filter`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(filter) do
    results = [
      subscribe_element_connected(filter),
      subscribe_element_disconnected(filter),
      subscribe_media_flow_out_state_change(filter),
      subscribe_media_flow_in_state_change(filter),
      subscribe_media_transcoding_state_change(filter),
      subscribe_error(filter)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.Filter`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(filter) do
    results = [
      unsubscribe_element_connected(filter),
      unsubscribe_element_disconnected(filter),
      unsubscribe_media_flow_out_state_change(filter),
      unsubscribe_media_flow_in_state_change(filter),
      unsubscribe_media_transcoding_state_change(filter),
      unsubscribe_error(filter)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.Filter{client: client, id: param}
  end

  @doc false
  def to_param(filter) do
    filter.id
  end

  @doc "Release the Kurento Object"
  def release(filter) do
    params = %{object: Kurento.Remote.Filter.to_param(filter)}
    client = filter.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end