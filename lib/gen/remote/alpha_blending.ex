defmodule Kurento.Remote.AlphaBlending do
  @moduledoc """
  A :rom:cls:`Hub` that mixes the :rom:attr:`MediaType.AUDIO` stream of its connected sources and constructs one output with :rom:attr:`MediaType.VIDEO` streams of its connected sources into its sink
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.AlphaBlending.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create for the given pipeline

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the dispatcher belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{type: "AlphaBlending", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.AlphaBlending.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `String`
  """
  def get_id(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `String`
  """
  def get_name(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `int`
  """
  def get_creation_time(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.AlphaBlending.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `name`: `String`
  """
  def set_name(alpha_blending, name) do
    client = alpha_blending.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.AlphaBlending.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(alpha_blending, send_tags_in_events) do
    client = alpha_blending.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.AlphaBlending.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(alpha_blending, key, value) do
    client = alpha_blending.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.AlphaBlending.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(alpha_blending, key) do
    client = alpha_blending.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.AlphaBlending.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(alpha_blending, key) do
    client = alpha_blending.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.AlphaBlending.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(alpha_blending) do
    client = alpha_blending.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.AlphaBlending.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(alpha_blending, details) do
    client = alpha_blending.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_master(Kurento.Remote.AlphaBlending.t(), Kurento.Remote.HubPort.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Sets the source port that will be the master entry to the mixer

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `source`: `Kurento.Remote.HubPort` - The reference to the HubPort setting as master port
  * `zOrder`: `int` - The order in z to draw the master image
  """
  def set_master(alpha_blending, source, z_order) do
    client = alpha_blending.client

    operation_params = %{
      source: Kurento.Remote.HubPort.to_param(source),
      zOrder: z_order
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "setMaster",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_port_properties(
          Kurento.Remote.AlphaBlending.t(),
          float(),
          float(),
          integer(),
          float(),
          float(),
          Kurento.Remote.HubPort.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Configure the blending mode of one port.

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  * `relativeX`: `float` - The x position relative to the master port. Values from 0 to 1 are accepted. The value 0, indicates the coordinate 0 in the master image.
  * `relativeY`: `float` - The y position relative to the master port. Values from 0 to 1 are accepted. The value 0, indicates the coordinate 0 in the master image.
  * `zOrder`: `int` - The order in z to draw the images. The greatest value of z is in the top.
  * `relativeWidth`: `float` - The image width relative to the master port width. Values from 0 to 1 are accepted.
  * `relativeHeight`: `float` - The image height relative to the master port height. Values from 0 to 1 are accepted.
  * `port`: `Kurento.Remote.HubPort` - The reference to the confingured port.
  """
  def set_port_properties(
        alpha_blending,
        relative_x,
        relative_y,
        z_order,
        relative_width,
        relative_height,
        port
      ) do
    client = alpha_blending.client

    operation_params = %{
      relativeX: relative_x,
      relativeY: relative_y,
      zOrder: z_order,
      relativeWidth: relative_width,
      relativeHeight: relative_height,
      port: Kurento.Remote.HubPort.to_param(port)
    }

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      operation: "setPortProperties",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.AlphaBlending.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  """
  def subscribe_error(alpha_blending) do
    client = alpha_blending.client

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.AlphaBlending.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `alpha_blending`: `Kurento.Remote.AlphaBlending` - The `Kurento.Remote.AlphaBlending` to operate on.
  """
  def unsubscribe_error(alpha_blending) do
    client = alpha_blending.client

    params = %{
      object: Kurento.Remote.AlphaBlending.to_param(alpha_blending),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.AlphaBlending`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(alpha_blending) do
    results = [
      subscribe_error(alpha_blending)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.AlphaBlending`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(alpha_blending) do
    results = [
      unsubscribe_error(alpha_blending)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.AlphaBlending{client: client, id: param}
  end

  @doc false
  def to_param(alpha_blending) do
    alpha_blending.id
  end

  @doc "Release the Kurento Object"
  def release(alpha_blending) do
    params = %{object: Kurento.Remote.AlphaBlending.to_param(alpha_blending)}
    client = alpha_blending.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end