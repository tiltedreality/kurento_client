defmodule Kurento.Remote.GStreamerFilter do
  @moduledoc """
  A generic filter interface that allows injecting any GStreamer element. 

  Note however that the current implementation of GStreamerFilter only allows single elements to be injected; one cannot indicate more than one at the same time; use several GStreamerFilters if you need to inject more than one element at the same time.
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t(), String.t()) ::
          {:ok, Kurento.Remote.GStreamerFilter.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`GStreamerFilter`.

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the filter belongs
  * `command`: `String` - String used to instantiate the GStreamer element, as in `gst-launch <https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html>`__.
  """
  def create(media_pipeline, command) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      command: command
    }

    client = media_pipeline.client

    params = %{type: "GStreamerFilter", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.GStreamerFilter.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec create(Kurento.Remote.MediaPipeline.t(), String.t(), Kurento.Enum.FilterType.t()) ::
          {:ok, Kurento.Remote.GStreamerFilter.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`GStreamerFilter`.

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the filter belongs
  * `command`: `String` - String used to instantiate the GStreamer element, as in `gst-launch <https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html>`__.
  * `filterType`: `Kurento.Enum.FilterType` - Sets the filter as Audio, Video, or Autodetect.
  """
  def create(media_pipeline, command, filter_type) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      command: command,
      filterType: Kurento.Enum.FilterType.to_param(filter_type)
    }

    client = media_pipeline.client

    params = %{type: "GStreamerFilter", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.GStreamerFilter.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `String`
  """
  def get_id(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `String`
  """
  def get_name(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `int`
  """
  def get_creation_time(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.GStreamerFilter.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `name`: `String`
  """
  def set_name(g_streamer_filter, name) do
    client = g_streamer_filter.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.GStreamerFilter.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(g_streamer_filter, send_tags_in_events) do
    client = g_streamer_filter.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.GStreamerFilter.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(g_streamer_filter, key, value) do
    client = g_streamer_filter.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.GStreamerFilter.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(g_streamer_filter, key) do
    client = g_streamer_filter.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.GStreamerFilter.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(g_streamer_filter, key) do
    client = g_streamer_filter.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_ouput_bitrate(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `int`
  """
  def get_min_ouput_bitrate(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_output_bitrate(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `int`
  """
  def get_min_output_bitrate(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_ouput_bitrate(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `int`
  """
  def get_max_ouput_bitrate(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_output_bitrate(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `int`
  """
  def get_max_output_bitrate(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_ouput_bitrate(Kurento.Remote.GStreamerFilter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `minOuputBitrate`: `int`
  """
  def set_min_ouput_bitrate(g_streamer_filter, min_ouput_bitrate) do
    client = g_streamer_filter.client

    operation_params = %{
      minOuputBitrate: min_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_output_bitrate(Kurento.Remote.GStreamerFilter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `minOutputBitrate`: `int`
  """
  def set_min_output_bitrate(g_streamer_filter, min_output_bitrate) do
    client = g_streamer_filter.client

    operation_params = %{
      minOutputBitrate: min_output_bitrate
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_ouput_bitrate(Kurento.Remote.GStreamerFilter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `maxOuputBitrate`: `int`
  """
  def set_max_ouput_bitrate(g_streamer_filter, max_ouput_bitrate) do
    client = g_streamer_filter.client

    operation_params = %{
      maxOuputBitrate: max_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_output_bitrate(Kurento.Remote.GStreamerFilter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `maxOutputBitrate`: `int`
  """
  def set_max_output_bitrate(g_streamer_filter, max_output_bitrate) do
    client = g_streamer_filter.client

    operation_params = %{
      maxOutputBitrate: max_output_bitrate
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(g_streamer_filter, media_type, description) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(g_streamer_filter, media_type, description) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(Kurento.Remote.GStreamerFilter.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  """
  def connect(g_streamer_filter, sink) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def connect(g_streamer_filter, sink, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(g_streamer_filter, sink, media_type, source_media_description) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(
        g_streamer_filter,
        sink,
        media_type,
        source_media_description,
        sink_media_description
      ) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(Kurento.Remote.GStreamerFilter.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  """
  def disconnect(g_streamer_filter, sink) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def disconnect(g_streamer_filter, sink, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(g_streamer_filter, sink, media_type, source_media_description) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(
        g_streamer_filter,
        sink,
        media_type,
        source_media_description,
        sink_media_description
      ) do
    client = g_streamer_filter.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_audio_format(Kurento.Remote.GStreamerFilter.t(), Kurento.Struct.AudioCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the audio stream. 

  MediaElements that do not support configuration of audio capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception. 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `caps`: `Kurento.Struct.AudioCaps` - The format for the stream of audio
  """
  def set_audio_format(g_streamer_filter, caps) do
    client = g_streamer_filter.client

    operation_params = %{
      caps: Kurento.Struct.AudioCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setAudioFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_video_format(Kurento.Remote.GStreamerFilter.t(), Kurento.Struct.VideoCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the video stream. 

  MediaElements that do not support configuration of video capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `caps`: `Kurento.Struct.VideoCaps` - The format for the stream of video
  """
  def set_video_format(g_streamer_filter, caps) do
    client = g_streamer_filter.client

    operation_params = %{
      caps: Kurento.Struct.VideoCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setVideoFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.GstreamerDotDetails.t()
        ) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(g_streamer_filter, details) do
    client = g_streamer_filter.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_output_bitrate(Kurento.Remote.GStreamerFilter.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  @deprecated Allows change the target bitrate for the media output, if the media is encoded using VP8 or H264. This method only works if it is called before the media starts to flow.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `bitrate`: `int` - Configure the enconding media bitrate in bps
  """
  def set_output_bitrate(g_streamer_filter, bitrate) do
    client = g_streamer_filter.client

    operation_params = %{
      bitrate: bitrate
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sinkMediaDescription`: `String` - Description of the sink

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(g_streamer_filter, media_type, sink_media_description) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sourceMediaDescription`: `String` - Description of the source

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(g_streamer_filter, media_type, source_media_description) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(Kurento.Remote.GStreamerFilter.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(g_streamer_filter, media_type) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(
          Kurento.Remote.GStreamerFilter.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `binName`: `String` - Internal name of the processing bin, as previously given by ``MediaTranscodingStateChange``.

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(g_streamer_filter, media_type, bin_name) do
    client = g_streamer_filter.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      binName: bin_name
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_command(Kurento.Remote.GStreamerFilter.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  String used to instantiate the GStreamer element, as in `gst-launch <https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html>`__.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.

  Returns: `String`
  """
  def get_command(g_streamer_filter) do
    client = g_streamer_filter.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "getCommand",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_element_property(Kurento.Remote.GStreamerFilter.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Provide a value to one of the GStreamer element's properties.

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  * `propertyName`: `String` - Name of the property that needs to be modified in the GStreamer element.
  * `propertyValue`: `String` - Value that must be assigned to the property.
  """
  def set_element_property(g_streamer_filter, property_name, property_value) do
    client = g_streamer_filter.client

    operation_params = %{
      propertyName: property_name,
      propertyValue: property_value
    }

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      operation: "setElementProperty",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_connected(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_connected events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_element_connected(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_connected(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_connected events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_element_connected(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_disconnected(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_disconnected events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_element_disconnected(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_disconnected(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_disconnected events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_element_disconnected(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_out_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_out_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_media_flow_out_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_out_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_out_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_media_flow_out_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_in_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_in_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_media_flow_in_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_in_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_in_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_media_flow_in_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_transcoding_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_transcoding_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_media_transcoding_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_transcoding_state_change(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_transcoding_state_change events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_media_transcoding_state_change(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def subscribe_error(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.GStreamerFilter.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `g_streamer_filter`: `Kurento.Remote.GStreamerFilter` - The `Kurento.Remote.GStreamerFilter` to operate on.
  """
  def unsubscribe_error(g_streamer_filter) do
    client = g_streamer_filter.client

    params = %{
      object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.GStreamerFilter`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(g_streamer_filter) do
    results = [
      subscribe_element_connected(g_streamer_filter),
      subscribe_element_disconnected(g_streamer_filter),
      subscribe_media_flow_out_state_change(g_streamer_filter),
      subscribe_media_flow_in_state_change(g_streamer_filter),
      subscribe_media_transcoding_state_change(g_streamer_filter),
      subscribe_error(g_streamer_filter)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.GStreamerFilter`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(g_streamer_filter) do
    results = [
      unsubscribe_element_connected(g_streamer_filter),
      unsubscribe_element_disconnected(g_streamer_filter),
      unsubscribe_media_flow_out_state_change(g_streamer_filter),
      unsubscribe_media_flow_in_state_change(g_streamer_filter),
      unsubscribe_media_transcoding_state_change(g_streamer_filter),
      unsubscribe_error(g_streamer_filter)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.GStreamerFilter{client: client, id: param}
  end

  @doc false
  def to_param(g_streamer_filter) do
    g_streamer_filter.id
  end

  @doc "Release the Kurento Object"
  def release(g_streamer_filter) do
    params = %{object: Kurento.Remote.GStreamerFilter.to_param(g_streamer_filter)}
    client = g_streamer_filter.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end