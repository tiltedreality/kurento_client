defmodule Kurento.Remote.Dispatcher do
  @moduledoc """
  A :rom:cls:`Hub` that allows routing between arbitrary port pairs
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.Dispatcher.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`Dispatcher` belonging to the given pipeline.

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the dispatcher belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{type: "Dispatcher", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.Dispatcher.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.Dispatcher.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.Dispatcher.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.Dispatcher.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `String`
  """
  def get_id(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.Dispatcher.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.Dispatcher.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.Dispatcher.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `String`
  """
  def get_name(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.Dispatcher.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.Dispatcher.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `int`
  """
  def get_creation_time(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.Dispatcher.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `name`: `String`
  """
  def set_name(dispatcher, name) do
    client = dispatcher.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.Dispatcher.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(dispatcher, send_tags_in_events) do
    client = dispatcher.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.Dispatcher.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(dispatcher, key, value) do
    client = dispatcher.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.Dispatcher.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(dispatcher, key) do
    client = dispatcher.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.Dispatcher.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(dispatcher, key) do
    client = dispatcher.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.Dispatcher.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Dispatcher.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(dispatcher) do
    client = dispatcher.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Dispatcher.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(dispatcher, details) do
    client = dispatcher.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.Dispatcher.t(),
          Kurento.Remote.HubPort.t(),
          Kurento.Remote.HubPort.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects each corresponding :rom:enum:`MediaType` of the given source port with the sink port.

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  * `source`: `Kurento.Remote.HubPort` - Source port to be connected
  * `sink`: `Kurento.Remote.HubPort` - Sink port to be connected
  """
  def connect(dispatcher, source, sink) do
    client = dispatcher.client

    operation_params = %{
      source: Kurento.Remote.HubPort.to_param(source),
      sink: Kurento.Remote.HubPort.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.Dispatcher.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  """
  def subscribe_error(dispatcher) do
    client = dispatcher.client

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.Dispatcher.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `dispatcher`: `Kurento.Remote.Dispatcher` - The `Kurento.Remote.Dispatcher` to operate on.
  """
  def unsubscribe_error(dispatcher) do
    client = dispatcher.client

    params = %{
      object: Kurento.Remote.Dispatcher.to_param(dispatcher),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.Dispatcher`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(dispatcher) do
    results = [
      subscribe_error(dispatcher)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.Dispatcher`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(dispatcher) do
    results = [
      unsubscribe_error(dispatcher)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.Dispatcher{client: client, id: param}
  end

  @doc false
  def to_param(dispatcher) do
    dispatcher.id
  end

  @doc "Release the Kurento Object"
  def release(dispatcher) do
    params = %{object: Kurento.Remote.Dispatcher.to_param(dispatcher)}
    client = dispatcher.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end