defmodule Kurento.Remote.MediaPipeline do
  @moduledoc """
  A pipeline is a container for a collection of :rom:cls:`MediaElements<MediaElement>` and :rom:cls:`MediaMixers<MediaMixer>`. It offers the methods needed to control the creation and connection of elements inside a certain pipeline.
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Client.client()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`MediaPipeline`

  ## Params

  * `client`: `Kurento.Client` - The `Kurento.Client` to use
  """
  def create(client) do
    constructor_params = %{}

    params = %{type: "MediaPipeline", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `String`
  """
  def get_id(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `String`
  """
  def get_name(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `int`
  """
  def get_creation_time(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.MediaPipeline.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `name`: `String`
  """
  def set_name(media_pipeline, name) do
    client = media_pipeline.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.MediaPipeline.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(media_pipeline, send_tags_in_events) do
    client = media_pipeline.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.MediaPipeline.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(media_pipeline, key, value) do
    client = media_pipeline.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.MediaPipeline.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(media_pipeline, key) do
    client = media_pipeline.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.MediaPipeline.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(media_pipeline, key) do
    client = media_pipeline.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_latency_stats(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  If statistics about pipeline latency are enabled for all mediaElements

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `boolean`
  """
  def get_latency_stats(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getLatencyStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_latency_stats(Kurento.Remote.MediaPipeline.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  If statistics about pipeline latency are enabled for all mediaElements

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `latencyStats`: `boolean`
  """
  def set_latency_stats(media_pipeline, latency_stats) do
    client = media_pipeline.client

    operation_params = %{
      latencyStats: latency_stats
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "setLatencyStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(media_pipeline) do
    client = media_pipeline.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.MediaPipeline.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(media_pipeline, details) do
    client = media_pipeline.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.MediaPipeline.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  """
  def subscribe_error(media_pipeline) do
    client = media_pipeline.client

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.MediaPipeline.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `media_pipeline`: `Kurento.Remote.MediaPipeline` - The `Kurento.Remote.MediaPipeline` to operate on.
  """
  def unsubscribe_error(media_pipeline) do
    client = media_pipeline.client

    params = %{
      object: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.MediaPipeline`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(media_pipeline) do
    results = [
      subscribe_error(media_pipeline)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.MediaPipeline`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(media_pipeline) do
    results = [
      unsubscribe_error(media_pipeline)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.MediaPipeline{client: client, id: param}
  end

  @doc false
  def to_param(media_pipeline) do
    media_pipeline.id
  end

  @doc "Release the Kurento Object"
  def release(media_pipeline) do
    params = %{object: Kurento.Remote.MediaPipeline.to_param(media_pipeline)}
    client = media_pipeline.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end