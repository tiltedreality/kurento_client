defmodule Kurento.Remote.RtpEndpoint do
  @moduledoc """
  Endpoint that provides bidirectional content delivery capabilities with remote networked peers through RTP or SRTP protocol. An :rom:cls:`RtpEndpoint` contains paired sink and source :rom:cls:`MediaPad` for audio and video. This endpoint inherits from :rom:cls:`BaseRtpEndpoint`. 

  In order to establish an RTP/SRTP communication, peers engage in an SDP negotiation process, where one of the peers (the offerer) sends an offer, while the other peer (the offeree) responds with an answer. This endpoint can function in both situations <ul style='list-style-type:circle'> *  As offerer: The negotiation process is initiated by the media server 

  * KMS generates the SDP offer through the generateOffer method. This offer must then be sent to the remote peer (the offeree) through the signaling channel, for processing.
  * The remote peer process the Offer, and generates an Answer to this offer. The Answer is sent back to the media server.
  * Upon receiving the Answer, the endpoint must invoke the processAnswer method.

  *  As offeree: The negotiation process is initiated by the remote peer 

  * The remote peer, acting as offerer, generates an SDP offer and sends it to the WebRTC endpoint in Kurento.
  * The endpoint will process the Offer invoking the processOffer method. The result of this method will be a string, containing an SDP Answer.
  * The SDP Answer must be sent back to the offerer, so it can be processed.

  In case of unidirectional connections (i.e. only one peer is going to send media), the process is more simple, as only the emitter needs to process an SDP. On top of the information about media codecs and types, the SDP must contain the IP of the remote peer, and the port where it will be listening. This way, the SDP can be mangled without needing to go through the exchange process, as the receiving peer does not need to process any answer. 

  The user can set some bandwidth limits that will be used during the negotiation process. The default bandwidth range of the endpoint is 100kbps-500kbps, but it can be changed separately for input/output directions and for audio/video streams. <ul style='list-style-type:circle'> *  Input bandwidth control mechanism: Configuration interval used to inform remote peer the range of bitrates that can be pushed into this RtpEndpoint object. These values are announced in the SDP. 

  *  setMaxVideoRecvBandwidth: sets Max bitrate limits expected for received video stream. 
  *  setMaxAudioRecvBandwidth: sets Max bitrate limits expected for received audio stream. 

  *  Output bandwidth control mechanism: Configuration interval used to control bitrate of the output video stream sent to remote peer. Remote peers can also announce bandwidth limitation in their SDPs (through the b=<modifier>:<value> tag). Kurento will always enforce bitrate limitations specified by the remote peer over internal configurations. 

  *  setMaxVideoSendBandwidth: sets Max bitrate limits for video sent to remote peer. 
  *  setMinVideoSendBandwidth: sets Min bitrate limits for audio sent to remote peer. 

  All bandwidth control parameters must be changed before the SDP negotiation takes place, and can't be modified afterwards. TODO: What happens if the b=as tag form the SDP has a lower value than the one set in setMinVideoSendBandwidth? 

  Take into consideration that setting a too high upper limit for the output bandwidth can be a reason for the local network connection to be overflooded.
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.RtpEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RtpEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{type: "RtpEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RtpEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec create(Kurento.Remote.MediaPipeline.t(), Kurento.Struct.SDES.t()) ::
          {:ok, Kurento.Remote.RtpEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RtpEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  * `crypto`: `Kurento.Struct.SDES` - SDES-type param. If present, this parameter indicates that the communication will be encrypted. By default no encryption is used.
  """
  def create(media_pipeline, crypto) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      crypto: Kurento.Struct.SDES.to_param(crypto)
    }

    client = media_pipeline.client

    params = %{type: "RtpEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RtpEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec create(Kurento.Remote.MediaPipeline.t(), Kurento.Struct.SDES.t(), boolean()) ::
          {:ok, Kurento.Remote.RtpEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RtpEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  * `crypto`: `Kurento.Struct.SDES` - SDES-type param. If present, this parameter indicates that the communication will be encrypted. By default no encryption is used.
  * `useIpv6`: `boolean` - This configures the endpoint to use IPv6 instead of IPv4.
  """
  def create(media_pipeline, crypto, use_ipv6) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      crypto: Kurento.Struct.SDES.to_param(crypto),
      useIpv6: use_ipv6
    }

    client = media_pipeline.client

    params = %{type: "RtpEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RtpEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String`
  """
  def get_id(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String`
  """
  def get_name(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_creation_time(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.RtpEndpoint.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `name`: `String`
  """
  def set_name(rtp_endpoint, name) do
    client = rtp_endpoint.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.RtpEndpoint.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(rtp_endpoint, send_tags_in_events) do
    client = rtp_endpoint.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.RtpEndpoint.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(rtp_endpoint, key, value) do
    client = rtp_endpoint.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.RtpEndpoint.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(rtp_endpoint, key) do
    client = rtp_endpoint.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.RtpEndpoint.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(rtp_endpoint, key) do
    client = rtp_endpoint.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_ouput_bitrate(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_ouput_bitrate(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_output_bitrate(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_output_bitrate(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_ouput_bitrate(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_ouput_bitrate(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_output_bitrate(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_output_bitrate(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_ouput_bitrate(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `minOuputBitrate`: `int`
  """
  def set_min_ouput_bitrate(rtp_endpoint, min_ouput_bitrate) do
    client = rtp_endpoint.client

    operation_params = %{
      minOuputBitrate: min_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_output_bitrate(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `minOutputBitrate`: `int`
  """
  def set_min_output_bitrate(rtp_endpoint, min_output_bitrate) do
    client = rtp_endpoint.client

    operation_params = %{
      minOutputBitrate: min_output_bitrate
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_ouput_bitrate(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `maxOuputBitrate`: `int`
  """
  def set_max_ouput_bitrate(rtp_endpoint, max_ouput_bitrate) do
    client = rtp_endpoint.client

    operation_params = %{
      maxOuputBitrate: max_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_output_bitrate(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `maxOutputBitrate`: `int`
  """
  def set_max_output_bitrate(rtp_endpoint, max_output_bitrate) do
    client = rtp_endpoint.client

    operation_params = %{
      maxOutputBitrate: max_output_bitrate
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(rtp_endpoint, media_type, description) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(rtp_endpoint, media_type, description) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(Kurento.Remote.RtpEndpoint.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  """
  def connect(rtp_endpoint, sink) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def connect(rtp_endpoint, sink, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(rtp_endpoint, sink, media_type, source_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(rtp_endpoint, sink, media_type, source_media_description, sink_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(Kurento.Remote.RtpEndpoint.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  """
  def disconnect(rtp_endpoint, sink) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def disconnect(rtp_endpoint, sink, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(rtp_endpoint, sink, media_type, source_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(rtp_endpoint, sink, media_type, source_media_description, sink_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_audio_format(Kurento.Remote.RtpEndpoint.t(), Kurento.Struct.AudioCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the audio stream. 

  MediaElements that do not support configuration of audio capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception. 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `caps`: `Kurento.Struct.AudioCaps` - The format for the stream of audio
  """
  def set_audio_format(rtp_endpoint, caps) do
    client = rtp_endpoint.client

    operation_params = %{
      caps: Kurento.Struct.AudioCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setAudioFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_video_format(Kurento.Remote.RtpEndpoint.t(), Kurento.Struct.VideoCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the video stream. 

  MediaElements that do not support configuration of video capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `caps`: `Kurento.Struct.VideoCaps` - The format for the stream of video
  """
  def set_video_format(rtp_endpoint, caps) do
    client = rtp_endpoint.client

    operation_params = %{
      caps: Kurento.Struct.VideoCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setVideoFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(rtp_endpoint, details) do
    client = rtp_endpoint.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_output_bitrate(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  @deprecated Allows change the target bitrate for the media output, if the media is encoded using VP8 or H264. This method only works if it is called before the media starts to flow.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `bitrate`: `int` - Configure the enconding media bitrate in bps
  """
  def set_output_bitrate(rtp_endpoint, bitrate) do
    client = rtp_endpoint.client

    operation_params = %{
      bitrate: bitrate
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sinkMediaDescription`: `String` - Description of the sink

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(rtp_endpoint, media_type, sink_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sourceMediaDescription`: `String` - Description of the source

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(rtp_endpoint, media_type, source_media_description) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(Kurento.Remote.RtpEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(rtp_endpoint, media_type) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(
          Kurento.Remote.RtpEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `binName`: `String` - Internal name of the processing bin, as previously given by ``MediaTranscodingStateChange``.

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(rtp_endpoint, media_type, bin_name) do
    client = rtp_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      binName: bin_name
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_audio_recv_bandwidth(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum input bitrate, signaled in SDP Offers to WebRTC and RTP senders. 

  This is used to put a limit on the bitrate that the remote peer will send to this endpoint. The net effect of setting this parameter is that *when Kurento generates an SDP Offer*, an 'Application Specific' (AS) maximum bandwidth attribute will be added to the SDP media section: `b=AS:{value}`. 

  Note: This parameter has to be set before the SDP is generated.

  * Unit: kbps (kilobits per second).
   * Default: 0.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_audio_recv_bandwidth(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMaxAudioRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_video_recv_bandwidth(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum input bitrate, signaled in SDP Offers to WebRTC and RTP senders. 

  This is used to put a limit on the bitrate that the remote peer will send to this endpoint. The net effect of setting this parameter is that *when Kurento generates an SDP Offer*, an 'Application Specific' (AS) maximum bandwidth attribute will be added to the SDP media section: `b=AS:{value}`. 

  Note: This parameter has to be set before the SDP is generated.

  * Unit: kbps (kilobits per second).
   * Default: 0.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_video_recv_bandwidth(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMaxVideoRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_audio_recv_bandwidth(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum input bitrate, signaled in SDP Offers to WebRTC and RTP senders. 

  This is used to put a limit on the bitrate that the remote peer will send to this endpoint. The net effect of setting this parameter is that *when Kurento generates an SDP Offer*, an 'Application Specific' (AS) maximum bandwidth attribute will be added to the SDP media section: `b=AS:{value}`. 

  Note: This parameter has to be set before the SDP is generated.

  * Unit: kbps (kilobits per second).
   * Default: 0.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `maxAudioRecvBandwidth`: `int`
  """
  def set_max_audio_recv_bandwidth(rtp_endpoint, max_audio_recv_bandwidth) do
    client = rtp_endpoint.client

    operation_params = %{
      maxAudioRecvBandwidth: max_audio_recv_bandwidth
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMaxAudioRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_video_recv_bandwidth(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum input bitrate, signaled in SDP Offers to WebRTC and RTP senders. 

  This is used to put a limit on the bitrate that the remote peer will send to this endpoint. The net effect of setting this parameter is that *when Kurento generates an SDP Offer*, an 'Application Specific' (AS) maximum bandwidth attribute will be added to the SDP media section: `b=AS:{value}`. 

  Note: This parameter has to be set before the SDP is generated.

  * Unit: kbps (kilobits per second).
   * Default: 0.
   * 0 = unlimited.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `maxVideoRecvBandwidth`: `int`
  """
  def set_max_video_recv_bandwidth(rtp_endpoint, max_video_recv_bandwidth) do
    client = rtp_endpoint.client

    operation_params = %{
      maxVideoRecvBandwidth: max_video_recv_bandwidth
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMaxVideoRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec generate_offer(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Generates an SDP offer with media capabilities of the Endpoint. Throws: 

  *  SDP_END_POINT_ALREADY_NEGOTIATED If the endpoint is already negotiated. 
   *  SDP_END_POINT_GENERATE_OFFER_ERROR if the generated offer is empty. This is most likely due to an internal error.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String` - The SDP offer.
  """
  def generate_offer(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "generateOffer",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec generate_offer(Kurento.Remote.RtpEndpoint.t(), Kurento.Struct.OfferOptions.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Generates an SDP offer with media capabilities of the Endpoint. Throws: 

  *  SDP_END_POINT_ALREADY_NEGOTIATED If the endpoint is already negotiated. 
   *  SDP_END_POINT_GENERATE_OFFER_ERROR if the generated offer is empty. This is most likely due to an internal error.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `options`: `Kurento.Struct.OfferOptions` - An <code>OfferOptions</code> providing options requested for the offer.

  Returns: `String` - The SDP offer.
  """
  def generate_offer(rtp_endpoint, options) do
    client = rtp_endpoint.client

    operation_params = %{
      options: Kurento.Struct.OfferOptions.to_param(options)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "generateOffer",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec process_offer(Kurento.Remote.RtpEndpoint.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Processes SDP offer of the remote peer, and generates an SDP answer based on the endpoint's capabilities. 

  If no matching capabilities are found, the SDP will contain no codecs. 

  Throws: 

  *  SDP_PARSE_ERROR If the offer is empty or has errors. 
   *  SDP_END_POINT_ALREADY_NEGOTIATED If the endpoint is already negotiated. 
   *  SDP_END_POINT_PROCESS_OFFER_ERROR if the generated offer is empty. This is most likely due to an internal error.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `offer`: `String` - SessionSpec offer from the remote User Agent

  Returns: `String` - The chosen configuration from the ones stated in the SDP offer.
  """
  def process_offer(rtp_endpoint, offer) do
    client = rtp_endpoint.client

    operation_params = %{
      offer: offer
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "processOffer",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec process_answer(Kurento.Remote.RtpEndpoint.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Generates an SDP offer with media capabilities of the Endpoint. Throws: 

  *  SDP_PARSE_ERROR If the offer is empty or has errors. 
   *  SDP_END_POINT_ALREADY_NEGOTIATED If the endpoint is already negotiated. 
   *  SDP_END_POINT_PROCESS_ANSWER_ERROR if the result of processing the answer is an empty string. This is most likely due to an internal error. 
   *  SDP_END_POINT_NOT_OFFER_GENERATED If the method is invoked before the generateOffer method.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `answer`: `String` - SessionSpec answer from the remote User Agent

  Returns: `String` - Updated SDP offer, based on the answer received.
  """
  def process_answer(rtp_endpoint, answer) do
    client = rtp_endpoint.client

    operation_params = %{
      answer: answer
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "processAnswer",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_local_session_descriptor(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the local SDP. 

  *  No offer has been generated: returns null. 
   *  Offer has been generated: returns the SDP offer. 
   *  Offer has been generated and answer processed: returns the agreed SDP.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String` - The last agreed SessionSpec.
  """
  def get_local_session_descriptor(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getLocalSessionDescriptor",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_remote_session_descriptor(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This method returns the remote SDP. If the negotiation process is not complete, it will return NULL.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `String` - The last agreed User Agent session description.
  """
  def get_remote_session_descriptor(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getRemoteSessionDescriptor",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_video_recv_bandwidth(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum input bitrate, requested from WebRTC senders with REMB. 

  This is used to set a minimum value of local REMB during bandwidth estimation, if supported by the implementing class. The REMB estimation will then be sent to remote peers, requesting them to send at least the indicated video bitrate. It follows that min values will only have effect in remote peers that support this congestion control mechanism, such as Chrome. 

  * Unit: kbps (kilobits per second).
   * Default: 0.
   *  Note: The absolute minimum REMB value is 30 kbps, even if a lower value is set here.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_video_recv_bandwidth(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMinVideoRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_video_send_bandwidth(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  REMB override of minimum bitrate sent to WebRTC receivers. 

  With this parameter you can control the minimum video quality that will be sent when reacting to bad network conditions. Setting this parameter to a low value permits the video quality to drop when the network conditions get worse. 

  This parameter provides a way to override the bitrate requested by remote REMB bandwidth estimations: the bitrate sent will be always equal or greater than this parameter, even if the remote peer requests even lower bitrates. 

  Note that if you set this parameter too high (trying to avoid bad video quality altogether), you would be limiting the adaptation ability of the congestion control algorithm, and your stream might be unable to ever recover from adverse network conditions. 

  * Unit: kbps (kilobits per second).
   * Default: 100.
   *  0 = unlimited: the video bitrate will drop as needed, even to the lowest possible quality, which might make the video completely blurry and pixelated.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_video_send_bandwidth(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMinVideoSendBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_video_send_bandwidth(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  REMB override of maximum bitrate sent to WebRTC receivers. 

  With this parameter you can control the maximum video quality that will be sent when reacting to good network conditions. Setting this parameter to a high value permits the video quality to raise when the network conditions get better. 

  This parameter provides a way to limit the bitrate requested by remote REMB bandwidth estimations: the bitrate sent will be always equal or less than this parameter, even if the remote peer requests higher bitrates. 

  Note that the default value of **500 kbps** is a VERY conservative one, and leads to a low maximum video quality. Most applications will probably want to increase this to higher values such as 2000 kbps (2 mbps). 

  The REMB congestion control algorithm works by gradually increasing the output video bitrate, until the available bandwidth is fully used or the maximum send bitrate has been reached. This is a slow, progressive change, which starts at 300 kbps by default. You can change the default starting point of REMB estimations, by setting `RembParams.rembOnConnect`. 

  * Unit: kbps (kilobits per second).
   * Default: 500.
   *  0 = unlimited: the video bitrate will grow until all the available network bandwidth is used by the stream.<br /> Note that this might have a bad effect if more than one stream is running (as all of them would try to raise the video bitrate indefinitely, until the network gets saturated).

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_video_send_bandwidth(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMaxVideoSendBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_state(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Enum.MediaState.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Media flow state. 

  * CONNECTED: There is an RTCP flow.
   * DISCONNECTED: No RTCP packets have been received for at least 5 sec.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Kurento.Enum.MediaState`
  """
  def get_media_state(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMediaState",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Enum.MediaState.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_connection_state(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Enum.ConnectionState.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Connection state. 

  * CONNECTED
   * DISCONNECTED

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Kurento.Enum.ConnectionState`
  """
  def get_connection_state(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getConnectionState",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Enum.ConnectionState.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_mtu(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum Transmission Unit (MTU) used for RTP. 

  This setting affects the maximum size that will be used by RTP payloads. You can change it from the default, if you think that a different value would be beneficial for the typical network settings of your application. 

  The default value is 1200 Bytes. This is the same as in **libwebrtc** (from webrtc.org), as used by <a href='https://dxr.mozilla.org/mozilla-central/rev/b5c5ba07d3dbd0d07b66fa42a103f4df2c27d3a2/media/webrtc/trunk/webrtc/media/engine/constants.cc#16' >Firefox</a > or <a href='https://codesearch.chromium.org/chromium/src/third_party/webrtc/media/engine/constants.cc?l=15&rcl=6dd488b2e55125644263e4837f1abd950d5e410d' >Chrome</a > . You can read more about this value in <a href='https://groups.google.com/d/topic/discuss-webrtc/gH5ysR3SoZI/discussion' >Why RTP max packet size is 1200 in WebRTC?</a > . 

  **WARNING**: Change this value ONLY if you really know what you are doing and you have strong reasons to do so. Do NOT change this parameter just because it *seems* to work better for some reduced scope tests. The default value is a consensus chosen by people who have deep knowledge about network optimization. 

  * Unit: Bytes.
   * Default: 1200.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `int`
  """
  def get_mtu(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getMtu",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_remb_params(Kurento.Remote.RtpEndpoint.t()) ::
          {:ok, Kurento.Struct.RembParams.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Advanced parameters to configure the congestion control algorithm.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.

  Returns: `Kurento.Struct.RembParams`
  """
  def get_remb_params(rtp_endpoint) do
    client = rtp_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "getRembParams",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.RembParams.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_video_recv_bandwidth(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum input bitrate, requested from WebRTC senders with REMB. 

  This is used to set a minimum value of local REMB during bandwidth estimation, if supported by the implementing class. The REMB estimation will then be sent to remote peers, requesting them to send at least the indicated video bitrate. It follows that min values will only have effect in remote peers that support this congestion control mechanism, such as Chrome. 

  * Unit: kbps (kilobits per second).
   * Default: 0.
   *  Note: The absolute minimum REMB value is 30 kbps, even if a lower value is set here.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `minVideoRecvBandwidth`: `int`
  """
  def set_min_video_recv_bandwidth(rtp_endpoint, min_video_recv_bandwidth) do
    client = rtp_endpoint.client

    operation_params = %{
      minVideoRecvBandwidth: min_video_recv_bandwidth
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMinVideoRecvBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_video_send_bandwidth(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  REMB override of minimum bitrate sent to WebRTC receivers. 

  With this parameter you can control the minimum video quality that will be sent when reacting to bad network conditions. Setting this parameter to a low value permits the video quality to drop when the network conditions get worse. 

  This parameter provides a way to override the bitrate requested by remote REMB bandwidth estimations: the bitrate sent will be always equal or greater than this parameter, even if the remote peer requests even lower bitrates. 

  Note that if you set this parameter too high (trying to avoid bad video quality altogether), you would be limiting the adaptation ability of the congestion control algorithm, and your stream might be unable to ever recover from adverse network conditions. 

  * Unit: kbps (kilobits per second).
   * Default: 100.
   *  0 = unlimited: the video bitrate will drop as needed, even to the lowest possible quality, which might make the video completely blurry and pixelated.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `minVideoSendBandwidth`: `int`
  """
  def set_min_video_send_bandwidth(rtp_endpoint, min_video_send_bandwidth) do
    client = rtp_endpoint.client

    operation_params = %{
      minVideoSendBandwidth: min_video_send_bandwidth
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMinVideoSendBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_video_send_bandwidth(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  REMB override of maximum bitrate sent to WebRTC receivers. 

  With this parameter you can control the maximum video quality that will be sent when reacting to good network conditions. Setting this parameter to a high value permits the video quality to raise when the network conditions get better. 

  This parameter provides a way to limit the bitrate requested by remote REMB bandwidth estimations: the bitrate sent will be always equal or less than this parameter, even if the remote peer requests higher bitrates. 

  Note that the default value of **500 kbps** is a VERY conservative one, and leads to a low maximum video quality. Most applications will probably want to increase this to higher values such as 2000 kbps (2 mbps). 

  The REMB congestion control algorithm works by gradually increasing the output video bitrate, until the available bandwidth is fully used or the maximum send bitrate has been reached. This is a slow, progressive change, which starts at 300 kbps by default. You can change the default starting point of REMB estimations, by setting `RembParams.rembOnConnect`. 

  * Unit: kbps (kilobits per second).
   * Default: 500.
   *  0 = unlimited: the video bitrate will grow until all the available network bandwidth is used by the stream.<br /> Note that this might have a bad effect if more than one stream is running (as all of them would try to raise the video bitrate indefinitely, until the network gets saturated).

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `maxVideoSendBandwidth`: `int`
  """
  def set_max_video_send_bandwidth(rtp_endpoint, max_video_send_bandwidth) do
    client = rtp_endpoint.client

    operation_params = %{
      maxVideoSendBandwidth: max_video_send_bandwidth
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMaxVideoSendBandwidth",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_mtu(Kurento.Remote.RtpEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum Transmission Unit (MTU) used for RTP. 

  This setting affects the maximum size that will be used by RTP payloads. You can change it from the default, if you think that a different value would be beneficial for the typical network settings of your application. 

  The default value is 1200 Bytes. This is the same as in **libwebrtc** (from webrtc.org), as used by <a href='https://dxr.mozilla.org/mozilla-central/rev/b5c5ba07d3dbd0d07b66fa42a103f4df2c27d3a2/media/webrtc/trunk/webrtc/media/engine/constants.cc#16' >Firefox</a > or <a href='https://codesearch.chromium.org/chromium/src/third_party/webrtc/media/engine/constants.cc?l=15&rcl=6dd488b2e55125644263e4837f1abd950d5e410d' >Chrome</a > . You can read more about this value in <a href='https://groups.google.com/d/topic/discuss-webrtc/gH5ysR3SoZI/discussion' >Why RTP max packet size is 1200 in WebRTC?</a > . 

  **WARNING**: Change this value ONLY if you really know what you are doing and you have strong reasons to do so. Do NOT change this parameter just because it *seems* to work better for some reduced scope tests. The default value is a consensus chosen by people who have deep knowledge about network optimization. 

  * Unit: Bytes.
   * Default: 1200.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `mtu`: `int`
  """
  def set_mtu(rtp_endpoint, mtu) do
    client = rtp_endpoint.client

    operation_params = %{
      mtu: mtu
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setMtu",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_remb_params(Kurento.Remote.RtpEndpoint.t(), Kurento.Struct.RembParams.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Advanced parameters to configure the congestion control algorithm.

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  * `rembParams`: `Kurento.Struct.RembParams`
  """
  def set_remb_params(rtp_endpoint, remb_params) do
    client = rtp_endpoint.client

    operation_params = %{
      rembParams: Kurento.Struct.RembParams.to_param(remb_params)
    }

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      operation: "setRembParams",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_on_key_soft_limit(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :on_key_soft_limit events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_on_key_soft_limit(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "OnKeySoftLimit"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_on_key_soft_limit(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :on_key_soft_limit events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_on_key_soft_limit(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "OnKeySoftLimit"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_state_changed(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_state_changed events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_state_changed(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaStateChanged"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_state_changed(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_state_changed events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_state_changed(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaStateChanged"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_connection_state_changed(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :connection_state_changed events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_connection_state_changed(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ConnectionStateChanged"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_connection_state_changed(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :connection_state_changed events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_connection_state_changed(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ConnectionStateChanged"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_session_terminated(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_session_terminated events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_session_terminated(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaSessionTerminated"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_session_terminated(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_session_terminated events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_session_terminated(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaSessionTerminated"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_session_started(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_session_started events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_session_started(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaSessionStarted"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_session_started(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_session_started events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_session_started(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaSessionStarted"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_connected(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_connected events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_element_connected(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_connected(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_connected events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_element_connected(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_disconnected(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_disconnected events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_element_disconnected(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_disconnected(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_disconnected events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_element_disconnected(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_out_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_out_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_flow_out_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_out_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_out_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_flow_out_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_in_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_in_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_flow_in_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_in_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_in_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_flow_in_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_transcoding_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_transcoding_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_media_transcoding_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_transcoding_state_change(Kurento.Remote.RtpEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_transcoding_state_change events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_media_transcoding_state_change(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.RtpEndpoint.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def subscribe_error(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.RtpEndpoint.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `rtp_endpoint`: `Kurento.Remote.RtpEndpoint` - The `Kurento.Remote.RtpEndpoint` to operate on.
  """
  def unsubscribe_error(rtp_endpoint) do
    client = rtp_endpoint.client

    params = %{
      object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.RtpEndpoint`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.OnKeySoftLimit`
  * `Kurento.Event.MediaStateChanged`
  * `Kurento.Event.ConnectionStateChanged`
  * `Kurento.Event.MediaSessionTerminated`
  * `Kurento.Event.MediaSessionStarted`
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(rtp_endpoint) do
    results = [
      subscribe_on_key_soft_limit(rtp_endpoint),
      subscribe_media_state_changed(rtp_endpoint),
      subscribe_connection_state_changed(rtp_endpoint),
      subscribe_media_session_terminated(rtp_endpoint),
      subscribe_media_session_started(rtp_endpoint),
      subscribe_element_connected(rtp_endpoint),
      subscribe_element_disconnected(rtp_endpoint),
      subscribe_media_flow_out_state_change(rtp_endpoint),
      subscribe_media_flow_in_state_change(rtp_endpoint),
      subscribe_media_transcoding_state_change(rtp_endpoint),
      subscribe_error(rtp_endpoint)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.RtpEndpoint`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.OnKeySoftLimit`
  * `Kurento.Event.MediaStateChanged`
  * `Kurento.Event.ConnectionStateChanged`
  * `Kurento.Event.MediaSessionTerminated`
  * `Kurento.Event.MediaSessionStarted`
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(rtp_endpoint) do
    results = [
      unsubscribe_on_key_soft_limit(rtp_endpoint),
      unsubscribe_media_state_changed(rtp_endpoint),
      unsubscribe_connection_state_changed(rtp_endpoint),
      unsubscribe_media_session_terminated(rtp_endpoint),
      unsubscribe_media_session_started(rtp_endpoint),
      unsubscribe_element_connected(rtp_endpoint),
      unsubscribe_element_disconnected(rtp_endpoint),
      unsubscribe_media_flow_out_state_change(rtp_endpoint),
      unsubscribe_media_flow_in_state_change(rtp_endpoint),
      unsubscribe_media_transcoding_state_change(rtp_endpoint),
      unsubscribe_error(rtp_endpoint)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.RtpEndpoint{client: client, id: param}
  end

  @doc false
  def to_param(rtp_endpoint) do
    rtp_endpoint.id
  end

  @doc "Release the Kurento Object"
  def release(rtp_endpoint) do
    params = %{object: Kurento.Remote.RtpEndpoint.to_param(rtp_endpoint)}
    client = rtp_endpoint.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end