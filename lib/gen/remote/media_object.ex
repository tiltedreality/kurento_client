defmodule Kurento.Remote.MediaObject do
  @moduledoc """
  Base interface used to manage capabilities common to all Kurento elements. ####Properties 

  *  **id**: unique identifier assigned to this `MediaObject` at instantiation time. :rom:cls:`MediaPipeline` IDs are generated with a GUID followed by suffix `_kurento.MediaPipeline`. :rom:cls:`MediaElement` IDs are also a GUID with suffix `_kurento.{ElementType}` and prefixed by parent's ID. > 

  *MediaPipeline ID example*

  ` 907cac3a-809a-4bbe-a93e-ae7e944c5cae_kurento.MediaPipeline ` 

  *MediaElement ID example*

  ` 907cac3a-809a-4bbe-a93e-ae7e944c5cae_kurento.MediaPipeline/403da25a-805b-4cf1-8c55-f190588e6c9b_kurento.WebRtcEndpoint ` 

  *  **name**: free text intended to provide a friendly name for this `MediaObject`. Its default value is the same as the ID. 
  *  **tags**: key-value pairs intended for applications to associate metadata to this `MediaObject` instance. 

  ####Events 

  *  **ErrorEvent**: reports asynchronous error events. It is recommended to always subscribe a listener to this event, as regular error from the pipeline will be notified through it, instead of through an exception when invoking a method.
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec get_media_pipeline(Kurento.Remote.MediaObject.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.MediaObject.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.MediaObject.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `String`
  """
  def get_id(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.MediaObject.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.MediaObject.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.MediaObject.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `String`
  """
  def get_name(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.MediaObject.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.MediaObject.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `int`
  """
  def get_creation_time(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.MediaObject.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  * `name`: `String`
  """
  def set_name(media_object, name) do
    client = media_object.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.MediaObject.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(media_object, send_tags_in_events) do
    client = media_object.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.MediaObject.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(media_object, key, value) do
    client = media_object.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.MediaObject.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(media_object, key) do
    client = media_object.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.MediaObject.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(media_object, key) do
    client = media_object.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.MediaObject.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(media_object) do
    client = media_object.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.MediaObject.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  """
  def subscribe_error(media_object) do
    client = media_object.client

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.MediaObject.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `media_object`: `Kurento.Remote.MediaObject` - The `Kurento.Remote.MediaObject` to operate on.
  """
  def unsubscribe_error(media_object) do
    client = media_object.client

    params = %{
      object: Kurento.Remote.MediaObject.to_param(media_object),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.MediaObject`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(media_object) do
    results = [
      subscribe_error(media_object)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.MediaObject`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(media_object) do
    results = [
      unsubscribe_error(media_object)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.MediaObject{client: client, id: param}
  end

  @doc false
  def to_param(media_object) do
    media_object.id
  end

  @doc "Release the Kurento Object"
  def release(media_object) do
    params = %{object: Kurento.Remote.MediaObject.to_param(media_object)}
    client = media_object.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end