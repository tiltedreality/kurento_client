defmodule Kurento.Remote.Composite do
  @moduledoc """
  A :rom:cls:`Hub` that mixes the :rom:attr:`MediaType.AUDIO` stream of its connected sources and constructs a grid with the :rom:attr:`MediaType.VIDEO` streams of its connected sources into its sink
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.Composite.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create for the given pipeline

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the dispatcher belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{type: "Composite", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.Composite.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.Composite.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.Composite.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.Composite.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `String`
  """
  def get_id(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.Composite.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.Composite.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.Composite.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `String`
  """
  def get_name(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.Composite.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.Composite.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `int`
  """
  def get_creation_time(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.Composite.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `name`: `String`
  """
  def set_name(composite, name) do
    client = composite.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.Composite.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(composite, send_tags_in_events) do
    client = composite.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.Composite.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(composite, key, value) do
    client = composite.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.Composite.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(composite, key) do
    client = composite.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.Composite.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(composite, key) do
    client = composite.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.Composite.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Composite.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(composite) do
    client = composite.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.Composite.t(), Kurento.Enum.GstreamerDotDetails.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(composite, details) do
    client = composite.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.Composite.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  """
  def subscribe_error(composite) do
    client = composite.client

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.Composite.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `composite`: `Kurento.Remote.Composite` - The `Kurento.Remote.Composite` to operate on.
  """
  def unsubscribe_error(composite) do
    client = composite.client

    params = %{
      object: Kurento.Remote.Composite.to_param(composite),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.Composite`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(composite) do
    results = [
      subscribe_error(composite)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.Composite`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(composite) do
    results = [
      unsubscribe_error(composite)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.Composite{client: client, id: param}
  end

  @doc false
  def to_param(composite) do
    composite.id
  end

  @doc "Release the Kurento Object"
  def release(composite) do
    params = %{object: Kurento.Remote.Composite.to_param(composite)}
    client = composite.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end