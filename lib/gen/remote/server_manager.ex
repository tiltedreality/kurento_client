defmodule Kurento.Remote.ServerManager do
  @moduledoc """
  This is a standalone object for managing the MediaServer
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec get_media_pipeline(Kurento.Remote.ServerManager.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.ServerManager.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.ServerManager.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `String`
  """
  def get_id(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.ServerManager.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.ServerManager.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.ServerManager.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `String`
  """
  def get_name(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.ServerManager.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.ServerManager.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `int`
  """
  def get_creation_time(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.ServerManager.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `name`: `String`
  """
  def set_name(server_manager, name) do
    client = server_manager.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.ServerManager.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(server_manager, send_tags_in_events) do
    client = server_manager.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.ServerManager.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(server_manager, key, value) do
    client = server_manager.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.ServerManager.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(server_manager, key) do
    client = server_manager.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.ServerManager.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(server_manager, key) do
    client = server_manager.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.ServerManager.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_info(Kurento.Remote.ServerManager.t()) ::
          {:ok, Kurento.Struct.ServerInfo.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Server information, version, modules, factories, etc

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `Kurento.Struct.ServerInfo`
  """
  def get_info(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getInfo",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.ServerInfo.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_pipelines(Kurento.Remote.ServerManager.t()) ::
          {:ok, [Kurento.Remote.MediaPipeline.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  All the pipelines available in the server

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `MediaPipeline[]`
  """
  def get_pipelines(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getPipelines",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaPipeline.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sessions(Kurento.Remote.ServerManager.t()) ::
          {:ok, [String.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  All active sessions in the server

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `String[]`
  """
  def get_sessions(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getSessions",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_metadata(Kurento.Remote.ServerManager.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Metadata stored in the server

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `String`
  """
  def get_metadata(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getMetadata",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_kmd(Kurento.Remote.ServerManager.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the kmd associated to a module

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `moduleName`: `String` - Name of the module to get its kmd file

  Returns: `String` - The kmd file.
  """
  def get_kmd(server_manager, module_name) do
    client = server_manager.client

    operation_params = %{
      moduleName: module_name
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getKmd",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_cpu_count(Kurento.Remote.ServerManager.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Number of CPU cores that the media server can use. 

  Linux processes can be configured to use only a subset of the cores that are available in the system, via the process affinity settings (**sched_setaffinity(2)**). With this method it is possible to know the number of cores that the media server can use in the machine where it is running. 

  For example, it's possible to limit the core affinity inside a Docker container by running with a command such as *docker run --cpuset-cpus='0,1'*. 

  Note that the return value represents the number of *logical* processing units available, i.e. CPU cores including Hyper-Threading.

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `int` - Number of CPU cores available for the media server.
  """
  def get_cpu_count(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getCpuCount",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_used_cpu(Kurento.Remote.ServerManager.t(), integer()) ::
          {:ok, float()} | {:error, Kurento.CallError.t()}
  @doc """
  Average CPU usage of the server. 

  This method measures the average CPU usage of the media server during the requested interval. Normally you will want to choose an interval between 1000 and 10000 ms. 

  The returned value represents the global system CPU usage of the media server, as an average across all processing units (CPU cores).

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  * `interval`: `int` - Time to measure the average CPU usage, in milliseconds.

  Returns: `float` - CPU usage %.
  """
  def get_used_cpu(server_manager, interval \\ 1000) do
    client = server_manager.client

    operation_params = %{
      interval: interval
    }

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getUsedCpu",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_used_memory(Kurento.Remote.ServerManager.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the amount of memory that the server is using, in KiB

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.

  Returns: `int64` - Used memory, in KiB.
  """
  def get_used_memory(server_manager) do
    client = server_manager.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      operation: "getUsedMemory",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_object_created(Kurento.Remote.ServerManager.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :object_created events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def subscribe_object_created(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "ObjectCreated"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_object_created(Kurento.Remote.ServerManager.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :object_created events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def unsubscribe_object_created(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "ObjectCreated"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_object_destroyed(Kurento.Remote.ServerManager.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :object_destroyed events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def subscribe_object_destroyed(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "ObjectDestroyed"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_object_destroyed(Kurento.Remote.ServerManager.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :object_destroyed events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def unsubscribe_object_destroyed(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "ObjectDestroyed"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.ServerManager.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def subscribe_error(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.ServerManager.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `server_manager`: `Kurento.Remote.ServerManager` - The `Kurento.Remote.ServerManager` to operate on.
  """
  def unsubscribe_error(server_manager) do
    client = server_manager.client

    params = %{
      object: Kurento.Remote.ServerManager.to_param(server_manager),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.ServerManager`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ObjectCreated`
  * `Kurento.Event.ObjectDestroyed`
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(server_manager) do
    results = [
      subscribe_object_created(server_manager),
      subscribe_object_destroyed(server_manager),
      subscribe_error(server_manager)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.ServerManager`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.ObjectCreated`
  * `Kurento.Event.ObjectDestroyed`
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(server_manager) do
    results = [
      unsubscribe_object_created(server_manager),
      unsubscribe_object_destroyed(server_manager),
      unsubscribe_error(server_manager)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.ServerManager{client: client, id: param}
  end

  @doc false
  def to_param(server_manager) do
    server_manager.id
  end

  @doc "Release the Kurento Object"
  def release(server_manager) do
    params = %{object: Kurento.Remote.ServerManager.to_param(server_manager)}
    client = server_manager.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end