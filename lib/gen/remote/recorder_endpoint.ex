defmodule Kurento.Remote.RecorderEndpoint do
  @moduledoc """
  Provides functionality to store media contents. 

  RecorderEndpoint can store media into local files or send it to a remote network storage. When another :rom:cls:`MediaElement` is connected to a RecorderEndpoint, the media coming from the former will be encapsulated into the selected recording format and stored in the designated location. 

  These parameters must be provided to create a RecorderEndpoint, and they cannot be changed afterwards: 

  *  **Destination URI**, where media will be stored. These formats are supported: 

  *  File: A file path that will be written into the local file system. Example: 

  * `file:///path/to/file`

  *  HTTP: A POST request will be used against a remote server. The server must support using the *chunked* encoding mode (HTTP header `Transfer-Encoding: chunked`). Examples: 

  * `http(s)://{server-ip}/path/to/file`
  *  ` http(s)://{username}:{password}@{server-ip}:{server-port}/path/to/file ` 

  *  Relative URIs (with no schema) are supported. They are completed by prepending a default URI defined by property *defaultPath*. This property is defined in the configuration file */etc/kurento/modules/kurento/UriEndpoint.conf.ini*, and the default value is `file:///var/lib/kurento/` 
  *  ** NOTE (for current versions of Kurento 6.x): special characters are not supported in `{username}` or `{password}`. ** This means that `{username}` cannot contain colons (`:`), and `{password}` cannot contain 'at' signs (`@`). This is a limitation of GStreamer 1.8 (the underlying media framework behind Kurento), and is already fixed in newer versions (which the upcoming Kurento 7.x will use). 
  *  ** NOTE (for upcoming Kurento 7.x): special characters in `{username}` or `{password}` must be url-encoded. ** This means that colons (`:`) should be replaced with `%3A`, and 'at' signs (`@`) should be replaced with `%40`. 

  *  **Media Profile** (:rom:attr:`MediaProfileSpecType`), used for storage. This will determine the video and audio encoding. See below for more details about Media Profile. 
  *  **EndOfStream** (optional), a parameter that dictates if the recording should be automatically stopped once the EOS event is detected. 

  Note that ** RecorderEndpoint requires write permissions to the destination ** ; otherwise, the media server won't be able to store any information, and an :rom:evt:`Error` will be fired. Make sure your application subscribes to this event, otherwise troubleshooting issues will be difficult. 

  *  To write local files (if you use `file://`), the system user that is owner of the media server process needs to have write permissions for the requested path. By default, this user is named '`kurento`'. 
  *  To record through HTTP, the remote server must be accessible through the network, and also have the correct write permissions for the destination path. 

  The **Media Profile** is quite an important parameter, as it will determine whether the server needs to perform on-the-fly transcoding of the media. If the input stream codec if not compatible with the selected media profile, the media will be transcoded into a suitable format. This will result in a higher CPU load and will impact overall performance of the media server. 

  For example: If your pipeline receives **VP8**-encoded video from WebRTC, and sends it to a RecorderEndpoint; depending on the format selected... 

  *  WEBM: The input codec is the same as the recording format, so no transcoding will take place. 
  *  MP4: The media server will have to transcode from **VP8** to **H264**. This will raise the CPU load in the system. 
  *  MKV: Again, video must be transcoded from **VP8** to **H264**, which means more CPU load. 

  From this you can see how selecting the correct format for your application is a very important decision. 

  Recording will start as soon as the user invokes the `record` method. The recorder will then store, in the location indicated, the media that the source is sending to the endpoint. If no media is being received, or no endpoint has been connected, then the destination will be empty. The recorder starts storing information into the file as soon as it gets it. 

  **Recording must be stopped** when no more data should be stored. This can be with the `stopAndWait` method, which blocks and returns only after all the information was stored correctly. 

  ** If your output file is empty, this means that the recorder is waiting for input media. ** When another endpoint is connected to the recorder, by default both AUDIO and VIDEO media types are expected, unless specified otherwise when invoking the `connect` method. Failing to provide both types, will result in the RecorderEndpoint buffering the received media: it won't be written to the file until the recording is stopped. The recorder waits until all types of media start arriving, in order to synchronize them appropriately. 

  The source endpoint can be hot-swapped while the recording is taking place. The recorded file will then contain different feeds. When switching video sources, if the new video has different size, the recorder will retain the size of the previous source. If the source is disconnected, the last frame recorded will be shown for the duration of the disconnection, or until the recording is stopped. 

  ** It is recommended to start recording only after media arrives. ** For this, you may use the `MediaFlowInStateChange` and `MediaFlowOutStateChange` events of your endpoints, and synchronize the recording with the moment media comes into the Recorder. For example: 

  *  When the remote video arrives to KMS, your WebRtcEndpoint will start generating packets into the Kurento Pipeline, and it will trigger a `MediaFlowOutStateChange` event. 
  *  When video packets arrive from the WebRtcEndpoint to the RecorderEndpoint, the RecorderEndpoint will raise a `MediaFlowInStateChange` event. 
  *  You should only start recording when RecorderEndpoint has notified a `MediaFlowInStateChange` for ALL streams (so, if you record AUDIO+VIDEO, your application must receive a `MediaFlowInStateChange` event for audio, and another `MediaFlowInStateChange` event for video).
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t(), String.t()) ::
          {:ok, Kurento.Remote.RecorderEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RecorderEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  * `uri`: `String` - URI where the recording will be stored. It must be accessible from the media server process itself:
                <ul>
                  <li>Local server resources: The user running the Kurento Media Server must have write permission over the file.</li>
                  <li>Network resources: Must be accessible from the network where the media server is running.</li>
                </ul>
  """
  def create(media_pipeline, uri) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      uri: uri
    }

    client = media_pipeline.client

    params = %{type: "RecorderEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RecorderEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec create(
          Kurento.Remote.MediaPipeline.t(),
          String.t(),
          Kurento.Enum.MediaProfileSpecType.t()
        ) :: {:ok, Kurento.Remote.RecorderEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RecorderEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  * `uri`: `String` - URI where the recording will be stored. It must be accessible from the media server process itself:
                <ul>
                  <li>Local server resources: The user running the Kurento Media Server must have write permission over the file.</li>
                  <li>Network resources: Must be accessible from the network where the media server is running.</li>
                </ul>
  * `mediaProfile`: `Kurento.Enum.MediaProfileSpecType` - Sets the media profile used for recording. If the profile is different than the one being received at the sink pad, media will be transcoded, resulting in a higher CPU load. For instance, when recording a VP8 encoded video from a WebRTC endpoint in MP4, the load is higher that when recording to WEBM.
  """
  def create(media_pipeline, uri, media_profile) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      uri: uri,
      mediaProfile: Kurento.Enum.MediaProfileSpecType.to_param(media_profile)
    }

    client = media_pipeline.client

    params = %{type: "RecorderEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RecorderEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec create(
          Kurento.Remote.MediaPipeline.t(),
          String.t(),
          Kurento.Enum.MediaProfileSpecType.t(),
          boolean()
        ) :: {:ok, Kurento.Remote.RecorderEndpoint.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Builder for the :rom:cls:`RecorderEndpoint`

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the endpoint belongs
  * `uri`: `String` - URI where the recording will be stored. It must be accessible from the media server process itself:
                <ul>
                  <li>Local server resources: The user running the Kurento Media Server must have write permission over the file.</li>
                  <li>Network resources: Must be accessible from the network where the media server is running.</li>
                </ul>
  * `mediaProfile`: `Kurento.Enum.MediaProfileSpecType` - Sets the media profile used for recording. If the profile is different than the one being received at the sink pad, media will be transcoded, resulting in a higher CPU load. For instance, when recording a VP8 encoded video from a WebRTC endpoint in MP4, the load is higher that when recording to WEBM.
  * `stopOnEndOfStream`: `boolean` - Forces the recorder endpoint to finish processing data when an :term:`EOS` is detected in the stream
  """
  def create(media_pipeline, uri, media_profile, stop_on_end_of_stream) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline),
      uri: uri,
      mediaProfile: Kurento.Enum.MediaProfileSpecType.to_param(media_profile),
      stopOnEndOfStream: stop_on_end_of_stream
    }

    client = media_pipeline.client

    params = %{type: "RecorderEndpoint", constructorParams: constructor_params, properties: %{}}
    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.RecorderEndpoint.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `String`
  """
  def get_id(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `String`
  """
  def get_name(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `int`
  """
  def get_creation_time(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.RecorderEndpoint.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `name`: `String`
  """
  def set_name(recorder_endpoint, name) do
    client = recorder_endpoint.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.RecorderEndpoint.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(recorder_endpoint, send_tags_in_events) do
    client = recorder_endpoint.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.RecorderEndpoint.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(recorder_endpoint, key, value) do
    client = recorder_endpoint.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.RecorderEndpoint.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(recorder_endpoint, key) do
    client = recorder_endpoint.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.RecorderEndpoint.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(recorder_endpoint, key) do
    client = recorder_endpoint.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_ouput_bitrate(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_ouput_bitrate(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_min_output_bitrate(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `int`
  """
  def get_min_output_bitrate(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_ouput_bitrate(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_ouput_bitrate(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_max_output_bitrate(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `int`
  """
  def get_max_output_bitrate(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_ouput_bitrate(Kurento.Remote.RecorderEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`minOutputBitrate` instead of this function.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `minOuputBitrate`: `int`
  """
  def set_min_ouput_bitrate(recorder_endpoint, min_ouput_bitrate) do
    client = recorder_endpoint.client

    operation_params = %{
      minOuputBitrate: min_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setMinOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_min_output_bitrate(Kurento.Remote.RecorderEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Minimum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: 0.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `minOutputBitrate`: `int`
  """
  def set_min_output_bitrate(recorder_endpoint, min_output_bitrate) do
    client = recorder_endpoint.client

    operation_params = %{
      minOutputBitrate: min_output_bitrate
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setMinOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_ouput_bitrate(Kurento.Remote.RecorderEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bandwidth for transcoding. @deprecated Deprecated due to a typo. Use :rom:meth:`maxOutputBitrate` instead of this function.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `maxOuputBitrate`: `int`
  """
  def set_max_ouput_bitrate(recorder_endpoint, max_ouput_bitrate) do
    client = recorder_endpoint.client

    operation_params = %{
      maxOuputBitrate: max_ouput_bitrate
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setMaxOuputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_max_output_bitrate(Kurento.Remote.RecorderEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Maximum video bitrate for transcoding. 

  * Unit: bps (bits per second).
   * Default: MAXINT.
   * 0 = unlimited.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `maxOutputBitrate`: `int`
  """
  def set_max_output_bitrate(recorder_endpoint, max_output_bitrate) do
    client = recorder_endpoint.client

    operation_params = %{
      maxOutputBitrate: max_output_bitrate
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setMaxOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_source_connections(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the sink pads of this media element. 

  Since sink pads are the interface through which a media element gets it's media, whatever is connected to an element's sink pad is formally a source of media. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are sending media to this element. The list will be empty if no sources are found.
  """
  def get_source_connections(recorder_endpoint, media_type, description) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSourceConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_sink_connections(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, [Kurento.Struct.ElementConnectionData.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Gets information about the source pads of this media element. 

  Since source pads connect to other media element's sinks, this is formally the sink of media from the element's perspective. Media can be filtered by type, or by the description given to the pad though which both elements are connected.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO`, :rom:attr:`MediaType.VIDEO` or :rom:attr:`MediaType.DATA`
  * `description`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources

  Returns: `ElementConnectionData[]` - A list of the connections information that are receiving media from this element. The list will be empty if no sources are found.
  """
  def get_sink_connections(recorder_endpoint, media_type, description) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      description: description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getSinkConnections",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value

        {:ok,
         Enum.map(value["value"], &Kurento.Struct.ElementConnectionData.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(Kurento.Remote.RecorderEndpoint.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  """
  def connect(recorder_endpoint, sink) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def connect(recorder_endpoint, sink, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(recorder_endpoint, sink, media_type, source_media_description) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec connect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Connects two elements, with the media flowing from left to right. 

  The element that invokes the connect will be the source of media, creating one sink pad for each type of media connected. The element given as parameter to the method will be the sink, and it will create one sink pad per media type connected. 

  If otherwise not specified, all types of media are connected by default (AUDIO, VIDEO and DATA). It is recommended to connect the specific types of media if not all of them will be used. For this purpose, the connect method can be invoked more than once on the same two elements, but with different media types. 

  The connection is unidirectional. If a bidirectional connection is desired, the position of the media elements must be inverted. For instance, webrtc1.connect(webrtc2) is connecting webrtc1 as source of webrtc2. In order to create a WebRTC one-2one conversation, the user would need to specify the connection on the other direction with webrtc2.connect(webrtc1). 

  Even though one media element can have one sink pad per type of media, only one media element can be connected to another at a given time. If a media element is connected to another, the former will become the source of the sink media element, regardless whether there was another element connected or not.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will receive media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def connect(
        recorder_endpoint,
        sink,
        media_type,
        source_media_description,
        sink_media_description
      ) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "connect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(Kurento.Remote.RecorderEndpoint.t(), Kurento.Remote.MediaElement.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  """
  def disconnect(recorder_endpoint, sink) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  """
  def disconnect(recorder_endpoint, sink, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(recorder_endpoint, sink, media_type, source_media_description) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec disconnect(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Remote.MediaElement.t(),
          Kurento.Enum.MediaType.t(),
          String.t(),
          String.t()
        ) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Disconnects two media elements. This will release the source pads of the source media element, and the sink pads of the sink media element.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `sink`: `Kurento.Remote.MediaElement` - the target :rom:cls:`MediaElement` that will stop receiving media
  * `mediaType`: `Kurento.Enum.MediaType` - the :rom:enum:`MediaType` of the pads that will be connected
  * `sourceMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  * `sinkMediaDescription`: `String` - A textual description of the media source. Currently not used, aimed mainly for :rom:attr:`MediaType.DATA` sources
  """
  def disconnect(
        recorder_endpoint,
        sink,
        media_type,
        source_media_description,
        sink_media_description
      ) do
    client = recorder_endpoint.client

    operation_params = %{
      sink: Kurento.Remote.MediaElement.to_param(sink),
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description,
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "disconnect",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_audio_format(Kurento.Remote.RecorderEndpoint.t(), Kurento.Struct.AudioCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the audio stream. 

  MediaElements that do not support configuration of audio capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception. 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `caps`: `Kurento.Struct.AudioCaps` - The format for the stream of audio
  """
  def set_audio_format(recorder_endpoint, caps) do
    client = recorder_endpoint.client

    operation_params = %{
      caps: Kurento.Struct.AudioCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setAudioFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_video_format(Kurento.Remote.RecorderEndpoint.t(), Kurento.Struct.VideoCaps.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Set the type of data for the video stream. 

  MediaElements that do not support configuration of video capabilities will throw a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR exception 

  NOTE: This method is not implemented yet by the Media Server to do anything useful.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `caps`: `Kurento.Struct.VideoCaps` - The format for the stream of video
  """
  def set_video_format(recorder_endpoint, caps) do
    client = recorder_endpoint.client

    operation_params = %{
      caps: Kurento.Struct.VideoCaps.to_param(caps)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setVideoFormat",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.GstreamerDotDetails.t()
        ) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Return a .dot file describing the topology of the media element. 

  The element can be queried for certain type of data:

  * SHOW_ALL: default value
   * SHOW_CAPS_DETAILS
   * SHOW_FULL_PARAMS
   * SHOW_MEDIA_TYPE
   * SHOW_NON_DEFAULT_PARAMS
   * SHOW_STATES
   * SHOW_VERBOSE

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(recorder_endpoint, details) do
    client = recorder_endpoint.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_output_bitrate(Kurento.Remote.RecorderEndpoint.t(), integer()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  @deprecated Allows change the target bitrate for the media output, if the media is encoded using VP8 or H264. This method only works if it is called before the media starts to flow.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `bitrate`: `int` - Configure the enconding media bitrate in bps
  """
  def set_output_bitrate(recorder_endpoint, bitrate) do
    client = recorder_endpoint.client

    operation_params = %{
      bitrate: bitrate
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "setOutputBitrate",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_stats(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, Kurento.Struct.Stats.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Gets the statistics related to an endpoint. If no media type is specified, it returns statistics for all available types.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `Stats<>` - Delivers a successful result in the form of a RTC stats report. A RTC stats report represents a map between strings, identifying the inspected objects (RTCStats.id), and their corresponding RTCStats objects.
  """
  def get_stats(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getStats",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Struct.Stats.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_in(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is receiving media of a certain type. The media sink pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sinkMediaDescription`: `String` - Description of the sink

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_in(recorder_endpoint, media_type, sink_media_description) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sinkMediaDescription: sink_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaFlowingIn",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_flowing_out(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  This method indicates whether the media element is emitting media of a certain type. The media source pad can be identified individually, if needed. It is only supported for AUDIO and VIDEO types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. If the pad indicated does not exist, if will return false.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `sourceMediaDescription`: `String` - Description of the source

  Returns: `boolean` - TRUE if there is media, FALSE in other case.
  """
  def is_media_flowing_out(recorder_endpoint, media_type, source_media_description) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      sourceMediaDescription: source_media_description
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaFlowingOut",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(Kurento.Remote.RecorderEndpoint.t(), Kurento.Enum.MediaType.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(recorder_endpoint, media_type) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type)
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec is_media_transcoding(
          Kurento.Remote.RecorderEndpoint.t(),
          Kurento.Enum.MediaType.t(),
          String.t()
        ) :: {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Indicates whether this media element is actively transcoding between input and output pads. This operation is only supported for AUDIO and VIDEO media types, raising a MEDIA_OBJECT_ILLEGAL_PARAM_ERROR otherwise. The internal GStreamer processing bin can be indicated, if needed; if the bin doesn't exist, the return value will be FALSE.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  * `mediaType`: `Kurento.Enum.MediaType` - One of :rom:attr:`MediaType.AUDIO` or :rom:attr:`MediaType.VIDEO`
  * `binName`: `String` - Internal name of the processing bin, as previously given by ``MediaTranscodingStateChange``.

  Returns: `boolean` - TRUE if media is being transcoded, FALSE otherwise.
  """
  def is_media_transcoding(recorder_endpoint, media_type, bin_name) do
    client = recorder_endpoint.client

    operation_params = %{
      mediaType: Kurento.Enum.MediaType.to_param(media_type),
      binName: bin_name
    }

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "isMediaTranscoding",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_uri(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  The uri for this endpoint.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `String`
  """
  def get_uri(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getUri",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_state(Kurento.Remote.RecorderEndpoint.t()) ::
          {:ok, Kurento.Enum.UriEndpointState.t()} | {:error, Kurento.CallError.t()}
  @doc """
  State of the endpoint

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.

  Returns: `Kurento.Enum.UriEndpointState`
  """
  def get_state(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "getState",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Enum.UriEndpointState.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec pause(Kurento.Remote.RecorderEndpoint.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Pauses the feed

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def pause(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "pause",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec stop(Kurento.Remote.RecorderEndpoint.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Stops the feed

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def stop(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "stop",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec record(Kurento.Remote.RecorderEndpoint.t()) :: :ok | {:error, Kurento.CallError.t()}
  @doc """
  Starts storing media received through the sink pad.

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def record(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "record",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec stop_and_wait(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Stops recording and does not return until all the content has been written to the selected uri. This can cause timeouts on some clients if there is too much content to write, or the transport is slow

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def stop_and_wait(recorder_endpoint) do
    client = recorder_endpoint.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      operation: "stopAndWait",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_recording(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :recording events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_recording(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Recording"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_recording(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :recording events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_recording(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Recording"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_paused(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :paused events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_paused(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Paused"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_paused(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :paused events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_paused(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Paused"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_stopped(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :stopped events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_stopped(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Stopped"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_stopped(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :stopped events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_stopped(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Stopped"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_uri_endpoint_state_changed(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :uri_endpoint_state_changed events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_uri_endpoint_state_changed(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "UriEndpointStateChanged"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_uri_endpoint_state_changed(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :uri_endpoint_state_changed events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_uri_endpoint_state_changed(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "UriEndpointStateChanged"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_connected(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_connected events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_element_connected(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_connected(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_connected events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_element_connected(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "ElementConnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_element_disconnected(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :element_disconnected events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_element_disconnected(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_element_disconnected(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :element_disconnected events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_element_disconnected(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "ElementDisconnected"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_out_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_out_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_media_flow_out_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_out_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_out_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_media_flow_out_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaFlowOutStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_flow_in_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_flow_in_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_media_flow_in_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_flow_in_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_flow_in_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_media_flow_in_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaFlowInStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_media_transcoding_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :media_transcoding_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_media_transcoding_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_media_transcoding_state_change(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :media_transcoding_state_change events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_media_transcoding_state_change(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "MediaTranscodingStateChange"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def subscribe_error(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.RecorderEndpoint.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `recorder_endpoint`: `Kurento.Remote.RecorderEndpoint` - The `Kurento.Remote.RecorderEndpoint` to operate on.
  """
  def unsubscribe_error(recorder_endpoint) do
    client = recorder_endpoint.client

    params = %{
      object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.RecorderEndpoint`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Recording`
  * `Kurento.Event.Paused`
  * `Kurento.Event.Stopped`
  * `Kurento.Event.UriEndpointStateChanged`
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(recorder_endpoint) do
    results = [
      subscribe_recording(recorder_endpoint),
      subscribe_paused(recorder_endpoint),
      subscribe_stopped(recorder_endpoint),
      subscribe_uri_endpoint_state_changed(recorder_endpoint),
      subscribe_element_connected(recorder_endpoint),
      subscribe_element_disconnected(recorder_endpoint),
      subscribe_media_flow_out_state_change(recorder_endpoint),
      subscribe_media_flow_in_state_change(recorder_endpoint),
      subscribe_media_transcoding_state_change(recorder_endpoint),
      subscribe_error(recorder_endpoint)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.RecorderEndpoint`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Recording`
  * `Kurento.Event.Paused`
  * `Kurento.Event.Stopped`
  * `Kurento.Event.UriEndpointStateChanged`
  * `Kurento.Event.ElementConnected`
  * `Kurento.Event.ElementDisconnected`
  * `Kurento.Event.MediaFlowOutStateChange`
  * `Kurento.Event.MediaFlowInStateChange`
  * `Kurento.Event.MediaTranscodingStateChange`
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(recorder_endpoint) do
    results = [
      unsubscribe_recording(recorder_endpoint),
      unsubscribe_paused(recorder_endpoint),
      unsubscribe_stopped(recorder_endpoint),
      unsubscribe_uri_endpoint_state_changed(recorder_endpoint),
      unsubscribe_element_connected(recorder_endpoint),
      unsubscribe_element_disconnected(recorder_endpoint),
      unsubscribe_media_flow_out_state_change(recorder_endpoint),
      unsubscribe_media_flow_in_state_change(recorder_endpoint),
      unsubscribe_media_transcoding_state_change(recorder_endpoint),
      unsubscribe_error(recorder_endpoint)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.RecorderEndpoint{client: client, id: param}
  end

  @doc false
  def to_param(recorder_endpoint) do
    recorder_endpoint.id
  end

  @doc "Release the Kurento Object"
  def release(recorder_endpoint) do
    params = %{object: Kurento.Remote.RecorderEndpoint.to_param(recorder_endpoint)}
    client = recorder_endpoint.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end