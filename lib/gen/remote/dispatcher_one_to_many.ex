defmodule Kurento.Remote.DispatcherOneToMany do
  @moduledoc """
  A :rom:cls:`Hub` that sends a given source to all the connected sinks
  """
  defstruct [:client, :id]

  @type t :: %{optional(:__struct__) => atom(), client: Kurento.Client.client(), id: String.t()}

  @spec create(Kurento.Remote.MediaPipeline.t()) ::
          {:ok, Kurento.Remote.DispatcherOneToMany.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Create a :rom:cls:`DispatcherOneToMany` belonging to the given pipeline.

  ## Params

  * `mediaPipeline`: `Kurento.Remote.MediaPipeline` - the :rom:cls:`MediaPipeline` to which the dispatcher belongs
  """
  def create(media_pipeline) do
    constructor_params = %{
      mediaPipeline: Kurento.Remote.MediaPipeline.to_param(media_pipeline)
    }

    client = media_pipeline.client

    params = %{
      type: "DispatcherOneToMany",
      constructorParams: constructor_params,
      properties: %{}
    }

    request = Kurento.RPCRequest.create("create", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        {:ok, Kurento.Remote.DispatcherOneToMany.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_media_pipeline(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, Kurento.Remote.MediaPipeline.t()} | {:error, Kurento.CallError.t()}
  @doc """
  :rom:cls:`MediaPipeline` to which this `MediaObject` belongs. It returns itself when invoked for a pipeline object.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `Kurento.Remote.MediaPipeline`
  """
  def get_media_pipeline(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getMediaPipeline",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaPipeline.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_parent(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, Kurento.Remote.MediaObject.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Parent of this `MediaObject`. 

  The parent of a :rom:cls:`Hub` or a :rom:cls:`MediaElement` is its :rom:cls:`MediaPipeline`. A :rom:cls:`MediaPipeline` has no parent, so this property will be null.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `Kurento.Remote.MediaObject`
  """
  def get_parent(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getParent",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Kurento.Remote.MediaObject.from_param(client, value["value"])}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_id(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Unique identifier of this `MediaObject`. 

  It's a synthetic identifier composed by a GUID and `MediaObject` type. The ID is prefixed with the parent ID when the object has parent: *ID_parent/ID_media-object*.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `String`
  """
  def get_id(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getId",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_childs(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`. @deprecated Use children instead.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `MediaObject[]`
  """
  def get_childs(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getChilds",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_children(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, [Kurento.Remote.MediaObject.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Children of this `MediaObject`.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `MediaObject[]`
  """
  def get_children(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getChildren",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Remote.MediaObject.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_name(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `String`
  """
  def get_name(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_send_tags_in_events(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, boolean()} | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `boolean`
  """
  def get_send_tags_in_events(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_creation_time(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, integer()} | {:error, Kurento.CallError.t()}
  @doc """
  `MediaObject` creation time in seconds since Epoch.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `int`
  """
  def get_creation_time(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getCreationTime",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_name(Kurento.Remote.DispatcherOneToMany.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  This `MediaObject`'s name. 

  This is just sugar to simplify developers' life debugging, it is not used internally for indexing nor identifying the objects. By default, it's the object's ID.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `name`: `String`
  """
  def set_name(dispatcher_one_to_many, name) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      name: name
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "setName",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_send_tags_in_events(Kurento.Remote.DispatcherOneToMany.t(), boolean()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Flag activating or deactivating sending the element's tags in fired events.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `sendTagsInEvents`: `boolean`
  """
  def set_send_tags_in_events(dispatcher_one_to_many, send_tags_in_events) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      sendTagsInEvents: send_tags_in_events
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "setSendTagsInEvents",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec add_tag(Kurento.Remote.DispatcherOneToMany.t(), String.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Adds a new tag to this `MediaObject`. If the tag is already present, it changes the value.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `key`: `String` - Tag name.
  * `value`: `String` - Value associated to this tag.
  """
  def add_tag(dispatcher_one_to_many, key, value) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      key: key,
      value: value
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "addTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_tag(Kurento.Remote.DispatcherOneToMany.t(), String.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Removes an existing tag. Exists silently with no error if tag is not defined.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `key`: `String` - Tag name to be removed
  """
  def remove_tag(dispatcher_one_to_many, key) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "removeTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tag(Kurento.Remote.DispatcherOneToMany.t(), String.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns the value of given tag, or MEDIA_OBJECT_TAG_KEY_NOT_FOUND if tag is not defined.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `key`: `String` - Tag key.

  Returns: `String` - The value associated to the given key.
  """
  def get_tag(dispatcher_one_to_many, key) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      key: key
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getTag",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_tags(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, [Kurento.Struct.Tag.t()]} | {:error, Kurento.CallError.t()}
  @doc """
  Returns all tags attached to this `MediaObject`.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `Tag[]` - An array containing all key-value pairs associated with this <code>MediaObject</code>.
  """
  def get_tags(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getTags",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, Enum.map(value["value"], &Kurento.Struct.Tag.from_param(client, &1))}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(Kurento.Remote.DispatcherOneToMany.t()) ::
          {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec get_gstreamer_dot(
          Kurento.Remote.DispatcherOneToMany.t(),
          Kurento.Enum.GstreamerDotDetails.t()
        ) :: {:ok, String.t()} | {:error, Kurento.CallError.t()}
  @doc """
  Returns a string in dot (graphviz) format that represents the gstreamer elements inside the pipeline

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `details`: `Kurento.Enum.GstreamerDotDetails` - Details of graph

  Returns: `String` - The dot graph.
  """
  def get_gstreamer_dot(dispatcher_one_to_many, details) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      details: Kurento.Enum.GstreamerDotDetails.to_param(details)
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "getGstreamerDot",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        {:ok, value["value"]}

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec set_source(Kurento.Remote.DispatcherOneToMany.t(), Kurento.Remote.HubPort.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Sets the source port that will be connected to the sinks of every :rom:cls:`HubPort` of the dispatcher

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  * `source`: `Kurento.Remote.HubPort` - source to be broadcasted
  """
  def set_source(dispatcher_one_to_many, source) do
    client = dispatcher_one_to_many.client

    operation_params = %{
      source: Kurento.Remote.HubPort.to_param(source)
    }

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "setSource",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec remove_source(Kurento.Remote.DispatcherOneToMany.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  Remove the source port and stop the media pipeline.

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  """
  def remove_source(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    operation_params = %{}

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      operation: "removeSource",
      operationParams: operation_params
    }

    request = Kurento.RPCRequest.create("invoke", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec subscribe_error(Kurento.Remote.DispatcherOneToMany.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  subscribe to :error events

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  """
  def subscribe_error(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("subscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @spec unsubscribe_error(Kurento.Remote.DispatcherOneToMany.t()) ::
          :ok | {:error, Kurento.CallError.t()}
  @doc """
  unsubscribe from :error events

  ## Params

  * `dispatcher_one_to_many`: `Kurento.Remote.DispatcherOneToMany` - The `Kurento.Remote.DispatcherOneToMany` to operate on.
  """
  def unsubscribe_error(dispatcher_one_to_many) do
    client = dispatcher_one_to_many.client

    params = %{
      object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many),
      type: "Error"
    }

    request = Kurento.RPCRequest.create("unsubscribe", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, value} ->
        _ = value
        :ok

      {:error, err} ->
        {:error, Kurento.CallError.from_map(err)}
    end
  end

  @doc """
  Subscribe to all events from this `Kurento.Remote.DispatcherOneToMany`

  Note: this is not an atomic operation, and is equivalent to calling all the `subscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all subscriptions were successfull, otherwise the first `{:error error}`.
  """
  def subscribe_all(dispatcher_one_to_many) do
    results = [
      subscribe_error(dispatcher_one_to_many)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc """
  Unsubscribe from all events from this `Kurento.Remote.DispatcherOneToMany`

  Note: this is not an atomic operation, and is equivalent to calling all the `unsubscribe_` functions
  one-by-one

  This includes:
  * `Kurento.Event.Error`

  Returns `:ok` if all unsubscriptions were successfull, otherwise the first `{:error error}`.
  """
  def unsubscribe_all(dispatcher_one_to_many) do
    results = [
      unsubscribe_error(dispatcher_one_to_many)
    ]

    Enum.find(results, :ok, &(&1 !== :ok))
  end

  @doc false
  def from_param(client, param) do
    %Kurento.Remote.DispatcherOneToMany{client: client, id: param}
  end

  @doc false
  def to_param(dispatcher_one_to_many) do
    dispatcher_one_to_many.id
  end

  @doc "Release the Kurento Object"
  def release(dispatcher_one_to_many) do
    params = %{object: Kurento.Remote.DispatcherOneToMany.to_param(dispatcher_one_to_many)}
    client = dispatcher_one_to_many.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end