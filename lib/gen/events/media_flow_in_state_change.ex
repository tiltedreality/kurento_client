defmodule Kurento.Event.MediaFlowInStateChange do
  @moduledoc """
  Fired when the incoming media flow begins or ends. The event contains: 

  * State: whether the endpoint is receiving media (FLOWING) or not (NOT_FLOWING).
  * padName. The name of the pad that changed state.
  * MediaType: The type of media flowing.

  RaiseBase > Media > MediaFlowInStateChange

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `state:` `Kurento.Enum.MediaFlowState` - Current media state
  * `padName:` `String` - Name of the pad which has media
  * `mediaType:` `Kurento.Enum.MediaType` - Type of media that is flowing


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :state, :pad_name, :media_type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          state: Kurento.Enum.MediaFlowState.t(),
          pad_name: String.t(),
          media_type: Kurento.Enum.MediaType.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.MediaFlowInStateChange{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      state: Kurento.Enum.MediaFlowState.from_param(client, params["state"]),
      pad_name: params["padName"],
      media_type: Kurento.Enum.MediaType.from_param(client, params["mediaType"])
    }
  end

  @doc false
  def to_param(media_flow_in_state_change) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(media_flow_in_state_change.source),
      "timestamp" => media_flow_in_state_change.timestamp,
      "timestampMillis" => media_flow_in_state_change.timestamp_millis,
      "tags" => Enum.map(media_flow_in_state_change.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => media_flow_in_state_change.type,
      "state" => Kurento.Enum.MediaFlowState.to_param(media_flow_in_state_change.state),
      "padName" => media_flow_in_state_change.pad_name,
      "mediaType" => Kurento.Enum.MediaType.to_param(media_flow_in_state_change.media_type)
    }
  end
end