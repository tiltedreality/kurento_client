defmodule Kurento.Event.RaiseBase do
  @moduledoc """


  RaiseBase

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.RaiseBase{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1))
    }
  end

  @doc false
  def to_param(raise_base) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(raise_base.source),
      "timestamp" => raise_base.timestamp,
      "timestampMillis" => raise_base.timestamp_millis,
      "tags" => Enum.map(raise_base.tags, &Kurento.Struct.Tag.to_param(&1))
    }
  end
end