defmodule Kurento.Event.UriEndpointStateChanged do
  @moduledoc """
  Indicates the new state of the endpoint

  RaiseBase > Media > UriEndpointStateChanged

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `state:` `Kurento.Enum.UriEndpointState` - the new state


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :state]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          state: Kurento.Enum.UriEndpointState.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.UriEndpointStateChanged{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      state: Kurento.Enum.UriEndpointState.from_param(client, params["state"])
    }
  end

  @doc false
  def to_param(uri_endpoint_state_changed) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(uri_endpoint_state_changed.source),
      "timestamp" => uri_endpoint_state_changed.timestamp,
      "timestampMillis" => uri_endpoint_state_changed.timestamp_millis,
      "tags" => Enum.map(uri_endpoint_state_changed.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => uri_endpoint_state_changed.type,
      "state" => Kurento.Enum.UriEndpointState.to_param(uri_endpoint_state_changed.state)
    }
  end
end