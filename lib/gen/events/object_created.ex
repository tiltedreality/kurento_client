defmodule Kurento.Event.ObjectCreated do
  @moduledoc """
  Indicates that an object has been created on the mediaserver

  RaiseBase > ObjectCreated

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `object:` `Kurento.Remote.MediaObject` - The object that has been created


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :object]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          object: Kurento.Remote.MediaObject.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.ObjectCreated{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      object: Kurento.Remote.MediaObject.from_param(client, params["object"])
    }
  end

  @doc false
  def to_param(object_created) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(object_created.source),
      "timestamp" => object_created.timestamp,
      "timestampMillis" => object_created.timestamp_millis,
      "tags" => Enum.map(object_created.tags, &Kurento.Struct.Tag.to_param(&1)),
      "object" => Kurento.Remote.MediaObject.to_param(object_created.object)
    }
  end
end