defmodule Kurento.Event.Media do
  @moduledoc """
  Base for all events raised by elements in the Kurento media server.

  RaiseBase > Media

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.Media{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"]
    }
  end

  @doc false
  def to_param(media) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(media.source),
      "timestamp" => media.timestamp,
      "timestampMillis" => media.timestamp_millis,
      "tags" => Enum.map(media.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => media.type
    }
  end
end