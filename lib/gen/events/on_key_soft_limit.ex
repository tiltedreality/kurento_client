defmodule Kurento.Event.OnKeySoftLimit do
  @moduledoc """
  Fired when encryption is used and any stream reached the soft key usage limit, which means it will expire soon.

  RaiseBase > Media > OnKeySoftLimit

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `mediaType:` `Kurento.Enum.MediaType` - The media stream


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :media_type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          media_type: Kurento.Enum.MediaType.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.OnKeySoftLimit{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      media_type: Kurento.Enum.MediaType.from_param(client, params["mediaType"])
    }
  end

  @doc false
  def to_param(on_key_soft_limit) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(on_key_soft_limit.source),
      "timestamp" => on_key_soft_limit.timestamp,
      "timestampMillis" => on_key_soft_limit.timestamp_millis,
      "tags" => Enum.map(on_key_soft_limit.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => on_key_soft_limit.type,
      "mediaType" => Kurento.Enum.MediaType.to_param(on_key_soft_limit.media_type)
    }
  end
end