defmodule Kurento.Event.ElementConnected do
  @moduledoc """
  Indicates that an element has been connected to another

  RaiseBase > Media > ElementConnected

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `sink:` `Kurento.Remote.MediaElement` - sink element in new connection
  * `mediaType:` `Kurento.Enum.MediaType` - Media type of the connection
  * `sourceMediaDescription:` `String` - Description of the source media
  * `sinkMediaDescription:` `String` - Description of the sink media


  """
  defstruct [
    :source,
    :timestamp,
    :timestamp_millis,
    :tags,
    :type,
    :sink,
    :media_type,
    :source_media_description,
    :sink_media_description
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          sink: Kurento.Remote.MediaElement.t(),
          media_type: Kurento.Enum.MediaType.t(),
          source_media_description: String.t(),
          sink_media_description: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.ElementConnected{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      sink: Kurento.Remote.MediaElement.from_param(client, params["sink"]),
      media_type: Kurento.Enum.MediaType.from_param(client, params["mediaType"]),
      source_media_description: params["sourceMediaDescription"],
      sink_media_description: params["sinkMediaDescription"]
    }
  end

  @doc false
  def to_param(element_connected) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(element_connected.source),
      "timestamp" => element_connected.timestamp,
      "timestampMillis" => element_connected.timestamp_millis,
      "tags" => Enum.map(element_connected.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => element_connected.type,
      "sink" => Kurento.Remote.MediaElement.to_param(element_connected.sink),
      "mediaType" => Kurento.Enum.MediaType.to_param(element_connected.media_type),
      "sourceMediaDescription" => element_connected.source_media_description,
      "sinkMediaDescription" => element_connected.sink_media_description
    }
  end
end