defmodule Kurento.Event.OnIceCandidate do
  @moduledoc """
  Notifies a new local candidate. These candidates should be sent to the remote peer, to complete the ICE negotiation process. @deprecated Use `IceCandidateFound` instead.

  RaiseBase > Media > OnIceCandidate

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `candidate:` `Kurento.Struct.IceCandidate` - New local candidate


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :candidate]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          candidate: Kurento.Struct.IceCandidate.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.OnIceCandidate{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      candidate: Kurento.Struct.IceCandidate.from_param(client, params["candidate"])
    }
  end

  @doc false
  def to_param(on_ice_candidate) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(on_ice_candidate.source),
      "timestamp" => on_ice_candidate.timestamp,
      "timestampMillis" => on_ice_candidate.timestamp_millis,
      "tags" => Enum.map(on_ice_candidate.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => on_ice_candidate.type,
      "candidate" => Kurento.Struct.IceCandidate.to_param(on_ice_candidate.candidate)
    }
  end
end