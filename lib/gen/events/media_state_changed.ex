defmodule Kurento.Event.MediaStateChanged do
  @moduledoc """
  This event is fired when the media connection between two peers changes, based on the RTCP packet flow. It contains the old and the new state. Possible values are 

  * CONNECTED
  * DISCONNECTED

  RaiseBase > Media > MediaStateChanged

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `oldState:` `Kurento.Enum.MediaState` - The previous state
  * `newState:` `Kurento.Enum.MediaState` - The new state


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :old_state, :new_state]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          old_state: Kurento.Enum.MediaState.t(),
          new_state: Kurento.Enum.MediaState.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.MediaStateChanged{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      old_state: Kurento.Enum.MediaState.from_param(client, params["oldState"]),
      new_state: Kurento.Enum.MediaState.from_param(client, params["newState"])
    }
  end

  @doc false
  def to_param(media_state_changed) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(media_state_changed.source),
      "timestamp" => media_state_changed.timestamp,
      "timestampMillis" => media_state_changed.timestamp_millis,
      "tags" => Enum.map(media_state_changed.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => media_state_changed.type,
      "oldState" => Kurento.Enum.MediaState.to_param(media_state_changed.old_state),
      "newState" => Kurento.Enum.MediaState.to_param(media_state_changed.new_state)
    }
  end
end