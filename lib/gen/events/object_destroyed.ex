defmodule Kurento.Event.ObjectDestroyed do
  @moduledoc """
  Indicates that an object has been destroyed on the mediaserver

  RaiseBase > ObjectDestroyed

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `objectId:` `String` - The id of the object that has been destroyed


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :object_id]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          object_id: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.ObjectDestroyed{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      object_id: params["objectId"]
    }
  end

  @doc false
  def to_param(object_destroyed) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(object_destroyed.source),
      "timestamp" => object_destroyed.timestamp,
      "timestampMillis" => object_destroyed.timestamp_millis,
      "tags" => Enum.map(object_destroyed.tags, &Kurento.Struct.Tag.to_param(&1)),
      "objectId" => object_destroyed.object_id
    }
  end
end