defmodule Kurento.Event.CodeFound do
  @moduledoc """
  Event raised by a :rom:cls:`ZBarFilter` when a code is found in the data being streamed.

  RaiseBase > Media > CodeFound

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `codeType:` `String` - type of :term:`QR` code found
  * `value:` `String` - value contained in the :term:`QR` code


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :code_type, :value]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          code_type: String.t(),
          value: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.CodeFound{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      code_type: params["codeType"],
      value: params["value"]
    }
  end

  @doc false
  def to_param(code_found) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(code_found.source),
      "timestamp" => code_found.timestamp,
      "timestampMillis" => code_found.timestamp_millis,
      "tags" => Enum.map(code_found.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => code_found.type,
      "codeType" => code_found.code_type,
      "value" => code_found.value
    }
  end
end