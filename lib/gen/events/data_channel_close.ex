defmodule Kurento.Event.DataChannelClose do
  @moduledoc """
  Event fired when a data channel is closed.

  RaiseBase > Media > DataChannelClose

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `channelId:` `int` - The channel identifier


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :channel_id]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          channel_id: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.DataChannelClose{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      channel_id: params["channelId"]
    }
  end

  @doc false
  def to_param(data_channel_close) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(data_channel_close.source),
      "timestamp" => data_channel_close.timestamp,
      "timestampMillis" => data_channel_close.timestamp_millis,
      "tags" => Enum.map(data_channel_close.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => data_channel_close.type,
      "channelId" => data_channel_close.channel_id
    }
  end
end