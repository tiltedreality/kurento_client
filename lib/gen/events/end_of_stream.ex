defmodule Kurento.Event.EndOfStream do
  @moduledoc """
  Event raised when the stream that the element sends out is finished.

  RaiseBase > Media > EndOfStream

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.EndOfStream{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"]
    }
  end

  @doc false
  def to_param(end_of_stream) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(end_of_stream.source),
      "timestamp" => end_of_stream.timestamp,
      "timestampMillis" => end_of_stream.timestamp_millis,
      "tags" => Enum.map(end_of_stream.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => end_of_stream.type
    }
  end
end