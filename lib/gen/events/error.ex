defmodule Kurento.Event.Error do
  @moduledoc """
  Fired whenever an undefined error related to the MediaObject has occurred

  RaiseBase > Error

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `description:` `String` - Textual description of the error
  * `errorCode:` `int` - Server side integer error code
  * `type:` `String` - Integer code as a String


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :description, :error_code, :type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          description: String.t(),
          error_code: integer(),
          type: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.Error{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      description: params["description"],
      error_code: params["errorCode"],
      type: params["type"]
    }
  end

  @doc false
  def to_param(error) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(error.source),
      "timestamp" => error.timestamp,
      "timestampMillis" => error.timestamp_millis,
      "tags" => Enum.map(error.tags, &Kurento.Struct.Tag.to_param(&1)),
      "description" => error.description,
      "errorCode" => error.error_code,
      "type" => error.type
    }
  end
end