defmodule Kurento.Event.NewCandidatePairSelected do
  @moduledoc """
  Event fired when a new pair of ICE candidates is used by the ICE library. This could also happen in the middle of a session, though not likely.

  RaiseBase > Media > NewCandidatePairSelected

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `candidatePair:` `Kurento.Struct.IceCandidatePair` - The new pair of candidates


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :candidate_pair]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          candidate_pair: Kurento.Struct.IceCandidatePair.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.NewCandidatePairSelected{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      candidate_pair: Kurento.Struct.IceCandidatePair.from_param(client, params["candidatePair"])
    }
  end

  @doc false
  def to_param(new_candidate_pair_selected) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(new_candidate_pair_selected.source),
      "timestamp" => new_candidate_pair_selected.timestamp,
      "timestampMillis" => new_candidate_pair_selected.timestamp_millis,
      "tags" => Enum.map(new_candidate_pair_selected.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => new_candidate_pair_selected.type,
      "candidatePair" =>
        Kurento.Struct.IceCandidatePair.to_param(new_candidate_pair_selected.candidate_pair)
    }
  end
end