defmodule Kurento.Event.MediaTranscodingStateChange do
  @moduledoc """
  Event fired when an incoming media begins and codec transcoding is either required or not.

  RaiseBase > Media > MediaTranscodingStateChange

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `state:` `Kurento.Enum.MediaTranscodingState` - Current transcoding state; either enabled or disabled.
  * `binName:` `String` - Name of the GStreamer bin which is processing the media.
  * `mediaType:` `Kurento.Enum.MediaType` - Type of media that is being processed; either audio or video.


  """
  defstruct [:source, :timestamp, :timestamp_millis, :tags, :type, :state, :bin_name, :media_type]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          state: Kurento.Enum.MediaTranscodingState.t(),
          bin_name: String.t(),
          media_type: Kurento.Enum.MediaType.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.MediaTranscodingStateChange{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      state: Kurento.Enum.MediaTranscodingState.from_param(client, params["state"]),
      bin_name: params["binName"],
      media_type: Kurento.Enum.MediaType.from_param(client, params["mediaType"])
    }
  end

  @doc false
  def to_param(media_transcoding_state_change) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(media_transcoding_state_change.source),
      "timestamp" => media_transcoding_state_change.timestamp,
      "timestampMillis" => media_transcoding_state_change.timestamp_millis,
      "tags" => Enum.map(media_transcoding_state_change.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => media_transcoding_state_change.type,
      "state" =>
        Kurento.Enum.MediaTranscodingState.to_param(media_transcoding_state_change.state),
      "binName" => media_transcoding_state_change.bin_name,
      "mediaType" => Kurento.Enum.MediaType.to_param(media_transcoding_state_change.media_type)
    }
  end
end