defmodule Kurento.Event.OnIceComponentStateChanged do
  @moduledoc """
  Event fired when and ICE component state changes. See :rom:cls:`IceComponentState` for a list of possible states. @deprecated Use `IceComponentStateChange` instead.

  RaiseBase > Media > OnIceComponentStateChanged

  * `source:` `Kurento.Remote.MediaObject` - Object that raised the event
  * `timestamp:` `String` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `String` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `tags:` `Tag[]` - 


  * `type:` `String` - Type of event that was raised


  * `streamId:` `int` - The ID of the stream
  * `componentId:` `int` - The ID of the component
  * `state:` `Kurento.Enum.IceComponentState` - The state of the component


  """
  defstruct [
    :source,
    :timestamp,
    :timestamp_millis,
    :tags,
    :type,
    :stream_id,
    :component_id,
    :state
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaObject.t(),
          timestamp: String.t(),
          timestamp_millis: String.t(),
          tags: [Kurento.Struct.Tag.t()],
          type: String.t(),
          stream_id: integer(),
          component_id: integer(),
          state: Kurento.Enum.IceComponentState.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Event.OnIceComponentStateChanged{
      source: Kurento.Remote.MediaObject.from_param(client, params["source"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      tags: Enum.map(params["tags"], &Kurento.Struct.Tag.from_param(client, &1)),
      type: params["type"],
      stream_id: params["streamId"],
      component_id: params["componentId"],
      state: Kurento.Enum.IceComponentState.from_param(client, params["state"])
    }
  end

  @doc false
  def to_param(on_ice_component_state_changed) do
    %{
      "source" => Kurento.Remote.MediaObject.to_param(on_ice_component_state_changed.source),
      "timestamp" => on_ice_component_state_changed.timestamp,
      "timestampMillis" => on_ice_component_state_changed.timestamp_millis,
      "tags" => Enum.map(on_ice_component_state_changed.tags, &Kurento.Struct.Tag.to_param(&1)),
      "type" => on_ice_component_state_changed.type,
      "streamId" => on_ice_component_state_changed.stream_id,
      "componentId" => on_ice_component_state_changed.component_id,
      "state" => Kurento.Enum.IceComponentState.to_param(on_ice_component_state_changed.state)
    }
  end
end