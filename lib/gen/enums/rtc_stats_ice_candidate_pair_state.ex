defmodule Kurento.Enum.RTCStatsIceCandidatePairState do
  @moduledoc """
  Represents the state of the checklist for the local and remote candidates in a pair.
  """

  @type t :: :frozen | :waiting | :inprogress | :failed | :succeeded | :cancelled

  @spec frozen :: :frozen
  def frozen() do
    :frozen
  end

  @spec waiting :: :waiting
  def waiting() do
    :waiting
  end

  @spec inprogress :: :inprogress
  def inprogress() do
    :inprogress
  end

  @spec failed :: :failed
  def failed() do
    :failed
  end

  @spec succeeded :: :succeeded
  def succeeded() do
    :succeeded
  end

  @spec cancelled :: :cancelled
  def cancelled() do
    :cancelled
  end

  @spec values :: [Kurento.Enum.RTCStatsIceCandidatePairState.t()]
  def values() do
    [:frozen, :waiting, :inprogress, :failed, :succeeded, :cancelled]
  end

  @doc false
  def to_param(:frozen) do
    "frozen"
  end

  @doc false
  def to_param(:waiting) do
    "waiting"
  end

  @doc false
  def to_param(:inprogress) do
    "inprogress"
  end

  @doc false
  def to_param(:failed) do
    "failed"
  end

  @doc false
  def to_param(:succeeded) do
    "succeeded"
  end

  @doc false
  def to_param(:cancelled) do
    "cancelled"
  end

  @doc false
  def from_param(_client, "frozen") do
    :frozen
  end

  @doc false
  def from_param(_client, "waiting") do
    :waiting
  end

  @doc false
  def from_param(_client, "inprogress") do
    :inprogress
  end

  @doc false
  def from_param(_client, "failed") do
    :failed
  end

  @doc false
  def from_param(_client, "succeeded") do
    :succeeded
  end

  @doc false
  def from_param(_client, "cancelled") do
    :cancelled
  end
end