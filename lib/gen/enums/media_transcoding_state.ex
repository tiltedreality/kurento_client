defmodule Kurento.Enum.MediaTranscodingState do
  @moduledoc """
  Transcoding state for a media.
  """

  @type t :: :transcoding | :not_transcoding

  @spec transcoding :: :transcoding
  def transcoding() do
    :transcoding
  end

  @spec not_transcoding :: :not_transcoding
  def not_transcoding() do
    :not_transcoding
  end

  @spec values :: [Kurento.Enum.MediaTranscodingState.t()]
  def values() do
    [:transcoding, :not_transcoding]
  end

  @doc false
  def to_param(:transcoding) do
    "TRANSCODING"
  end

  @doc false
  def to_param(:not_transcoding) do
    "NOT_TRANSCODING"
  end

  @doc false
  def from_param(_client, "TRANSCODING") do
    :transcoding
  end

  @doc false
  def from_param(_client, "NOT_TRANSCODING") do
    :not_transcoding
  end
end