defmodule Kurento.Enum.CryptoSuite do
  @moduledoc """
  Describes the encryption and authentication algorithms
  """

  @type t ::
          :aes_128_cm_hmac_sh_a1_32
          | :aes_128_cm_hmac_sh_a1_80
          | :aes_256_cm_hmac_sh_a1_32
          | :aes_256_cm_hmac_sh_a1_80

  @spec aes_128_cm_hmac_sh_a1_32 :: :aes_128_cm_hmac_sh_a1_32
  def aes_128_cm_hmac_sh_a1_32() do
    :aes_128_cm_hmac_sh_a1_32
  end

  @spec aes_128_cm_hmac_sh_a1_80 :: :aes_128_cm_hmac_sh_a1_80
  def aes_128_cm_hmac_sh_a1_80() do
    :aes_128_cm_hmac_sh_a1_80
  end

  @spec aes_256_cm_hmac_sh_a1_32 :: :aes_256_cm_hmac_sh_a1_32
  def aes_256_cm_hmac_sh_a1_32() do
    :aes_256_cm_hmac_sh_a1_32
  end

  @spec aes_256_cm_hmac_sh_a1_80 :: :aes_256_cm_hmac_sh_a1_80
  def aes_256_cm_hmac_sh_a1_80() do
    :aes_256_cm_hmac_sh_a1_80
  end

  @spec values :: [Kurento.Enum.CryptoSuite.t()]
  def values() do
    [
      :aes_128_cm_hmac_sh_a1_32,
      :aes_128_cm_hmac_sh_a1_80,
      :aes_256_cm_hmac_sh_a1_32,
      :aes_256_cm_hmac_sh_a1_80
    ]
  end

  @doc false
  def to_param(:aes_128_cm_hmac_sh_a1_32) do
    "AES_128_CM_HMAC_SHA1_32"
  end

  @doc false
  def to_param(:aes_128_cm_hmac_sh_a1_80) do
    "AES_128_CM_HMAC_SHA1_80"
  end

  @doc false
  def to_param(:aes_256_cm_hmac_sh_a1_32) do
    "AES_256_CM_HMAC_SHA1_32"
  end

  @doc false
  def to_param(:aes_256_cm_hmac_sh_a1_80) do
    "AES_256_CM_HMAC_SHA1_80"
  end

  @doc false
  def from_param(_client, "AES_128_CM_HMAC_SHA1_32") do
    :aes_128_cm_hmac_sh_a1_32
  end

  @doc false
  def from_param(_client, "AES_128_CM_HMAC_SHA1_80") do
    :aes_128_cm_hmac_sh_a1_80
  end

  @doc false
  def from_param(_client, "AES_256_CM_HMAC_SHA1_32") do
    :aes_256_cm_hmac_sh_a1_32
  end

  @doc false
  def from_param(_client, "AES_256_CM_HMAC_SHA1_80") do
    :aes_256_cm_hmac_sh_a1_80
  end
end