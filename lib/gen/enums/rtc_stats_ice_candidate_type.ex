defmodule Kurento.Enum.RTCStatsIceCandidateType do
  @moduledoc """
  Types of candidates
  """

  @type t :: :host | :serverreflexive | :peerreflexive | :relayed

  @spec host :: :host
  def host() do
    :host
  end

  @spec serverreflexive :: :serverreflexive
  def serverreflexive() do
    :serverreflexive
  end

  @spec peerreflexive :: :peerreflexive
  def peerreflexive() do
    :peerreflexive
  end

  @spec relayed :: :relayed
  def relayed() do
    :relayed
  end

  @spec values :: [Kurento.Enum.RTCStatsIceCandidateType.t()]
  def values() do
    [:host, :serverreflexive, :peerreflexive, :relayed]
  end

  @doc false
  def to_param(:host) do
    "host"
  end

  @doc false
  def to_param(:serverreflexive) do
    "serverreflexive"
  end

  @doc false
  def to_param(:peerreflexive) do
    "peerreflexive"
  end

  @doc false
  def to_param(:relayed) do
    "relayed"
  end

  @doc false
  def from_param(_client, "host") do
    :host
  end

  @doc false
  def from_param(_client, "serverreflexive") do
    :serverreflexive
  end

  @doc false
  def from_param(_client, "peerreflexive") do
    :peerreflexive
  end

  @doc false
  def from_param(_client, "relayed") do
    :relayed
  end
end