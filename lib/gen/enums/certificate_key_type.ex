defmodule Kurento.Enum.CertificateKeyType do
  @moduledoc """
  .
  """

  @type t :: :rsa | :ecdsa

  @spec rsa :: :rsa
  def rsa() do
    :rsa
  end

  @spec ecdsa :: :ecdsa
  def ecdsa() do
    :ecdsa
  end

  @spec values :: [Kurento.Enum.CertificateKeyType.t()]
  def values() do
    [:rsa, :ecdsa]
  end

  @doc false
  def to_param(:rsa) do
    "RSA"
  end

  @doc false
  def to_param(:ecdsa) do
    "ECDSA"
  end

  @doc false
  def from_param(_client, "RSA") do
    :rsa
  end

  @doc false
  def from_param(_client, "ECDSA") do
    :ecdsa
  end
end