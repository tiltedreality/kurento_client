defmodule Kurento.Enum.ServerType do
  @moduledoc """
  Indicates if the server is a real media server or a proxy
  """

  @type t :: :kms | :kcs

  @spec kms :: :kms
  def kms() do
    :kms
  end

  @spec kcs :: :kcs
  def kcs() do
    :kcs
  end

  @spec values :: [Kurento.Enum.ServerType.t()]
  def values() do
    [:kms, :kcs]
  end

  @doc false
  def to_param(:kms) do
    "KMS"
  end

  @doc false
  def to_param(:kcs) do
    "KCS"
  end

  @doc false
  def from_param(_client, "KMS") do
    :kms
  end

  @doc false
  def from_param(_client, "KCS") do
    :kcs
  end
end