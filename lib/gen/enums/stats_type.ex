defmodule Kurento.Enum.StatsType do
  @moduledoc """
  The type of the object.
  """

  @type t ::
          :inboundrtp
          | :outboundrtp
          | :session
          | :datachannel
          | :track
          | :transport
          | :candidatepair
          | :localcandidate
          | :remotecandidate
          | :element
          | :endpoint

  @spec inboundrtp :: :inboundrtp
  def inboundrtp() do
    :inboundrtp
  end

  @spec outboundrtp :: :outboundrtp
  def outboundrtp() do
    :outboundrtp
  end

  @spec session :: :session
  def session() do
    :session
  end

  @spec datachannel :: :datachannel
  def datachannel() do
    :datachannel
  end

  @spec track :: :track
  def track() do
    :track
  end

  @spec transport :: :transport
  def transport() do
    :transport
  end

  @spec candidatepair :: :candidatepair
  def candidatepair() do
    :candidatepair
  end

  @spec localcandidate :: :localcandidate
  def localcandidate() do
    :localcandidate
  end

  @spec remotecandidate :: :remotecandidate
  def remotecandidate() do
    :remotecandidate
  end

  @spec element :: :element
  def element() do
    :element
  end

  @spec endpoint :: :endpoint
  def endpoint() do
    :endpoint
  end

  @spec values :: [Kurento.Enum.StatsType.t()]
  def values() do
    [
      :inboundrtp,
      :outboundrtp,
      :session,
      :datachannel,
      :track,
      :transport,
      :candidatepair,
      :localcandidate,
      :remotecandidate,
      :element,
      :endpoint
    ]
  end

  @doc false
  def to_param(:inboundrtp) do
    "inboundrtp"
  end

  @doc false
  def to_param(:outboundrtp) do
    "outboundrtp"
  end

  @doc false
  def to_param(:session) do
    "session"
  end

  @doc false
  def to_param(:datachannel) do
    "datachannel"
  end

  @doc false
  def to_param(:track) do
    "track"
  end

  @doc false
  def to_param(:transport) do
    "transport"
  end

  @doc false
  def to_param(:candidatepair) do
    "candidatepair"
  end

  @doc false
  def to_param(:localcandidate) do
    "localcandidate"
  end

  @doc false
  def to_param(:remotecandidate) do
    "remotecandidate"
  end

  @doc false
  def to_param(:element) do
    "element"
  end

  @doc false
  def to_param(:endpoint) do
    "endpoint"
  end

  @doc false
  def from_param(_client, "inboundrtp") do
    :inboundrtp
  end

  @doc false
  def from_param(_client, "outboundrtp") do
    :outboundrtp
  end

  @doc false
  def from_param(_client, "session") do
    :session
  end

  @doc false
  def from_param(_client, "datachannel") do
    :datachannel
  end

  @doc false
  def from_param(_client, "track") do
    :track
  end

  @doc false
  def from_param(_client, "transport") do
    :transport
  end

  @doc false
  def from_param(_client, "candidatepair") do
    :candidatepair
  end

  @doc false
  def from_param(_client, "localcandidate") do
    :localcandidate
  end

  @doc false
  def from_param(_client, "remotecandidate") do
    :remotecandidate
  end

  @doc false
  def from_param(_client, "element") do
    :element
  end

  @doc false
  def from_param(_client, "endpoint") do
    :endpoint
  end
end