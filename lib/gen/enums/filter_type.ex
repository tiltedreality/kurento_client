defmodule Kurento.Enum.FilterType do
  @moduledoc """
  Type of filter to be created. Can take the values AUDIO, VIDEO or AUTODETECT.
  """

  @type t :: :audio | :autodetect | :video

  @spec audio :: :audio
  def audio() do
    :audio
  end

  @spec autodetect :: :autodetect
  def autodetect() do
    :autodetect
  end

  @spec video :: :video
  def video() do
    :video
  end

  @spec values :: [Kurento.Enum.FilterType.t()]
  def values() do
    [:audio, :autodetect, :video]
  end

  @doc false
  def to_param(:audio) do
    "AUDIO"
  end

  @doc false
  def to_param(:autodetect) do
    "AUTODETECT"
  end

  @doc false
  def to_param(:video) do
    "VIDEO"
  end

  @doc false
  def from_param(_client, "AUDIO") do
    :audio
  end

  @doc false
  def from_param(_client, "AUTODETECT") do
    :autodetect
  end

  @doc false
  def from_param(_client, "VIDEO") do
    :video
  end
end