defmodule Kurento.Enum.IceComponentState do
  @moduledoc """
  States of an ICE component.
  """

  @type t :: :disconnected | :gathering | :connecting | :connected | :ready | :failed

  @spec disconnected :: :disconnected
  def disconnected() do
    :disconnected
  end

  @spec gathering :: :gathering
  def gathering() do
    :gathering
  end

  @spec connecting :: :connecting
  def connecting() do
    :connecting
  end

  @spec connected :: :connected
  def connected() do
    :connected
  end

  @spec ready :: :ready
  def ready() do
    :ready
  end

  @spec failed :: :failed
  def failed() do
    :failed
  end

  @spec values :: [Kurento.Enum.IceComponentState.t()]
  def values() do
    [:disconnected, :gathering, :connecting, :connected, :ready, :failed]
  end

  @doc false
  def to_param(:disconnected) do
    "DISCONNECTED"
  end

  @doc false
  def to_param(:gathering) do
    "GATHERING"
  end

  @doc false
  def to_param(:connecting) do
    "CONNECTING"
  end

  @doc false
  def to_param(:connected) do
    "CONNECTED"
  end

  @doc false
  def to_param(:ready) do
    "READY"
  end

  @doc false
  def to_param(:failed) do
    "FAILED"
  end

  @doc false
  def from_param(_client, "DISCONNECTED") do
    :disconnected
  end

  @doc false
  def from_param(_client, "GATHERING") do
    :gathering
  end

  @doc false
  def from_param(_client, "CONNECTING") do
    :connecting
  end

  @doc false
  def from_param(_client, "CONNECTED") do
    :connected
  end

  @doc false
  def from_param(_client, "READY") do
    :ready
  end

  @doc false
  def from_param(_client, "FAILED") do
    :failed
  end
end