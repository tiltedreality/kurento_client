defmodule Kurento.Enum.AudioCodec do
  @moduledoc """
  Codec used for transmission of audio.
  """

  @type t :: :opus | :pcmu | :raw

  @spec opus :: :opus
  def opus() do
    :opus
  end

  @spec pcmu :: :pcmu
  def pcmu() do
    :pcmu
  end

  @spec raw :: :raw
  def raw() do
    :raw
  end

  @spec values :: [Kurento.Enum.AudioCodec.t()]
  def values() do
    [:opus, :pcmu, :raw]
  end

  @doc false
  def to_param(:opus) do
    "OPUS"
  end

  @doc false
  def to_param(:pcmu) do
    "PCMU"
  end

  @doc false
  def to_param(:raw) do
    "RAW"
  end

  @doc false
  def from_param(_client, "OPUS") do
    :opus
  end

  @doc false
  def from_param(_client, "PCMU") do
    :pcmu
  end

  @doc false
  def from_param(_client, "RAW") do
    :raw
  end
end