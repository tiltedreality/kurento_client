defmodule Kurento.Enum.GstreamerDotDetails do
  @moduledoc """
  Details of gstreamer dot graphs
  """

  @type t ::
          :show_media_type
          | :show_caps_details
          | :show_non_default_params
          | :show_states
          | :show_full_params
          | :show_all
          | :show_verbose

  @spec show_media_type :: :show_media_type
  def show_media_type() do
    :show_media_type
  end

  @spec show_caps_details :: :show_caps_details
  def show_caps_details() do
    :show_caps_details
  end

  @spec show_non_default_params :: :show_non_default_params
  def show_non_default_params() do
    :show_non_default_params
  end

  @spec show_states :: :show_states
  def show_states() do
    :show_states
  end

  @spec show_full_params :: :show_full_params
  def show_full_params() do
    :show_full_params
  end

  @spec show_all :: :show_all
  def show_all() do
    :show_all
  end

  @spec show_verbose :: :show_verbose
  def show_verbose() do
    :show_verbose
  end

  @spec values :: [Kurento.Enum.GstreamerDotDetails.t()]
  def values() do
    [
      :show_media_type,
      :show_caps_details,
      :show_non_default_params,
      :show_states,
      :show_full_params,
      :show_all,
      :show_verbose
    ]
  end

  @doc false
  def to_param(:show_media_type) do
    "SHOW_MEDIA_TYPE"
  end

  @doc false
  def to_param(:show_caps_details) do
    "SHOW_CAPS_DETAILS"
  end

  @doc false
  def to_param(:show_non_default_params) do
    "SHOW_NON_DEFAULT_PARAMS"
  end

  @doc false
  def to_param(:show_states) do
    "SHOW_STATES"
  end

  @doc false
  def to_param(:show_full_params) do
    "SHOW_FULL_PARAMS"
  end

  @doc false
  def to_param(:show_all) do
    "SHOW_ALL"
  end

  @doc false
  def to_param(:show_verbose) do
    "SHOW_VERBOSE"
  end

  @doc false
  def from_param(_client, "SHOW_MEDIA_TYPE") do
    :show_media_type
  end

  @doc false
  def from_param(_client, "SHOW_CAPS_DETAILS") do
    :show_caps_details
  end

  @doc false
  def from_param(_client, "SHOW_NON_DEFAULT_PARAMS") do
    :show_non_default_params
  end

  @doc false
  def from_param(_client, "SHOW_STATES") do
    :show_states
  end

  @doc false
  def from_param(_client, "SHOW_FULL_PARAMS") do
    :show_full_params
  end

  @doc false
  def from_param(_client, "SHOW_ALL") do
    :show_all
  end

  @doc false
  def from_param(_client, "SHOW_VERBOSE") do
    :show_verbose
  end
end