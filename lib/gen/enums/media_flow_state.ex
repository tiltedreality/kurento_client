defmodule Kurento.Enum.MediaFlowState do
  @moduledoc """
  Flowing state of the media.
  """

  @type t :: :flowing | :not_flowing

  @spec flowing :: :flowing
  def flowing() do
    :flowing
  end

  @spec not_flowing :: :not_flowing
  def not_flowing() do
    :not_flowing
  end

  @spec values :: [Kurento.Enum.MediaFlowState.t()]
  def values() do
    [:flowing, :not_flowing]
  end

  @doc false
  def to_param(:flowing) do
    "FLOWING"
  end

  @doc false
  def to_param(:not_flowing) do
    "NOT_FLOWING"
  end

  @doc false
  def from_param(_client, "FLOWING") do
    :flowing
  end

  @doc false
  def from_param(_client, "NOT_FLOWING") do
    :not_flowing
  end
end