defmodule Kurento.Enum.GapsFixMethod do
  @moduledoc """
  How to fix gaps when they are found in the recorded stream. 

  Gaps are typically caused by packet loss in the input streams, such as when an RTP or WebRTC media flow suffers from network congestion and some packets don't arrive at the media server. 

  Different ways of handling gaps have different tradeoffs:

  *  **NONE**: Do not fix gaps. This means that the resulting files will contain gaps in the timestamps. Some players are clever enough to adapt to this during playback, so that the gaps are reduced to a minimum and no problems are perceived by the user; other players are not so sophisticated, and will struggle trying to decode a file that contains gaps. For example, trying to play such a file directly with Chrome will cause lipsync issues (audio and video out of sync). 

  For example, assume a session length of 15 seconds: packets arrive correctly during the first 5 seconds, then there is a gap, then data arrives again for the last 5 seconds. Also, for simplicity, assume 1 frame per second. With no fix for gaps, the RecorderEndpoint will store each frame as-is, with these timestamps: 

  <pre> frame 1  - 00:01 frame 2  - 00:02 frame 3  - 00:03 frame 4  - 00:04 frame 5  - 00:05 frame 11 - 00:11 frame 12 - 00:12 frame 13 - 00:13 frame 14 - 00:14 frame 15 - 00:15 </pre> 

  Notice how the frames between 6 to 10 are missing, but the last 5 frames still conserve their original timestamp. The total length of the file is detected as 15 seconds by most players, although playback could stutter or hang during the missing section. 

  *  **GENPTS**: Replace timestamps of all frames arriving after a gap. With this method, the length of each gap will be taken into account, to be subtracted from the timestamp of all following frames. This provides for a continuous, gap-less recording, at the expense of reducing the total apparent length of each file. 

  In our example, the RecorderEndpoint will change all timestamps that follow a gap in the stream, and store each frame as follows: 

  <pre> frame 1  - 00:01 frame 2  - 00:02 frame 3  - 00:03 frame 4  - 00:04 frame 5  - 00:05 frame 11 - 00:06 frame 12 - 00:07 frame 13 - 00:08 frame 14 - 00:09 frame 15 - 00:10 </pre> 

  Notice how the frames between 6 to 10 are missing, and the last 5 frames have their timestamps corrected to provide a smooth increment over the previous ones. The total length of the file is detected as 10 seconds, and playback should be correct throughout the whole file. 

  *  **FILL_IF_TRANSCODING**: NOT IMPLEMENTED. 

  This is a proposal for future improvement of the RecorderEndpoint.

  It is possible to perform a dynamic adaptation of audio rate and add frame duplication to the video, such that the missing parts are filled with artificial data. This has the advantage of providing a smooth playback result, and at the same time conserving all original timestamps. 

  However, the main issue with this method is that it requires accessing the decoded media; i.e., transcoding must be active. For this reason, the proposal is to offer this option to be enabled only when transcoding would still happen anyways. 

  In our example, the RecorderEndpoint would change all missing frames like this: 

  <pre> frame 1  - 00:01 frame 2  - 00:02 frame 3  - 00:03 frame 4  - 00:04 frame 5  - 00:05 fake frame - 00:06 fake frame - 00:07 fake frame - 00:08 fake frame - 00:09 fake frame - 00:10 frame 11 - 00:11 frame 12 - 00:12 frame 13 - 00:13 frame 14 - 00:14 frame 15 - 00:15 </pre> 

  This joins the best of both worlds: on one hand, the playback should be smooth and even the most basic players should be able to handle the recording files without issue. On the other, the total length of the file is left unmodified, so it matches with the expected duration of the sessions that are being recorded.
  """

  @type t :: :none | :genpts | :fill_if_transcoding

  @spec none :: :none
  def none() do
    :none
  end

  @spec genpts :: :genpts
  def genpts() do
    :genpts
  end

  @spec fill_if_transcoding :: :fill_if_transcoding
  def fill_if_transcoding() do
    :fill_if_transcoding
  end

  @spec values :: [Kurento.Enum.GapsFixMethod.t()]
  def values() do
    [:none, :genpts, :fill_if_transcoding]
  end

  @doc false
  def to_param(:none) do
    "NONE"
  end

  @doc false
  def to_param(:genpts) do
    "GENPTS"
  end

  @doc false
  def to_param(:fill_if_transcoding) do
    "FILL_IF_TRANSCODING"
  end

  @doc false
  def from_param(_client, "NONE") do
    :none
  end

  @doc false
  def from_param(_client, "GENPTS") do
    :genpts
  end

  @doc false
  def from_param(_client, "FILL_IF_TRANSCODING") do
    :fill_if_transcoding
  end
end