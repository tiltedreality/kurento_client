defmodule Kurento.Enum.RTCDataChannelState do
  @moduledoc """
  Represents the state of the RTCDataChannel
  """

  @type t :: :connecting | :open | :closing | :closed

  @spec connecting :: :connecting
  def connecting() do
    :connecting
  end

  @spec open :: :open
  def open() do
    :open
  end

  @spec closing :: :closing
  def closing() do
    :closing
  end

  @spec closed :: :closed
  def closed() do
    :closed
  end

  @spec values :: [Kurento.Enum.RTCDataChannelState.t()]
  def values() do
    [:connecting, :open, :closing, :closed]
  end

  @doc false
  def to_param(:connecting) do
    "connecting"
  end

  @doc false
  def to_param(:open) do
    "open"
  end

  @doc false
  def to_param(:closing) do
    "closing"
  end

  @doc false
  def to_param(:closed) do
    "closed"
  end

  @doc false
  def from_param(_client, "connecting") do
    :connecting
  end

  @doc false
  def from_param(_client, "open") do
    :open
  end

  @doc false
  def from_param(_client, "closing") do
    :closing
  end

  @doc false
  def from_param(_client, "closed") do
    :closed
  end
end