defmodule Kurento.Enum.MediaType do
  @moduledoc """
  Type of media stream to be exchanged. Can take the values AUDIO, DATA or VIDEO.
  """

  @type t :: :audio | :data | :video

  @spec audio :: :audio
  def audio() do
    :audio
  end

  @spec data :: :data
  def data() do
    :data
  end

  @spec video :: :video
  def video() do
    :video
  end

  @spec values :: [Kurento.Enum.MediaType.t()]
  def values() do
    [:audio, :data, :video]
  end

  @doc false
  def to_param(:audio) do
    "AUDIO"
  end

  @doc false
  def to_param(:data) do
    "DATA"
  end

  @doc false
  def to_param(:video) do
    "VIDEO"
  end

  @doc false
  def from_param(_client, "AUDIO") do
    :audio
  end

  @doc false
  def from_param(_client, "DATA") do
    :data
  end

  @doc false
  def from_param(_client, "VIDEO") do
    :video
  end
end