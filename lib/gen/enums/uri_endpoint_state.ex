defmodule Kurento.Enum.UriEndpointState do
  @moduledoc """
  State of the endpoint
  """

  @type t :: :stop | :start | :pause

  @spec stop :: :stop
  def stop() do
    :stop
  end

  @spec start :: :start
  def start() do
    :start
  end

  @spec pause :: :pause
  def pause() do
    :pause
  end

  @spec values :: [Kurento.Enum.UriEndpointState.t()]
  def values() do
    [:stop, :start, :pause]
  end

  @doc false
  def to_param(:stop) do
    "STOP"
  end

  @doc false
  def to_param(:start) do
    "START"
  end

  @doc false
  def to_param(:pause) do
    "PAUSE"
  end

  @doc false
  def from_param(_client, "STOP") do
    :stop
  end

  @doc false
  def from_param(_client, "START") do
    :start
  end

  @doc false
  def from_param(_client, "PAUSE") do
    :pause
  end
end