defmodule Kurento.Enum.MediaProfileSpecType do
  @moduledoc """
  Media Profile. Currently WEBM, MKV, MP4 and JPEG are supported.
  """

  @type t ::
          :webm
          | :mkv
          | :m_p4
          | :webm_video_only
          | :webm_audio_only
          | :mkv_video_only
          | :mkv_audio_only
          | :m_p4_video_only
          | :m_p4_audio_only
          | :jpeg_video_only
          | :kurento_split_recorder

  @spec webm :: :webm
  def webm() do
    :webm
  end

  @spec mkv :: :mkv
  def mkv() do
    :mkv
  end

  @spec m_p4 :: :m_p4
  def m_p4() do
    :m_p4
  end

  @spec webm_video_only :: :webm_video_only
  def webm_video_only() do
    :webm_video_only
  end

  @spec webm_audio_only :: :webm_audio_only
  def webm_audio_only() do
    :webm_audio_only
  end

  @spec mkv_video_only :: :mkv_video_only
  def mkv_video_only() do
    :mkv_video_only
  end

  @spec mkv_audio_only :: :mkv_audio_only
  def mkv_audio_only() do
    :mkv_audio_only
  end

  @spec m_p4_video_only :: :m_p4_video_only
  def m_p4_video_only() do
    :m_p4_video_only
  end

  @spec m_p4_audio_only :: :m_p4_audio_only
  def m_p4_audio_only() do
    :m_p4_audio_only
  end

  @spec jpeg_video_only :: :jpeg_video_only
  def jpeg_video_only() do
    :jpeg_video_only
  end

  @spec kurento_split_recorder :: :kurento_split_recorder
  def kurento_split_recorder() do
    :kurento_split_recorder
  end

  @spec values :: [Kurento.Enum.MediaProfileSpecType.t()]
  def values() do
    [
      :webm,
      :mkv,
      :m_p4,
      :webm_video_only,
      :webm_audio_only,
      :mkv_video_only,
      :mkv_audio_only,
      :m_p4_video_only,
      :m_p4_audio_only,
      :jpeg_video_only,
      :kurento_split_recorder
    ]
  end

  @doc false
  def to_param(:webm) do
    "WEBM"
  end

  @doc false
  def to_param(:mkv) do
    "MKV"
  end

  @doc false
  def to_param(:m_p4) do
    "MP4"
  end

  @doc false
  def to_param(:webm_video_only) do
    "WEBM_VIDEO_ONLY"
  end

  @doc false
  def to_param(:webm_audio_only) do
    "WEBM_AUDIO_ONLY"
  end

  @doc false
  def to_param(:mkv_video_only) do
    "MKV_VIDEO_ONLY"
  end

  @doc false
  def to_param(:mkv_audio_only) do
    "MKV_AUDIO_ONLY"
  end

  @doc false
  def to_param(:m_p4_video_only) do
    "MP4_VIDEO_ONLY"
  end

  @doc false
  def to_param(:m_p4_audio_only) do
    "MP4_AUDIO_ONLY"
  end

  @doc false
  def to_param(:jpeg_video_only) do
    "JPEG_VIDEO_ONLY"
  end

  @doc false
  def to_param(:kurento_split_recorder) do
    "KURENTO_SPLIT_RECORDER"
  end

  @doc false
  def from_param(_client, "WEBM") do
    :webm
  end

  @doc false
  def from_param(_client, "MKV") do
    :mkv
  end

  @doc false
  def from_param(_client, "MP4") do
    :m_p4
  end

  @doc false
  def from_param(_client, "WEBM_VIDEO_ONLY") do
    :webm_video_only
  end

  @doc false
  def from_param(_client, "WEBM_AUDIO_ONLY") do
    :webm_audio_only
  end

  @doc false
  def from_param(_client, "MKV_VIDEO_ONLY") do
    :mkv_video_only
  end

  @doc false
  def from_param(_client, "MKV_AUDIO_ONLY") do
    :mkv_audio_only
  end

  @doc false
  def from_param(_client, "MP4_VIDEO_ONLY") do
    :m_p4_video_only
  end

  @doc false
  def from_param(_client, "MP4_AUDIO_ONLY") do
    :m_p4_audio_only
  end

  @doc false
  def from_param(_client, "JPEG_VIDEO_ONLY") do
    :jpeg_video_only
  end

  @doc false
  def from_param(_client, "KURENTO_SPLIT_RECORDER") do
    :kurento_split_recorder
  end
end