defmodule Kurento.Enum.ConnectionState do
  @moduledoc """
  State of the connection.
  """

  @type t :: :disconnected | :connected

  @spec disconnected :: :disconnected
  def disconnected() do
    :disconnected
  end

  @spec connected :: :connected
  def connected() do
    :connected
  end

  @spec values :: [Kurento.Enum.ConnectionState.t()]
  def values() do
    [:disconnected, :connected]
  end

  @doc false
  def to_param(:disconnected) do
    "DISCONNECTED"
  end

  @doc false
  def to_param(:connected) do
    "CONNECTED"
  end

  @doc false
  def from_param(_client, "DISCONNECTED") do
    :disconnected
  end

  @doc false
  def from_param(_client, "CONNECTED") do
    :connected
  end
end