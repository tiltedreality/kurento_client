defmodule Kurento.Enum.VideoCodec do
  @moduledoc """
  Codec used for transmission of video.
  """

  @type t :: :v_p8 | :h264 | :raw

  @spec v_p8 :: :v_p8
  def v_p8() do
    :v_p8
  end

  @spec h264 :: :h264
  def h264() do
    :h264
  end

  @spec raw :: :raw
  def raw() do
    :raw
  end

  @spec values :: [Kurento.Enum.VideoCodec.t()]
  def values() do
    [:v_p8, :h264, :raw]
  end

  @doc false
  def to_param(:v_p8) do
    "VP8"
  end

  @doc false
  def to_param(:h264) do
    "H264"
  end

  @doc false
  def to_param(:raw) do
    "RAW"
  end

  @doc false
  def from_param(_client, "VP8") do
    :v_p8
  end

  @doc false
  def from_param(_client, "H264") do
    :h264
  end

  @doc false
  def from_param(_client, "RAW") do
    :raw
  end
end