defmodule Kurento.Struct.Stats do
  @moduledoc """
  A dictionary that represents the stats gathered.

  Stats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  """
  defstruct [:id, :type, :timestamp, :timestamp_millis]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.Stats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"]
    }
  end

  @doc false
  def to_param(stats) do
    %{
      "id" => stats.id,
      "type" => Kurento.Enum.StatsType.to_param(stats.type),
      "timestamp" => stats.timestamp,
      "timestampMillis" => stats.timestamp_millis
    }
  end
end