defmodule Kurento.Struct.RTCDataChannelStats do
  @moduledoc """
  Statistics related to RTC data channels.

  Stats > RTCStats > RTCDataChannelStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `label:` `String` - The RTCDatachannel label.
  * `protocol:` `String` - The protocol used.
  * `datachannelid:` `int64` - The RTCDatachannel identifier.
  * `state:` `Kurento.Enum.RTCDataChannelState` - The state of the RTCDatachannel.
  * `messagesSent:` `int64` - Represents the total number of API 'message' events sent.
  * `bytesSent:` `int64` - Represents the total number of payload bytes sent on this RTCDatachannel, i.e., not including headers or padding.
  * `messagesReceived:` `int64` - Represents the total number of API 'message' events received.
  * `bytesReceived:` `int64` - Represents the total number of bytes received on this RTCDatachannel, i.e., not including headers or padding.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :label,
    :protocol,
    :datachannelid,
    :state,
    :messages_sent,
    :bytes_sent,
    :messages_received,
    :bytes_received
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          label: String.t(),
          protocol: String.t(),
          datachannelid: integer(),
          state: Kurento.Enum.RTCDataChannelState.t(),
          messages_sent: integer(),
          bytes_sent: integer(),
          messages_received: integer(),
          bytes_received: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCDataChannelStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      label: params["label"],
      protocol: params["protocol"],
      datachannelid: params["datachannelid"],
      state: Kurento.Enum.RTCDataChannelState.from_param(client, params["state"]),
      messages_sent: params["messagesSent"],
      bytes_sent: params["bytesSent"],
      messages_received: params["messagesReceived"],
      bytes_received: params["bytesReceived"]
    }
  end

  @doc false
  def to_param(rtc_data_channel_stats) do
    %{
      "id" => rtc_data_channel_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_data_channel_stats.type),
      "timestamp" => rtc_data_channel_stats.timestamp,
      "timestampMillis" => rtc_data_channel_stats.timestamp_millis,
      "label" => rtc_data_channel_stats.label,
      "protocol" => rtc_data_channel_stats.protocol,
      "datachannelid" => rtc_data_channel_stats.datachannelid,
      "state" => Kurento.Enum.RTCDataChannelState.to_param(rtc_data_channel_stats.state),
      "messagesSent" => rtc_data_channel_stats.messages_sent,
      "bytesSent" => rtc_data_channel_stats.bytes_sent,
      "messagesReceived" => rtc_data_channel_stats.messages_received,
      "bytesReceived" => rtc_data_channel_stats.bytes_received
    }
  end
end