defmodule Kurento.Struct.RTCOutboundRTPStreamStats do
  @moduledoc """
  Statistics that represents the measurement metrics for the outgoing media stream.

  Stats > RTCStats > RTCRTPStreamStats > RTCOutboundRTPStreamStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `ssrc:` `String` - The synchronized source SSRC
  * `associateStatsId:` `String` - The associateStatsId is used for looking up the corresponding (local/remote) RTCStats object for a given SSRC.
  * `isRemote:` `boolean` - false indicates that the statistics are measured locally, while true indicates that the measurements were done at the remote endpoint and reported in an RTCP RR/XR.
  * `mediaTrackId:` `String` - Track identifier.
  * `transportId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCTransportStats associated with this RTP stream.
  * `codecId:` `String` - The codec identifier
  * `firCount:` `int64` - Count the total number of Full Intra Request (FIR) packets received by the sender. This metric is only valid for video and is sent by receiver.
  * `pliCount:` `int64` - Count the total number of Packet Loss Indication (PLI) packets received by the sender and is sent by receiver.
  * `nackCount:` `int64` - Count the total number of Negative ACKnowledgement (NACK) packets received by the sender and is sent by receiver.
  * `sliCount:` `int64` - Count the total number of Slice Loss Indication (SLI) packets received by the sender. This metric is only valid for video and is sent by receiver.
  * `remb:` `int64` - The Receiver Estimated Maximum Bitrate (REMB). This metric is only valid for video.
  * `packetsLost:` `int64` - Total number of RTP packets lost for this SSRC.
  * `fractionLost:` `double` - The fraction packet loss reported for this SSRC.


  * `packetsSent:` `int64` - Total number of RTP packets sent for this SSRC.
  * `bytesSent:` `int64` - Total number of bytes sent for this SSRC.
  * `targetBitrate:` `double` - Presently configured bitrate target of this SSRC, in bits per second.
  * `roundTripTime:` `double` - Estimated round trip time (seconds) for this SSRC based on the RTCP timestamp.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :ssrc,
    :associate_stats_id,
    :is_remote,
    :media_track_id,
    :transport_id,
    :codec_id,
    :fir_count,
    :pli_count,
    :nack_count,
    :sli_count,
    :remb,
    :packets_lost,
    :fraction_lost,
    :packets_sent,
    :bytes_sent,
    :target_bitrate,
    :round_trip_time
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          ssrc: String.t(),
          associate_stats_id: String.t(),
          is_remote: boolean(),
          media_track_id: String.t(),
          transport_id: String.t(),
          codec_id: String.t(),
          fir_count: integer(),
          pli_count: integer(),
          nack_count: integer(),
          sli_count: integer(),
          remb: integer(),
          packets_lost: integer(),
          fraction_lost: float(),
          packets_sent: integer(),
          bytes_sent: integer(),
          target_bitrate: float(),
          round_trip_time: float()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCOutboundRTPStreamStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      ssrc: params["ssrc"],
      associate_stats_id: params["associateStatsId"],
      is_remote: params["isRemote"],
      media_track_id: params["mediaTrackId"],
      transport_id: params["transportId"],
      codec_id: params["codecId"],
      fir_count: params["firCount"],
      pli_count: params["pliCount"],
      nack_count: params["nackCount"],
      sli_count: params["sliCount"],
      remb: params["remb"],
      packets_lost: params["packetsLost"],
      fraction_lost: params["fractionLost"],
      packets_sent: params["packetsSent"],
      bytes_sent: params["bytesSent"],
      target_bitrate: params["targetBitrate"],
      round_trip_time: params["roundTripTime"]
    }
  end

  @doc false
  def to_param(rtc_outbound_rtp_stream_stats) do
    %{
      "id" => rtc_outbound_rtp_stream_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_outbound_rtp_stream_stats.type),
      "timestamp" => rtc_outbound_rtp_stream_stats.timestamp,
      "timestampMillis" => rtc_outbound_rtp_stream_stats.timestamp_millis,
      "ssrc" => rtc_outbound_rtp_stream_stats.ssrc,
      "associateStatsId" => rtc_outbound_rtp_stream_stats.associate_stats_id,
      "isRemote" => rtc_outbound_rtp_stream_stats.is_remote,
      "mediaTrackId" => rtc_outbound_rtp_stream_stats.media_track_id,
      "transportId" => rtc_outbound_rtp_stream_stats.transport_id,
      "codecId" => rtc_outbound_rtp_stream_stats.codec_id,
      "firCount" => rtc_outbound_rtp_stream_stats.fir_count,
      "pliCount" => rtc_outbound_rtp_stream_stats.pli_count,
      "nackCount" => rtc_outbound_rtp_stream_stats.nack_count,
      "sliCount" => rtc_outbound_rtp_stream_stats.sli_count,
      "remb" => rtc_outbound_rtp_stream_stats.remb,
      "packetsLost" => rtc_outbound_rtp_stream_stats.packets_lost,
      "fractionLost" => rtc_outbound_rtp_stream_stats.fraction_lost,
      "packetsSent" => rtc_outbound_rtp_stream_stats.packets_sent,
      "bytesSent" => rtc_outbound_rtp_stream_stats.bytes_sent,
      "targetBitrate" => rtc_outbound_rtp_stream_stats.target_bitrate,
      "roundTripTime" => rtc_outbound_rtp_stream_stats.round_trip_time
    }
  end
end