defmodule Kurento.Struct.RTCMediaStreamTrackStats do
  @moduledoc """
  Statistics related to the media stream.

  Stats > RTCStats > RTCMediaStreamTrackStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `trackIdentifier:` `String` - Represents the track.id property.
  * `remoteSource:` `boolean` - true indicates that this is a remote source. false in other case.
  * `ssrcIds:` `String[]` - Synchronized sources.
  * `frameWidth:` `int64` - Only makes sense for video media streams and represents the width of the video frame for this SSRC.
  * `frameHeight:` `int64` - Only makes sense for video media streams and represents the height of the video frame for this SSRC.
  * `framesPerSecond:` `double` - Only valid for video. It represents the nominal FPS value.
  * `framesSent:` `int64` - Only valid for video. It represents the total number of frames sent for this SSRC.
  * `framesReceived:` `int64` - Only valid for video and when remoteSource is set to true. It represents the total number of frames received for this SSRC.
  * `framesDecoded:` `int64` - Only valid for video. It represents the total number of frames correctly decoded for this SSRC.
  * `framesDropped:` `int64` - Only valid for video. The total number of frames dropped predecode or dropped because the frame missed its display deadline.
  * `framesCorrupted:` `int64` - Only valid for video. The total number of corrupted frames that have been detected.
  * `audioLevel:` `double` - Only valid for audio, and the value is between 0..1 (linear), where 1.0 represents 0 dBov.
  * `echoReturnLoss:` `double` - Only present on audio tracks sourced from a microphone where echo cancellation is applied. Calculated in decibels.
  * `echoReturnLossEnhancement:` `double` - Only present on audio tracks sourced from a microphone where echo cancellation is applied.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :track_identifier,
    :remote_source,
    :ssrc_ids,
    :frame_width,
    :frame_height,
    :frames_per_second,
    :frames_sent,
    :frames_received,
    :frames_decoded,
    :frames_dropped,
    :frames_corrupted,
    :audio_level,
    :echo_return_loss,
    :echo_return_loss_enhancement
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          track_identifier: String.t(),
          remote_source: boolean(),
          ssrc_ids: [String.t()],
          frame_width: integer(),
          frame_height: integer(),
          frames_per_second: float(),
          frames_sent: integer(),
          frames_received: integer(),
          frames_decoded: integer(),
          frames_dropped: integer(),
          frames_corrupted: integer(),
          audio_level: float(),
          echo_return_loss: float(),
          echo_return_loss_enhancement: float()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCMediaStreamTrackStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      track_identifier: params["trackIdentifier"],
      remote_source: params["remoteSource"],
      ssrc_ids: params["ssrcIds"],
      frame_width: params["frameWidth"],
      frame_height: params["frameHeight"],
      frames_per_second: params["framesPerSecond"],
      frames_sent: params["framesSent"],
      frames_received: params["framesReceived"],
      frames_decoded: params["framesDecoded"],
      frames_dropped: params["framesDropped"],
      frames_corrupted: params["framesCorrupted"],
      audio_level: params["audioLevel"],
      echo_return_loss: params["echoReturnLoss"],
      echo_return_loss_enhancement: params["echoReturnLossEnhancement"]
    }
  end

  @doc false
  def to_param(rtc_media_stream_track_stats) do
    %{
      "id" => rtc_media_stream_track_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_media_stream_track_stats.type),
      "timestamp" => rtc_media_stream_track_stats.timestamp,
      "timestampMillis" => rtc_media_stream_track_stats.timestamp_millis,
      "trackIdentifier" => rtc_media_stream_track_stats.track_identifier,
      "remoteSource" => rtc_media_stream_track_stats.remote_source,
      "ssrcIds" => rtc_media_stream_track_stats.ssrc_ids,
      "frameWidth" => rtc_media_stream_track_stats.frame_width,
      "frameHeight" => rtc_media_stream_track_stats.frame_height,
      "framesPerSecond" => rtc_media_stream_track_stats.frames_per_second,
      "framesSent" => rtc_media_stream_track_stats.frames_sent,
      "framesReceived" => rtc_media_stream_track_stats.frames_received,
      "framesDecoded" => rtc_media_stream_track_stats.frames_decoded,
      "framesDropped" => rtc_media_stream_track_stats.frames_dropped,
      "framesCorrupted" => rtc_media_stream_track_stats.frames_corrupted,
      "audioLevel" => rtc_media_stream_track_stats.audio_level,
      "echoReturnLoss" => rtc_media_stream_track_stats.echo_return_loss,
      "echoReturnLossEnhancement" => rtc_media_stream_track_stats.echo_return_loss_enhancement
    }
  end
end