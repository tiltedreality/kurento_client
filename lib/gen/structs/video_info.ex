defmodule Kurento.Struct.VideoInfo do
  @moduledoc """


  VideoInfo

  * `isSeekable:` `boolean` - Seek is possible in video source
  * `seekableInit:` `int64` - First video position to do seek in ms
  * `seekableEnd:` `int64` - Last video position to do seek in ms
  * `duration:` `int64` - Video duration in ms


  """
  defstruct [:is_seekable, :seekable_init, :seekable_end, :duration]

  @type t :: %{
          optional(:__struct__) => atom(),
          is_seekable: boolean(),
          seekable_init: integer(),
          seekable_end: integer(),
          duration: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.VideoInfo{
      is_seekable: params["isSeekable"],
      seekable_init: params["seekableInit"],
      seekable_end: params["seekableEnd"],
      duration: params["duration"]
    }
  end

  @doc false
  def to_param(video_info) do
    %{
      "isSeekable" => video_info.is_seekable,
      "seekableInit" => video_info.seekable_init,
      "seekableEnd" => video_info.seekable_end,
      "duration" => video_info.duration
    }
  end
end