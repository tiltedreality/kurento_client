defmodule Kurento.Struct.VideoCaps do
  @moduledoc """
  Format for video media

  VideoCaps

  * `codec:` `Kurento.Enum.VideoCodec` - Video codec
  * `framerate:` `Kurento.Struct.Fraction` - Framerate


  """
  defstruct [:codec, :framerate]

  @type t :: %{
          optional(:__struct__) => atom(),
          codec: Kurento.Enum.VideoCodec.t(),
          framerate: Kurento.Struct.Fraction.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.VideoCaps{
      codec: Kurento.Enum.VideoCodec.from_param(client, params["codec"]),
      framerate: Kurento.Struct.Fraction.from_param(client, params["framerate"])
    }
  end

  @doc false
  def to_param(video_caps) do
    %{
      "codec" => Kurento.Enum.VideoCodec.to_param(video_caps.codec),
      "framerate" => Kurento.Struct.Fraction.to_param(video_caps.framerate)
    }
  end
end