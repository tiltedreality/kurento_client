defmodule Kurento.Struct.CodecConfiguration do
  @moduledoc """
  Defines specific configuration for codecs

  CodecConfiguration

  * `name:` `String` - Name of the codec. Must follow this format: <encoding name>/<clock rate>[/<encoding parameters>]
  * `properties:` `String<>` - String used for tuning codec properties


  """
  defstruct [:name, :properties]

  @type t :: %{
          optional(:__struct__) => atom(),
          name: String.t(),
          properties: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.CodecConfiguration{
      name: params["name"],
      properties: params["properties"]
    }
  end

  @doc false
  def to_param(codec_configuration) do
    %{
      "name" => codec_configuration.name,
      "properties" => codec_configuration.properties
    }
  end
end