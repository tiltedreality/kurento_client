defmodule Kurento.Struct.ElementStats do
  @moduledoc """
  A dictionary that represents the stats gathered in the media element.

  Stats > ElementStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `inputAudioLatency:` `double` - @deprecated Audio average measured on the sink pad in nano seconds
  * `inputVideoLatency:` `double` - @deprecated Video average measured on the sink pad in nano seconds
  * `inputLatency:` `MediaLatencyStat[]` - The average time that buffers take to get on the input pads of this element in nano seconds


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :input_audio_latency,
    :input_video_latency,
    :input_latency
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          input_audio_latency: float(),
          input_video_latency: float(),
          input_latency: [Kurento.Struct.MediaLatencyStat.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.ElementStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      input_audio_latency: params["inputAudioLatency"],
      input_video_latency: params["inputVideoLatency"],
      input_latency:
        Enum.map(params["inputLatency"], &Kurento.Struct.MediaLatencyStat.from_param(client, &1))
    }
  end

  @doc false
  def to_param(element_stats) do
    %{
      "id" => element_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(element_stats.type),
      "timestamp" => element_stats.timestamp,
      "timestampMillis" => element_stats.timestamp_millis,
      "inputAudioLatency" => element_stats.input_audio_latency,
      "inputVideoLatency" => element_stats.input_video_latency,
      "inputLatency" =>
        Enum.map(element_stats.input_latency, &Kurento.Struct.MediaLatencyStat.to_param(&1))
    }
  end
end