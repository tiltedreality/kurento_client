defmodule Kurento.Struct.RTCIceCandidatePairStats do
  @moduledoc """


  Stats > RTCStats > RTCIceCandidatePairStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `transportId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCTransportStats associated with this candidate pair.
  * `localCandidateId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCIceCandidateAttributes for the local candidate associated with this candidate pair.
  * `remoteCandidateId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCIceCandidateAttributes for the remote candidate associated with this candidate pair.
  * `state:` `Kurento.Enum.RTCStatsIceCandidatePairState` - Represents the state of the checklist for the local and remote candidates in a pair.
  * `priority:` `int64` - Calculated from candidate priorities as defined in [RFC5245] section 5.7.2.
  * `nominated:` `boolean` - Related to updating the nominated flag described in Section 7.1.3.2.4 of [RFC5245].
  * `writable:` `boolean` - Has gotten ACK to an ICE request.
  * `readable:` `boolean` - Has gotten a valid incoming ICE request.
  * `bytesSent:` `int64` - Represents the total number of payload bytes sent on this candidate pair, i.e., not including headers or padding.
  * `bytesReceived:` `int64` - Represents the total number of payload bytes received on this candidate pair, i.e., not including headers or padding.
  * `roundTripTime:` `double` - Represents the RTT computed by the STUN connectivity checks
  * `availableOutgoingBitrate:` `double` - Measured in Bits per second, and is implementation dependent. It may be calculated by the underlying congestion control.
  * `availableIncomingBitrate:` `double` - Measured in Bits per second, and is implementation dependent. It may be calculated by the underlying congestion control.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :transport_id,
    :local_candidate_id,
    :remote_candidate_id,
    :state,
    :priority,
    :nominated,
    :writable,
    :readable,
    :bytes_sent,
    :bytes_received,
    :round_trip_time,
    :available_outgoing_bitrate,
    :available_incoming_bitrate
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          transport_id: String.t(),
          local_candidate_id: String.t(),
          remote_candidate_id: String.t(),
          state: Kurento.Enum.RTCStatsIceCandidatePairState.t(),
          priority: integer(),
          nominated: boolean(),
          writable: boolean(),
          readable: boolean(),
          bytes_sent: integer(),
          bytes_received: integer(),
          round_trip_time: float(),
          available_outgoing_bitrate: float(),
          available_incoming_bitrate: float()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCIceCandidatePairStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      transport_id: params["transportId"],
      local_candidate_id: params["localCandidateId"],
      remote_candidate_id: params["remoteCandidateId"],
      state: Kurento.Enum.RTCStatsIceCandidatePairState.from_param(client, params["state"]),
      priority: params["priority"],
      nominated: params["nominated"],
      writable: params["writable"],
      readable: params["readable"],
      bytes_sent: params["bytesSent"],
      bytes_received: params["bytesReceived"],
      round_trip_time: params["roundTripTime"],
      available_outgoing_bitrate: params["availableOutgoingBitrate"],
      available_incoming_bitrate: params["availableIncomingBitrate"]
    }
  end

  @doc false
  def to_param(rtc_ice_candidate_pair_stats) do
    %{
      "id" => rtc_ice_candidate_pair_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_ice_candidate_pair_stats.type),
      "timestamp" => rtc_ice_candidate_pair_stats.timestamp,
      "timestampMillis" => rtc_ice_candidate_pair_stats.timestamp_millis,
      "transportId" => rtc_ice_candidate_pair_stats.transport_id,
      "localCandidateId" => rtc_ice_candidate_pair_stats.local_candidate_id,
      "remoteCandidateId" => rtc_ice_candidate_pair_stats.remote_candidate_id,
      "state" =>
        Kurento.Enum.RTCStatsIceCandidatePairState.to_param(rtc_ice_candidate_pair_stats.state),
      "priority" => rtc_ice_candidate_pair_stats.priority,
      "nominated" => rtc_ice_candidate_pair_stats.nominated,
      "writable" => rtc_ice_candidate_pair_stats.writable,
      "readable" => rtc_ice_candidate_pair_stats.readable,
      "bytesSent" => rtc_ice_candidate_pair_stats.bytes_sent,
      "bytesReceived" => rtc_ice_candidate_pair_stats.bytes_received,
      "roundTripTime" => rtc_ice_candidate_pair_stats.round_trip_time,
      "availableOutgoingBitrate" => rtc_ice_candidate_pair_stats.available_outgoing_bitrate,
      "availableIncomingBitrate" => rtc_ice_candidate_pair_stats.available_incoming_bitrate
    }
  end
end