defmodule Kurento.Struct.IceConnection do
  @moduledoc """
  The ICE connection state for a certain stream and component.

  IceConnection

  * `streamId:` `String` - The ID of the stream
  * `componentId:` `int` - The ID of the component
  * `state:` `Kurento.Enum.IceComponentState` - The state of the component


  """
  defstruct [:stream_id, :component_id, :state]

  @type t :: %{
          optional(:__struct__) => atom(),
          stream_id: String.t(),
          component_id: integer(),
          state: Kurento.Enum.IceComponentState.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.IceConnection{
      stream_id: params["streamId"],
      component_id: params["componentId"],
      state: Kurento.Enum.IceComponentState.from_param(client, params["state"])
    }
  end

  @doc false
  def to_param(ice_connection) do
    %{
      "streamId" => ice_connection.stream_id,
      "componentId" => ice_connection.component_id,
      "state" => Kurento.Enum.IceComponentState.to_param(ice_connection.state)
    }
  end
end