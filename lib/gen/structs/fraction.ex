defmodule Kurento.Struct.Fraction do
  @moduledoc """
  Type that represents a fraction of an integer numerator over an integer denominator

  Fraction

  * `numerator:` `int` - the numerator of the fraction
  * `denominator:` `int` - the denominator of the fraction


  """
  defstruct [:numerator, :denominator]

  @type t :: %{
          optional(:__struct__) => atom(),
          numerator: integer(),
          denominator: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.Fraction{
      numerator: params["numerator"],
      denominator: params["denominator"]
    }
  end

  @doc false
  def to_param(fraction) do
    %{
      "numerator" => fraction.numerator,
      "denominator" => fraction.denominator
    }
  end
end