defmodule Kurento.Struct.AudioCaps do
  @moduledoc """
  Format for audio media

  AudioCaps

  * `codec:` `Kurento.Enum.AudioCodec` - Audio codec
  * `bitrate:` `int` - Bitrate


  """
  defstruct [:codec, :bitrate]

  @type t :: %{
          optional(:__struct__) => atom(),
          codec: Kurento.Enum.AudioCodec.t(),
          bitrate: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.AudioCaps{
      codec: Kurento.Enum.AudioCodec.from_param(client, params["codec"]),
      bitrate: params["bitrate"]
    }
  end

  @doc false
  def to_param(audio_caps) do
    %{
      "codec" => Kurento.Enum.AudioCodec.to_param(audio_caps.codec),
      "bitrate" => audio_caps.bitrate
    }
  end
end