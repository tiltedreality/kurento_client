defmodule Kurento.Struct.Tag do
  @moduledoc """
  Pair key-value with info about a MediaObject

  Tag

  * `key:` `String` - Tag key
  * `value:` `String` - Tag Value


  """
  defstruct [:key, :value]

  @type t :: %{
          optional(:__struct__) => atom(),
          key: String.t(),
          value: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.Tag{
      key: params["key"],
      value: params["value"]
    }
  end

  @doc false
  def to_param(tag) do
    %{
      "key" => tag.key,
      "value" => tag.value
    }
  end
end