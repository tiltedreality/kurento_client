defmodule Kurento.Struct.RTCCertificateStats do
  @moduledoc """


  Stats > RTCStats > RTCCertificateStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `fingerprint:` `String` - Only use the fingerprint value as defined in Section 5 of [RFC4572].
  * `fingerprintAlgorithm:` `String` - For instance, 'sha-256'.
  * `base64Certificate:` `String` - For example, DER-encoded, base-64 representation of a certifiate.
  * `issuerCertificateId:` `String` - 


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :fingerprint,
    :fingerprint_algorithm,
    :base64_certificate,
    :issuer_certificate_id
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          fingerprint: String.t(),
          fingerprint_algorithm: String.t(),
          base64_certificate: String.t(),
          issuer_certificate_id: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCCertificateStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      fingerprint: params["fingerprint"],
      fingerprint_algorithm: params["fingerprintAlgorithm"],
      base64_certificate: params["base64Certificate"],
      issuer_certificate_id: params["issuerCertificateId"]
    }
  end

  @doc false
  def to_param(rtc_certificate_stats) do
    %{
      "id" => rtc_certificate_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_certificate_stats.type),
      "timestamp" => rtc_certificate_stats.timestamp,
      "timestampMillis" => rtc_certificate_stats.timestamp_millis,
      "fingerprint" => rtc_certificate_stats.fingerprint,
      "fingerprintAlgorithm" => rtc_certificate_stats.fingerprint_algorithm,
      "base64Certificate" => rtc_certificate_stats.base64_certificate,
      "issuerCertificateId" => rtc_certificate_stats.issuer_certificate_id
    }
  end
end