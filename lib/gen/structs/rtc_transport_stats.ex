defmodule Kurento.Struct.RTCTransportStats do
  @moduledoc """
  Statistics related to RTC data channels.

  Stats > RTCStats > RTCTransportStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `bytesSent:` `int64` - Represents the total number of payload bytes sent on this PeerConnection, i.e., not including headers or padding.
  * `bytesReceived:` `int64` - Represents the total number of bytes received on this PeerConnection, i.e., not including headers or padding.
  * `rtcpTransportStatsId:` `String` - If RTP and RTCP are not multiplexed, this is the id of the transport that gives stats for the RTCP component, and this record has only the RTP component stats.
  * `activeConnection:` `boolean` - Set to true when transport is active.
  * `selectedCandidatePairId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCIceCandidatePairStats associated with this transport.
  * `localCertificateId:` `String` - For components where DTLS is negotiated, give local certificate.
  * `remoteCertificateId:` `String` - For components where DTLS is negotiated, give remote certificate.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :bytes_sent,
    :bytes_received,
    :rtcp_transport_stats_id,
    :active_connection,
    :selected_candidate_pair_id,
    :local_certificate_id,
    :remote_certificate_id
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          bytes_sent: integer(),
          bytes_received: integer(),
          rtcp_transport_stats_id: String.t(),
          active_connection: boolean(),
          selected_candidate_pair_id: String.t(),
          local_certificate_id: String.t(),
          remote_certificate_id: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCTransportStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      bytes_sent: params["bytesSent"],
      bytes_received: params["bytesReceived"],
      rtcp_transport_stats_id: params["rtcpTransportStatsId"],
      active_connection: params["activeConnection"],
      selected_candidate_pair_id: params["selectedCandidatePairId"],
      local_certificate_id: params["localCertificateId"],
      remote_certificate_id: params["remoteCertificateId"]
    }
  end

  @doc false
  def to_param(rtc_transport_stats) do
    %{
      "id" => rtc_transport_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_transport_stats.type),
      "timestamp" => rtc_transport_stats.timestamp,
      "timestampMillis" => rtc_transport_stats.timestamp_millis,
      "bytesSent" => rtc_transport_stats.bytes_sent,
      "bytesReceived" => rtc_transport_stats.bytes_received,
      "rtcpTransportStatsId" => rtc_transport_stats.rtcp_transport_stats_id,
      "activeConnection" => rtc_transport_stats.active_connection,
      "selectedCandidatePairId" => rtc_transport_stats.selected_candidate_pair_id,
      "localCertificateId" => rtc_transport_stats.local_certificate_id,
      "remoteCertificateId" => rtc_transport_stats.remote_certificate_id
    }
  end
end