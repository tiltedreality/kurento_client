defmodule Kurento.Struct.SDES do
  @moduledoc """
  Security Descriptions for Media Streams

  SDES

  * `key:` `String` - 

  Master key and salt (plain text)

  This field provides the the cryptographic master key appended with the master salt, in plain text format. This allows to provide a key that is composed of readable ASCII characters. 

  The expected length of the key (as provided to this parameter) is determined by the crypto-suite for which the key applies (30 characters for AES_CM_128, 46 characters for AES_CM_256). If the length does not match the expected value, the key will be considered invalid. 

  If no key is provided, a random one will be generated using the `getrandom` system call.
  * `keyBase64:` `String` - 

  Master key and salt (base64 encoded)

  This field provides the cryptographic master key appended with the master salt, encoded in base64. This allows to provide a binary key that is not limited to the ASCII character set. 

  The expected length of the key (after being decoded from base64) is determined by the crypto-suite for which the key applies (30 bytes for AES_CM_128, 46 bytes for AES_CM_256). If the length does not match the expected value, the key will be considered invalid. 

  If no key is provided, a random one will be generated using the `getrandom` system call.
  * `crypto:` `Kurento.Enum.CryptoSuite` - Selects the cryptographic suite to be used. For available values, please see the CryptoSuite enum.


  """
  defstruct [:key, :key_base64, :crypto]

  @type t :: %{
          optional(:__struct__) => atom(),
          key: String.t(),
          key_base64: String.t(),
          crypto: Kurento.Enum.CryptoSuite.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.SDES{
      key: params["key"],
      key_base64: params["keyBase64"],
      crypto: Kurento.Enum.CryptoSuite.from_param(client, params["crypto"])
    }
  end

  @doc false
  def to_param(sdes) do
    %{
      "key" => sdes.key,
      "keyBase64" => sdes.key_base64,
      "crypto" => Kurento.Enum.CryptoSuite.to_param(sdes.crypto)
    }
  end
end