defmodule Kurento.Struct.RTCMediaStreamStats do
  @moduledoc """
  Statistics related to the media stream.

  Stats > RTCStats > RTCMediaStreamStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `streamIdentifier:` `String` - Stream identifier.
  * `trackIds:` `String[]` - This is the id of the stats object, not the track.id.


  """
  defstruct [:id, :type, :timestamp, :timestamp_millis, :stream_identifier, :track_ids]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          stream_identifier: String.t(),
          track_ids: [String.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCMediaStreamStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      stream_identifier: params["streamIdentifier"],
      track_ids: params["trackIds"]
    }
  end

  @doc false
  def to_param(rtc_media_stream_stats) do
    %{
      "id" => rtc_media_stream_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_media_stream_stats.type),
      "timestamp" => rtc_media_stream_stats.timestamp,
      "timestampMillis" => rtc_media_stream_stats.timestamp_millis,
      "streamIdentifier" => rtc_media_stream_stats.stream_identifier,
      "trackIds" => rtc_media_stream_stats.track_ids
    }
  end
end