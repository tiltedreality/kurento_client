defmodule Kurento.Struct.OfferOptions do
  @moduledoc """
  Used to customize the offer created by `SdpEndpoint.generateOffer`.

  OfferOptions

  * `offerToReceiveAudio:` `boolean` - Whether or not to offer to the remote peer the opportunity to send audio.
  * `offerToReceiveVideo:` `boolean` - Whether or not to offer to the remote peer the opportunity to send video.


  """
  defstruct [:offer_to_receive_audio, :offer_to_receive_video]

  @type t :: %{
          optional(:__struct__) => atom(),
          offer_to_receive_audio: boolean(),
          offer_to_receive_video: boolean()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.OfferOptions{
      offer_to_receive_audio: params["offerToReceiveAudio"],
      offer_to_receive_video: params["offerToReceiveVideo"]
    }
  end

  @doc false
  def to_param(offer_options) do
    %{
      "offerToReceiveAudio" => offer_options.offer_to_receive_audio,
      "offerToReceiveVideo" => offer_options.offer_to_receive_video
    }
  end
end