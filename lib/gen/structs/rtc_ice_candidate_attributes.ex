defmodule Kurento.Struct.RTCIceCandidateAttributes do
  @moduledoc """


  Stats > RTCStats > RTCIceCandidateAttributes

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `ipAddress:` `String` - It is the IP address of the candidate, allowing for IPv4 addresses, IPv6 addresses, and fully qualified domain names (FQDNs).
  * `portNumber:` `int64` - It is the port number of the candidate.
  * `transport:` `String` - Valid values for transport is one of udp and tcp. Based on the 'transport' defined in [RFC5245] section 15.1.
  * `candidateType:` `Kurento.Enum.RTCStatsIceCandidateType` - The enumeration RTCStatsIceCandidateType is based on the cand-type defined in [RFC5245] section 15.1.
  * `priority:` `int64` - Represents the priority of the candidate
  * `addressSourceUrl:` `String` - The URL of the TURN or STUN server indicated in the RTCIceServers that translated this IP address.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :ip_address,
    :port_number,
    :transport,
    :candidate_type,
    :priority,
    :address_source_url
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          ip_address: String.t(),
          port_number: integer(),
          transport: String.t(),
          candidate_type: Kurento.Enum.RTCStatsIceCandidateType.t(),
          priority: integer(),
          address_source_url: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCIceCandidateAttributes{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      ip_address: params["ipAddress"],
      port_number: params["portNumber"],
      transport: params["transport"],
      candidate_type:
        Kurento.Enum.RTCStatsIceCandidateType.from_param(client, params["candidateType"]),
      priority: params["priority"],
      address_source_url: params["addressSourceUrl"]
    }
  end

  @doc false
  def to_param(rtc_ice_candidate_attributes) do
    %{
      "id" => rtc_ice_candidate_attributes.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_ice_candidate_attributes.type),
      "timestamp" => rtc_ice_candidate_attributes.timestamp,
      "timestampMillis" => rtc_ice_candidate_attributes.timestamp_millis,
      "ipAddress" => rtc_ice_candidate_attributes.ip_address,
      "portNumber" => rtc_ice_candidate_attributes.port_number,
      "transport" => rtc_ice_candidate_attributes.transport,
      "candidateType" =>
        Kurento.Enum.RTCStatsIceCandidateType.to_param(
          rtc_ice_candidate_attributes.candidate_type
        ),
      "priority" => rtc_ice_candidate_attributes.priority,
      "addressSourceUrl" => rtc_ice_candidate_attributes.address_source_url
    }
  end
end