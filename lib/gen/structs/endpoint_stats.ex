defmodule Kurento.Struct.EndpointStats do
  @moduledoc """
  A dictionary that represents the stats gathered in the endpoint element.

  Stats > ElementStats > EndpointStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `inputAudioLatency:` `double` - @deprecated Audio average measured on the sink pad in nano seconds
  * `inputVideoLatency:` `double` - @deprecated Video average measured on the sink pad in nano seconds
  * `inputLatency:` `MediaLatencyStat[]` - The average time that buffers take to get on the input pads of this element in nano seconds


  * `audioE2ELatency:` `double` - @deprecated End-to-end audio latency measured in nano seconds
  * `videoE2ELatency:` `double` - @deprecated End-to-end video latency measured in nano seconds
  * `E2ELatency:` `MediaLatencyStat[]` - The average end to end latency for each media stream measured in nano seconds


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :input_audio_latency,
    :input_video_latency,
    :input_latency,
    :audio_e2_e_latency,
    :video_e2_e_latency,
    :e2_e_latency
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          input_audio_latency: float(),
          input_video_latency: float(),
          input_latency: [Kurento.Struct.MediaLatencyStat.t()],
          audio_e2_e_latency: float(),
          video_e2_e_latency: float(),
          e2_e_latency: [Kurento.Struct.MediaLatencyStat.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.EndpointStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      input_audio_latency: params["inputAudioLatency"],
      input_video_latency: params["inputVideoLatency"],
      input_latency:
        Enum.map(params["inputLatency"], &Kurento.Struct.MediaLatencyStat.from_param(client, &1)),
      audio_e2_e_latency: params["audioE2ELatency"],
      video_e2_e_latency: params["videoE2ELatency"],
      e2_e_latency:
        Enum.map(params["E2ELatency"], &Kurento.Struct.MediaLatencyStat.from_param(client, &1))
    }
  end

  @doc false
  def to_param(endpoint_stats) do
    %{
      "id" => endpoint_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(endpoint_stats.type),
      "timestamp" => endpoint_stats.timestamp,
      "timestampMillis" => endpoint_stats.timestamp_millis,
      "inputAudioLatency" => endpoint_stats.input_audio_latency,
      "inputVideoLatency" => endpoint_stats.input_video_latency,
      "inputLatency" =>
        Enum.map(endpoint_stats.input_latency, &Kurento.Struct.MediaLatencyStat.to_param(&1)),
      "audioE2ELatency" => endpoint_stats.audio_e2_e_latency,
      "videoE2ELatency" => endpoint_stats.video_e2_e_latency,
      "E2ELatency" =>
        Enum.map(endpoint_stats.e2_e_latency, &Kurento.Struct.MediaLatencyStat.to_param(&1))
    }
  end
end