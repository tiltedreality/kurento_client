defmodule Kurento.Struct.RTCCodec do
  @moduledoc """
  RTC codec statistics

  Stats > RTCStats > RTCCodec

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `payloadType:` `int64` - Payload type as used in RTP encoding.
  * `codec:` `String` - e.g., video/vp8 or equivalent.
  * `clockRate:` `int64` - Represents the media sampling rate.
  * `channels:` `int64` - Use 2 for stereo, missing for most other cases.
  * `parameters:` `String` - From the SDP description line.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :payload_type,
    :codec,
    :clock_rate,
    :channels,
    :parameters
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          payload_type: integer(),
          codec: String.t(),
          clock_rate: integer(),
          channels: integer(),
          parameters: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCCodec{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      payload_type: params["payloadType"],
      codec: params["codec"],
      clock_rate: params["clockRate"],
      channels: params["channels"],
      parameters: params["parameters"]
    }
  end

  @doc false
  def to_param(rtc_codec) do
    %{
      "id" => rtc_codec.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_codec.type),
      "timestamp" => rtc_codec.timestamp,
      "timestampMillis" => rtc_codec.timestamp_millis,
      "payloadType" => rtc_codec.payload_type,
      "codec" => rtc_codec.codec,
      "clockRate" => rtc_codec.clock_rate,
      "channels" => rtc_codec.channels,
      "parameters" => rtc_codec.parameters
    }
  end
end