defmodule Kurento.Struct.RTCPeerConnectionStats do
  @moduledoc """
  Statistics related to the peer connection.

  Stats > RTCStats > RTCPeerConnectionStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `dataChannelsOpened:` `int64` - Represents the number of unique datachannels opened.
  * `dataChannelsClosed:` `int64` - Represents the number of unique datachannels closed.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :data_channels_opened,
    :data_channels_closed
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          data_channels_opened: integer(),
          data_channels_closed: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCPeerConnectionStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      data_channels_opened: params["dataChannelsOpened"],
      data_channels_closed: params["dataChannelsClosed"]
    }
  end

  @doc false
  def to_param(rtc_peer_connection_stats) do
    %{
      "id" => rtc_peer_connection_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtc_peer_connection_stats.type),
      "timestamp" => rtc_peer_connection_stats.timestamp,
      "timestampMillis" => rtc_peer_connection_stats.timestamp_millis,
      "dataChannelsOpened" => rtc_peer_connection_stats.data_channels_opened,
      "dataChannelsClosed" => rtc_peer_connection_stats.data_channels_closed
    }
  end
end