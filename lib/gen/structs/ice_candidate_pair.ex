defmodule Kurento.Struct.IceCandidatePair do
  @moduledoc """
  The ICE candidate pair used by the ice library, for a certain stream.

  IceCandidatePair

  * `streamID:` `String` - Stream ID of the ice connection
  * `componentID:` `int` - Component ID of the ice connection
  * `localCandidate:` `String` - The local candidate used by the ice library.
  * `remoteCandidate:` `String` - The remote candidate used by the ice library.


  """
  defstruct [:stream_id, :component_id, :local_candidate, :remote_candidate]

  @type t :: %{
          optional(:__struct__) => atom(),
          stream_id: String.t(),
          component_id: integer(),
          local_candidate: String.t(),
          remote_candidate: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.IceCandidatePair{
      stream_id: params["streamID"],
      component_id: params["componentID"],
      local_candidate: params["localCandidate"],
      remote_candidate: params["remoteCandidate"]
    }
  end

  @doc false
  def to_param(ice_candidate_pair) do
    %{
      "streamID" => ice_candidate_pair.stream_id,
      "componentID" => ice_candidate_pair.component_id,
      "localCandidate" => ice_candidate_pair.local_candidate,
      "remoteCandidate" => ice_candidate_pair.remote_candidate
    }
  end
end