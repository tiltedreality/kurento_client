defmodule Kurento.Struct.RTCRTPStreamStats do
  @moduledoc """
  Statistics for the RTP stream

  Stats > RTCStats > RTCRTPStreamStats

  * `id:` `String` - A unique id that is associated with the object that was inspected to produce this Stats object.
  * `type:` `Kurento.Enum.StatsType` - The type of this object.
  * `timestamp:` `double` - [DEPRECATED: Use timestampMillis] The timestamp associated with this object: Seconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).
  * `timestampMillis:` `int64` - The timestamp associated with this event: Milliseconds elapsed since the UNIX Epoch (Jan 1, 1970, UTC).


  * `ssrc:` `String` - The synchronized source SSRC
  * `associateStatsId:` `String` - The associateStatsId is used for looking up the corresponding (local/remote) RTCStats object for a given SSRC.
  * `isRemote:` `boolean` - false indicates that the statistics are measured locally, while true indicates that the measurements were done at the remote endpoint and reported in an RTCP RR/XR.
  * `mediaTrackId:` `String` - Track identifier.
  * `transportId:` `String` - It is a unique identifier that is associated to the object that was inspected to produce the RTCTransportStats associated with this RTP stream.
  * `codecId:` `String` - The codec identifier
  * `firCount:` `int64` - Count the total number of Full Intra Request (FIR) packets received by the sender. This metric is only valid for video and is sent by receiver.
  * `pliCount:` `int64` - Count the total number of Packet Loss Indication (PLI) packets received by the sender and is sent by receiver.
  * `nackCount:` `int64` - Count the total number of Negative ACKnowledgement (NACK) packets received by the sender and is sent by receiver.
  * `sliCount:` `int64` - Count the total number of Slice Loss Indication (SLI) packets received by the sender. This metric is only valid for video and is sent by receiver.
  * `remb:` `int64` - The Receiver Estimated Maximum Bitrate (REMB). This metric is only valid for video.
  * `packetsLost:` `int64` - Total number of RTP packets lost for this SSRC.
  * `fractionLost:` `double` - The fraction packet loss reported for this SSRC.


  """
  defstruct [
    :id,
    :type,
    :timestamp,
    :timestamp_millis,
    :ssrc,
    :associate_stats_id,
    :is_remote,
    :media_track_id,
    :transport_id,
    :codec_id,
    :fir_count,
    :pli_count,
    :nack_count,
    :sli_count,
    :remb,
    :packets_lost,
    :fraction_lost
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          id: String.t(),
          type: Kurento.Enum.StatsType.t(),
          timestamp: float(),
          timestamp_millis: integer(),
          ssrc: String.t(),
          associate_stats_id: String.t(),
          is_remote: boolean(),
          media_track_id: String.t(),
          transport_id: String.t(),
          codec_id: String.t(),
          fir_count: integer(),
          pli_count: integer(),
          nack_count: integer(),
          sli_count: integer(),
          remb: integer(),
          packets_lost: integer(),
          fraction_lost: float()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RTCRTPStreamStats{
      id: params["id"],
      type: Kurento.Enum.StatsType.from_param(client, params["type"]),
      timestamp: params["timestamp"],
      timestamp_millis: params["timestampMillis"],
      ssrc: params["ssrc"],
      associate_stats_id: params["associateStatsId"],
      is_remote: params["isRemote"],
      media_track_id: params["mediaTrackId"],
      transport_id: params["transportId"],
      codec_id: params["codecId"],
      fir_count: params["firCount"],
      pli_count: params["pliCount"],
      nack_count: params["nackCount"],
      sli_count: params["sliCount"],
      remb: params["remb"],
      packets_lost: params["packetsLost"],
      fraction_lost: params["fractionLost"]
    }
  end

  @doc false
  def to_param(rtcrtp_stream_stats) do
    %{
      "id" => rtcrtp_stream_stats.id,
      "type" => Kurento.Enum.StatsType.to_param(rtcrtp_stream_stats.type),
      "timestamp" => rtcrtp_stream_stats.timestamp,
      "timestampMillis" => rtcrtp_stream_stats.timestamp_millis,
      "ssrc" => rtcrtp_stream_stats.ssrc,
      "associateStatsId" => rtcrtp_stream_stats.associate_stats_id,
      "isRemote" => rtcrtp_stream_stats.is_remote,
      "mediaTrackId" => rtcrtp_stream_stats.media_track_id,
      "transportId" => rtcrtp_stream_stats.transport_id,
      "codecId" => rtcrtp_stream_stats.codec_id,
      "firCount" => rtcrtp_stream_stats.fir_count,
      "pliCount" => rtcrtp_stream_stats.pli_count,
      "nackCount" => rtcrtp_stream_stats.nack_count,
      "sliCount" => rtcrtp_stream_stats.sli_count,
      "remb" => rtcrtp_stream_stats.remb,
      "packetsLost" => rtcrtp_stream_stats.packets_lost,
      "fractionLost" => rtcrtp_stream_stats.fraction_lost
    }
  end
end