defmodule Kurento.Struct.ElementConnectionData do
  @moduledoc """


  ElementConnectionData

  * `source:` `Kurento.Remote.MediaElement` - The source element in the connection
  * `sink:` `Kurento.Remote.MediaElement` - The sink element in the connection
  * `type:` `Kurento.Enum.MediaType` - MediaType of the connection
  * `sourceDescription:` `String` - Description of source media. Could be emty.
  * `sinkDescription:` `String` - Description of sink media. Could be emty.


  """
  defstruct [:source, :sink, :type, :source_description, :sink_description]

  @type t :: %{
          optional(:__struct__) => atom(),
          source: Kurento.Remote.MediaElement.t(),
          sink: Kurento.Remote.MediaElement.t(),
          type: Kurento.Enum.MediaType.t(),
          source_description: String.t(),
          sink_description: String.t()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.ElementConnectionData{
      source: Kurento.Remote.MediaElement.from_param(client, params["source"]),
      sink: Kurento.Remote.MediaElement.from_param(client, params["sink"]),
      type: Kurento.Enum.MediaType.from_param(client, params["type"]),
      source_description: params["sourceDescription"],
      sink_description: params["sinkDescription"]
    }
  end

  @doc false
  def to_param(element_connection_data) do
    %{
      "source" => Kurento.Remote.MediaElement.to_param(element_connection_data.source),
      "sink" => Kurento.Remote.MediaElement.to_param(element_connection_data.sink),
      "type" => Kurento.Enum.MediaType.to_param(element_connection_data.type),
      "sourceDescription" => element_connection_data.source_description,
      "sinkDescription" => element_connection_data.sink_description
    }
  end
end