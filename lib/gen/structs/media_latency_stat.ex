defmodule Kurento.Struct.MediaLatencyStat do
  @moduledoc """
  A dictionary that represents the stats gathered.

  MediaLatencyStat

  * `name:` `String` - The identifier of the media stream
  * `type:` `Kurento.Enum.MediaType` - Type of media stream
  * `avg:` `double` - The average time that buffers take to get on the input pad of this element


  """
  defstruct [:name, :type, :avg]

  @type t :: %{
          optional(:__struct__) => atom(),
          name: String.t(),
          type: Kurento.Enum.MediaType.t(),
          avg: float()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.MediaLatencyStat{
      name: params["name"],
      type: Kurento.Enum.MediaType.from_param(client, params["type"]),
      avg: params["avg"]
    }
  end

  @doc false
  def to_param(media_latency_stat) do
    %{
      "name" => media_latency_stat.name,
      "type" => Kurento.Enum.MediaType.to_param(media_latency_stat.type),
      "avg" => media_latency_stat.avg
    }
  end
end