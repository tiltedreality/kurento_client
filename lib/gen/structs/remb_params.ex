defmodule Kurento.Struct.RembParams do
  @moduledoc """
  Defines values for parameters of congestion control

  RembParams

  * `packetsRecvIntervalTop:` `int` - Size of the RTP packets history to smooth fraction-lost. Units: num of packets
  * `exponentialFactor:` `float` - Factor used to increase exponentially the next REMB when it is below the threshold. REMB[i+1] = REMB[i] * (1 + exponentialFactor)
  * `linealFactorMin:` `int` - Set the min of the factor used to increase linearly the next REMB when it is over the threshold. Units: bps (bits per second). REMB[i+1] = REMB[i] + MIN (linealFactorMin, linealFactor)
  * `linealFactorGrade:` `float` - Determine the value of the next linearFactor based on the threshold and the current REMB. Taking into account that the frequency of updating is 500ms, the default value makes that the last REMB is reached in 60secs. linealFactor = (REMB - TH) / linealFactorGrade
  * `decrementFactor:` `float` - Determine how much is decreased the current REMB when too losses are detected. REMB[i+1] = REMB[i] * decrementFactor
  * `thresholdFactor:` `float` - Determine the next threshold (TH) when too losses are detected. TH[i+1] = REMB[i] * thresholdFactor
  * `upLosses:` `int` - Max fraction-lost to no determine too losses. This value is the denominator of the fraction N/256, so the default value is about 4% of losses (12/256)
  * `rembOnConnect:` `int` - Initial local REMB bandwidth estimation that gets propagated when a new endpoint is connected. 

  The REMB congestion control algorithm works by gradually increasing the output video bitrate, until the available bandwidth is fully used or the maximum send bitrate has been reached. This is a slow, progressive change, which starts at 300 kbps by default. You can change the default starting point of REMB estimations, by setting this parameter. 

  **WARNING**: If you set this parameter to a high value that is *higher than the network capacity*, then all endpoints will start already in a congested state, providing very bad video quality until the congestion control algorithm is able to recover from the situation. Network congestion is very unpredictable, so be careful when changing this parameter; for most use cases it is safer to just start with a low initial value and allow the REMB algorithm to raise until the optimum bitrate is reached. 

  * Unit: bps (bits per second).
  * Default: 300000 (300 kbps).


  """
  defstruct [
    :packets_recv_interval_top,
    :exponential_factor,
    :lineal_factor_min,
    :lineal_factor_grade,
    :decrement_factor,
    :threshold_factor,
    :up_losses,
    :remb_on_connect
  ]

  @type t :: %{
          optional(:__struct__) => atom(),
          packets_recv_interval_top: integer(),
          exponential_factor: float(),
          lineal_factor_min: integer(),
          lineal_factor_grade: float(),
          decrement_factor: float(),
          threshold_factor: float(),
          up_losses: integer(),
          remb_on_connect: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.RembParams{
      packets_recv_interval_top: params["packetsRecvIntervalTop"],
      exponential_factor: params["exponentialFactor"],
      lineal_factor_min: params["linealFactorMin"],
      lineal_factor_grade: params["linealFactorGrade"],
      decrement_factor: params["decrementFactor"],
      threshold_factor: params["thresholdFactor"],
      up_losses: params["upLosses"],
      remb_on_connect: params["rembOnConnect"]
    }
  end

  @doc false
  def to_param(remb_params) do
    %{
      "packetsRecvIntervalTop" => remb_params.packets_recv_interval_top,
      "exponentialFactor" => remb_params.exponential_factor,
      "linealFactorMin" => remb_params.lineal_factor_min,
      "linealFactorGrade" => remb_params.lineal_factor_grade,
      "decrementFactor" => remb_params.decrement_factor,
      "thresholdFactor" => remb_params.threshold_factor,
      "upLosses" => remb_params.up_losses,
      "rembOnConnect" => remb_params.remb_on_connect
    }
  end
end