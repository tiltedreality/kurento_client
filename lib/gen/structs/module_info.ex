defmodule Kurento.Struct.ModuleInfo do
  @moduledoc """
  Description of a loaded modules

  ModuleInfo

  * `version:` `String` - Module version
  * `name:` `String` - Module name
  * `generationTime:` `String` - Time that this module was generated
  * `factories:` `String[]` - Module available factories


  """
  defstruct [:version, :name, :generation_time, :factories]

  @type t :: %{
          optional(:__struct__) => atom(),
          version: String.t(),
          name: String.t(),
          generation_time: String.t(),
          factories: [String.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.ModuleInfo{
      version: params["version"],
      name: params["name"],
      generation_time: params["generationTime"],
      factories: params["factories"]
    }
  end

  @doc false
  def to_param(module_info) do
    %{
      "version" => module_info.version,
      "name" => module_info.name,
      "generationTime" => module_info.generation_time,
      "factories" => module_info.factories
    }
  end
end