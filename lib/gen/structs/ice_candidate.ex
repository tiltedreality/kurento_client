defmodule Kurento.Struct.IceCandidate do
  @moduledoc """
  IceCandidate representation based on `RTCIceCandidate` interface. @see https://www.w3.org/TR/2018/CR-webrtc-20180927/#rtcicecandidate-interface

  IceCandidate

  * `candidate:` `String` - The candidate-attribute as defined in section 15.1 of ICE (rfc5245).
  * `sdpMid:` `String` - If present, this contains the identifier of the 'media stream identification'.
  * `sdpMLineIndex:` `int` - The index (starting at zero) of the m-line in the SDP this candidate is associated with.


  """
  defstruct [:candidate, :sdp_mid, :sdp_m_line_index]

  @type t :: %{
          optional(:__struct__) => atom(),
          candidate: String.t(),
          sdp_mid: String.t(),
          sdp_m_line_index: integer()
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.IceCandidate{
      candidate: params["candidate"],
      sdp_mid: params["sdpMid"],
      sdp_m_line_index: params["sdpMLineIndex"]
    }
  end

  @doc false
  def to_param(ice_candidate) do
    %{
      "candidate" => ice_candidate.candidate,
      "sdpMid" => ice_candidate.sdp_mid,
      "sdpMLineIndex" => ice_candidate.sdp_m_line_index
    }
  end
end