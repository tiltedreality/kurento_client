defmodule Kurento.Struct.ServerInfo do
  @moduledoc """
  Description of the mediaserver

  ServerInfo

  * `version:` `String` - MediaServer version
  * `modules:` `ModuleInfo[]` - Descriptor of all modules loaded by the server
  * `type:` `Kurento.Enum.ServerType` - Describes the type of mediaserver
  * `capabilities:` `String[]` - Describes the capabilities that this server supports


  """
  defstruct [:version, :modules, :type, :capabilities]

  @type t :: %{
          optional(:__struct__) => atom(),
          version: String.t(),
          modules: [Kurento.Struct.ModuleInfo.t()],
          type: Kurento.Enum.ServerType.t(),
          capabilities: [String.t()]
        }

  @doc false
  def from_param(client, params) do
    _ = client

    %Kurento.Struct.ServerInfo{
      version: params["version"],
      modules: Enum.map(params["modules"], &Kurento.Struct.ModuleInfo.from_param(client, &1)),
      type: Kurento.Enum.ServerType.from_param(client, params["type"]),
      capabilities: params["capabilities"]
    }
  end

  @doc false
  def to_param(server_info) do
    %{
      "version" => server_info.version,
      "modules" => Enum.map(server_info.modules, &Kurento.Struct.ModuleInfo.to_param(&1)),
      "type" => Kurento.Enum.ServerType.to_param(server_info.type),
      "capabilities" => server_info.capabilities
    }
  end
end