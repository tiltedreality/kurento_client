defmodule Kurento.CallError do
  @moduledoc """
  Error returned by the Kurento Server.
  `code` is the integer error code
  `message` is the human readable `String` message
  """

  @type t :: %Kurento.CallError{code: integer(), message: String.t()}

  defstruct [:code, :message]

  def from_map(%{"code" => code, "message" => message}) do
    %Kurento.CallError{code: code, message: message}
  end
end
