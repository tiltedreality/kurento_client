defmodule Kurento.RPCSocket do
  @moduledoc false

  require Logger

  alias Kurento.RPCRequest

  defmodule Subscription do
    @moduledoc false
    @enforce_keys [:id]
    defstruct [:id, pids: MapSet.new()]

    @type t :: %Subscription{id: String.t(), pids: MapSet.t(pid())}

    def get_subscriptions(table) do
      case List.first(:ets.lookup(table, :subscriptions)) do
        {_, subscriptions} -> subscriptions
        _ -> %{}
      end
    end

    def set_subscriptions(table, subscriptions) do
      :ets.insert(table, {:subscriptions, subscriptions})
    end

    @spec get_subscription(:ets.tid(), {String.t(), String.t()}) :: Subscription.t() | nil
    def get_subscription(table, event) do
      subs = get_subscriptions(table)
      subs[event]
    end

    @spec add_subscription(:ets.tid(), {String.t(), String.t()}, Subscription.t()) :: true
    def add_subscription(table, event, subscription) do
      subs = table |> get_subscriptions() |> Map.put(event, subscription)
      set_subscriptions(table, subs)
    end

    @spec add_subscriber(:ets.tid(), {String.t(), String.t()}, pid()) :: true
    def add_subscriber(table, event, pid) do
      Process.monitor(pid)
      subscription = get_subscription(table, event)
      pids = subscription.pids
      new_subscription = %Subscription{subscription | pids: MapSet.put(pids, pid)}
      add_subscription(table, event, new_subscription)
    end

    @spec remove_subscriber(:ets.tid(), {String.t(), String.t()}, pid()) :: true
    def remove_subscriber(table, event, pid) do
      subscription = get_subscription(table, event)

      if subscription do
        pids = subscription.pids
        new_subscription = %Subscription{subscription | pids: MapSet.delete(pids, pid)}
        add_subscription(table, event, new_subscription)
      else
        true
      end
    end

    @spec remove_object_subscriptions(:ets.tid(), String.t()) :: :ok
    def remove_object_subscriptions(table, obj_id) do
      subs = get_subscriptions(table)

      events =
        subs
        |> Enum.filter(fn {event, _sub} ->
          {id, _type} = event
          String.contains?(id, obj_id)
        end)
        |> Enum.map(fn {event, _sub} ->
          event
        end)

      new_subs =
        Enum.reduce(events, subs, fn event, subs ->
          Map.delete(subs, event)
        end)

      set_subscriptions(table, new_subs)
      :ok
    end

    def remove_pid_subscriptions(table, pid) do
      subs =
        table
        |> get_subscriptions
        |> Enum.reduce(%{}, fn {event, sub}, acc ->
          new_sub = %{sub | pids: MapSet.delete(sub.pids, pid)}
          Map.put(acc, event, new_sub)
        end)

      empty =
        subs
        |> Enum.filter(fn {_event, sub} ->
          Enum.empty?(sub.pids)
        end)

      new_subs =
        Enum.reduce(empty, subs, fn {event, _obj}, subs ->
          Map.delete(subs, event)
        end)

      set_subscriptions(table, new_subs)
      empty
    end

    def remove_subscription(table, event) do
      subs = table |> get_subscriptions() |> Map.delete(event)

      set_subscriptions(table, subs)
    end

    @spec subscribed?(:ets.tid(), {String.t(), String.t()}) :: boolean()
    def subscribed?(table, event) do
      get_subscription(table, event) !== nil
    end
  end

  defmodule Pending do
    @moduledoc false
    defstruct [:from, type: :invoke, data: {}]

    @type t :: %Pending{
            from: GenServer.from() | nil,
            type: :invoke | :subscribe | :unsubscribe | :connect | :release,
            data: tuple()
          }
  end

  defmodule State do
    @moduledoc false
    @enforce_keys [:conn, :table, :client]
    defstruct [:conn, :table, :client, :spy, pending: %{}]

    @type t :: %State{
            conn: pid(),
            table: :ets.tid(),
            spy: pid() | nil,
            pending: %{optional(String.t()) => Pending.t()},
            client: pid()
          }
  end

  use GenServer

  defp send_spy(msg, spy) do
    if spy do
      send(spy, msg)
    end
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, opts)
  end

  @spec init(keyword) :: {:ok, State.t()}
  @impl GenServer
  def init(opts) do
    table = opts[:table]
    client = opts[:client]

    host = opts |> Keyword.get(:host, "localhost") |> to_charlist()
    port = Keyword.get(opts, :port, 8888)
    path = opts |> Keyword.get(:path, "/kurento") |> to_charlist()

    Logger.info("Starting Kurento RPC Socket")
    {:ok, conn} = :gun.open(host, port)
    {:ok, _} = :gun.await_up(conn)
    Logger.debug("Kurento HTTP connection established")
    :gun.ws_upgrade(conn, path)

    receive do
      {:gun_upgrade, _conn, _stream, _proto, _headers} -> :ok
    end

    Logger.debug("Kurento websocket established")
    session_id = get_session_id(table)
    {:ok, _} = connect(conn, session_id)

    {:ok, decoded} =
      receive do
        {:gun_ws, _conn, _ref, {:text, msg}} -> Jason.decode(msg)
      end

    got_session(decoded, table)

    Process.link(conn)
    {:ok, %State{conn: conn, table: table, client: client}}
  end

  @spec get_session_id(:ets.tid()) :: String.t() | nil
  defp get_session_id(table) do
    case List.first(:ets.lookup(table, :session_id)) do
      {_, session_id} -> session_id
      _ -> nil
    end
  end

  @spec set_session_id(:ets.tid(), String.t()) :: true
  defp set_session_id(table, session_id) do
    :ets.insert(table, {:session_id, session_id})
  end

  @spec reply_call(GenServer.from(), %{}) :: :ok | nil
  defp reply_call(from, decoded) do
    case decoded do
      %{"result" => result} ->
        if from do
          GenServer.reply(from, {:ok, result})
        end

      %{"error" => error} ->
        if from do
          GenServer.reply(from, {:error, error})
        end

      _ ->
        if from do
          GenServer.reply(
            from,
            {:error, %{"code" => -1, "message" => "Malformed response from server"}}
          )
        end
    end
  end

  @spec do_rpc_call(RPCRequest.t(), pid()) :: {:ok, String.t()}
  defp do_rpc_call(request, conn) do
    id = request.id
    encoded = Jason.encode!(request)

    :ok = :gun.ws_send(conn, {:text, encoded})
    {:ok, id}
  end

  defp got_session(decoded, table) do
    case decoded do
      %{"result" => result} ->
        session_id = result["sessionId"]
        set_session_id(table, session_id)
        Logger.info("Kurento session started: #{session_id}")

      %{"error" => error} ->
        session_id = get_session_id(table)

        if session_id do
          # We failed to connect to an existing session
          Logger.error("Unable to reconnect to session #{session_id}: #{inspect(error)}", error)
          exit(error)
        else
          # We failed to establish a new session
          Logger.error("failed to establish session", error)
          exit(error)
        end
    end
  end

  defp do_subscribed(decoded, from, event, table) do
    case decoded do
      %{"result" => result} ->
        if from do
          {pid, _} = from
          sub_id = result["value"]
          sub = %Subscription{id: sub_id}
          Subscription.add_subscription(table, event, sub)
          Subscription.add_subscriber(table, event, pid)
          GenServer.reply(from, {:ok, nil})
        end

      %{"error" => error} ->
        if from do
          GenServer.reply(from, {:error, error})
        end

      _ ->
        if from do
          GenServer.reply(
            from,
            {:error, %{"code" => -1, "message" => "Malformed response from server"}}
          )
        end
    end
  end

  @spec handle_subscribe(RPCRequest.t(), GenServer.from(), State.t()) ::
          {:noreply, State.t()} | {:reply, :ok, State.t()}
  def handle_subscribe(
        request,
        {pid, _} = from,
        %{table: table, conn: conn, pending: pending} = state
      ) do
    %{type: type, object: object_id} = request.params

    event = {object_id, type}

    if Subscription.subscribed?(table, event) do
      true = Subscription.add_subscriber(table, event, pid)
      {:reply, {:ok, nil}, state}
    else
      {:ok, id} = do_rpc_call(request, conn)
      pending = Map.put(pending, id, %Pending{type: :subscribe, from: from, data: event})
      {:noreply, %State{state | pending: pending}}
    end
  end

  def handle_unsubscribe(request, pid, state) do
    %{table: table, conn: conn} = state
    %{type: type, object: object_id} = request.params
    event = {object_id, type}

    true = Subscription.remove_subscriber(table, event, pid)
    sub = Subscription.get_subscription(table, event)

    id =
      if sub && Enum.empty?(sub.pids) do
        request = %{request | params: Map.put(request.params, :subscription, sub.id)}
        {:ok, id} = do_rpc_call(request, conn)
        Subscription.remove_subscription(table, event)
        id
      end

    pending =
      if id do
        Map.put(state.pending, id, %Pending{type: :unsubscribe, data: {event, sub}})
      else
        state.pending
      end

    {:reply, {:ok, nil}, %State{state | pending: pending}}
  end

  @spec handle_release(RPCRequest.t(), GenServer.from(), State.t()) :: {:noreply, State.t()}
  defp handle_release(request, from, state) do
    %{conn: conn, pending: pending} = state
    {:ok, id} = do_rpc_call(request, conn)
    %{object: object_id} = request.params

    pending = Map.put(pending, id, %Pending{from: from, type: :release, data: object_id})

    {:noreply, %State{state | pending: pending}}
  end

  @spec handle_invoke(RPCRequest.t(), GenServer.from(), State.t()) :: {:noreply, State.t()}
  def handle_invoke(request, from, %{conn: conn, pending: pending} = state) do
    {:ok, id} = do_rpc_call(request, conn)

    pending = Map.put(pending, id, %Pending{from: from})
    {:noreply, %State{state | pending: pending}}
  end

  @spec handle_method(RPCRequest.t(), GenServer.from(), State.t()) ::
          {:reply, any(), State.t()} | {:noreply, State.t()}
  defp handle_method(request, {pid, _} = from, %{table: table} = state) do
    params = Map.put(request.params, :sessionId, get_session_id(table))
    request = %Kurento.RPCRequest{request | params: params}

    case request.method do
      "subscribe" -> handle_subscribe(request, from, state)
      "unsubscribe" -> handle_unsubscribe(request, pid, state)
      "release" -> handle_release(request, from, state)
      _ -> handle_invoke(request, from, state)
    end
  end

  @spec handle_call(any(), GenServer.from(), State.t()) ::
          {:reply, any(), State.t()} | {:noreply, State.t()}
  @impl GenServer
  def handle_call(msg, from, state) do
    try do
      %{table: table} = state

      case msg do
        {:call, request} ->
          handle_method(request, from, state)

        :get_session_id ->
          {:reply, {:ok, get_session_id(table)}, state}

        :set_spy ->
          {pid, _} = from
          {:reply, :ok, %{state | spy: pid}}

        _ ->
          Logger.warn("Unhandled message #{inspect(msg)}")
          {:reply, {:error, :unknown}, state}
      end
    catch
      err ->
        Logger.error("Unhandled error: #{inspect(err)}")
        {:reply, {:error, err}, state}
    end
  end

  @spec handle_cast(any(), State.t()) :: {:noreply, State.t()}
  @impl GenServer
  def handle_cast(msg, state) do
    try do
      conn = state.conn

      case msg do
        :exit ->
          Logger.info("Kurento RPC Connection shutting down")
          :ok = :gun.shutdown(conn)
          exit(:normal)

        _ ->
          Logger.warn("Unhandled cast: #{inspect(msg)}")
      end
    catch
      err -> Logger.error("Unhandled error: #{inspect(err)}")
    end

    {:noreply, state}
  end

  defp handle_invoke_response(%{"id" => id} = decoded, %{pending: pending, table: table} = state) do
    %{type: type, from: from, data: data} = pending[id]
    pending = Map.delete(pending, id)

    case type do
      :subscribe ->
        event = data
        do_subscribed(decoded, from, event, table)
        {:noreply, %State{state | pending: pending}}

      :unsubscribe ->
        {event, sub} = data
        Logger.debug("Unsubscribed from event: #{inspect(event)}, id: #{sub.id}")
        {:noreply, %State{state | pending: pending}}

      :release ->
        obj_id = data
        Subscription.remove_object_subscriptions(table, obj_id)
        Logger.debug("Object #{obj_id} released")
        reply_call(from, decoded)
        {:noreply, %State{state | pending: pending}}

      :invoke ->
        reply_call(from, decoded)
        {:noreply, %State{state | pending: pending}}
    end
  end

  defp dispatch_event(pids, event_name, event) do
    Enum.each(pids, fn pid ->
      send(pid, {event_name, event})
    end)
  end

  defp handle_event(%{"params" => params}, %{table: table, client: client, spy: spy} = state) do
    val = params["value"]
    %{"object" => obj_id, "type" => type, "data" => data} = val
    event_key = {obj_id, type}
    event_name = Kurento.EventParser.event_name(type)
    event = Kurento.EventParser.parse_event(type, client, data)

    sub = Subscription.get_subscription(table, event_key)

    case sub do
      nil ->
        Logger.error("Receieved event I'm not subscribed to: #{inspect(val)}")
        send_spy({:unsubscribed_event, val}, spy)

      _ ->
        dispatch_event(sub.pids, event_name, event)
    end

    {:noreply, state}
  end

  defp handle_ws_message(msg, state) do
    {:ok, decoded} = Jason.decode(msg)

    case decoded do
      %{"id" => _} ->
        handle_invoke_response(decoded, state)

      %{"method" => "onEvent"} ->
        handle_event(decoded, state)
    end
  end

  @spec do_unsubscribe(keyword(Subscription.t()), State.t()) :: map()
  def do_unsubscribe(events, state) do
    session_id = get_session_id(state.table)

    Enum.reduce(events, state.pending, fn {event, subscription}, pending ->
      {obj_id, type} = event
      sub_id = subscription.id
      params = %{sessionId: session_id, object: obj_id, type: type, subscription: sub_id}
      request = RPCRequest.create("unsubscribe", params)
      {:ok, id} = do_rpc_call(request, state.conn)
      Map.put(pending, id, %Pending{type: :unsubscribe, data: {event, subscription}})
    end)
  end

  @spec handle_info(any, State.t()) :: {:noreply, State.t()}
  @impl GenServer
  def handle_info(msg, state) do
    try do
      case msg do
        {:gun_ws, _conn, _ref, {:text, recv}} ->
          handle_ws_message(recv, state)

        {:gun_error, _conn, _steam, _reason} ->
          Logger.error("Gun error: #{inspect(msg)}")
          exit(msg)

        {:gun_error, _conn, _reason} ->
          Logger.error("Gun error: #{inspect(msg)}")
          exit(msg)

        {:DOWN, _ref, :process, pid, _reason} ->
          empty = Subscription.remove_pid_subscriptions(state.table, pid)
          pending = do_unsubscribe(empty, state)
          {:noreply, %{state | pending: pending}}

        _ ->
          Logger.debug("Unhandled message: #{inspect(msg)}")
          {:noreply, state}
      end
    catch
      err ->
        Logger.error("Unhandled error: #{inspect(err)}")
        {:noreply, state}
    end
  end

  defp connect(conn, nil) do
    request = RPCRequest.create("connect")

    do_rpc_call(request, conn)
  end

  defp connect(conn, id) do
    request = RPCRequest.create("connect", %{sessionId: id})

    do_rpc_call(request, conn)
  end
end
