defmodule Kurento.App do
  use Application

  @impl Application
  def start(_type, _args) do
    # children = [
    #   {Registry, keys: :unique, name: Kurento.Registry}
    # ]

    # Supervisor.start_link(children, name: Kurento.Supervisor, strategy: :one_for_one)

    children = [
      {Registry, keys: :unique, name: Kurento.Registry}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
