defmodule Kurento.RPCRequest do
  @moduledoc false

  alias Kurento.RPCRequest

  @derive {Jason.Encoder, only: [:jsonrpc, :id, :method, :params]}
  @enforce_keys [:id, :method]
  defstruct [:id, :method, jsonrpc: "2.0", params: %{}]

  @type t :: %RPCRequest{id: String.t(), method: String.t(), jsonrpc: String.t(), params: map()}

  @spec create(String.t()) :: Kurento.RPCRequest.t()
  def create(method) do
    create(method, %{})
  end

  @spec create(String.t(), map()) :: Kurento.RPCRequest.t()
  def create(method, params) do
    %RPCRequest{id: UUID.uuid1(), method: method, params: params}
  end

  @spec encode(RPCRequest.t()) :: String.t()
  def encode(request) do
    Jason.encode!(request)
  end
end
