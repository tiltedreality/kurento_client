defmodule Kurento.Client do
  use Supervisor

  @type client :: any()

  defp via(name) do
    {:via, Registry, {Kurento.Registry, name}}
  end

  @doc """
  For starting a Kurento.Client under your own Supervisor chain.

  Because a client can lose it's connection to the server and automatically re-establish it
  for a viariety of reasons, each client has it's own supervisor responsible for
  restarting it as needed. This function returns that supervisor for use in another
  supervision chain, NOT the client itself.

  This is important because while `opts` is the same as for `start_client/1`,
  it differs in that `:client_name` has no default and MUST be specified, and
  you must retain it for using as the `client` to the various Kurento functions.
  It is recommended this be something meaningful, such as the hostname of the client
  (String names are allowable).
  """
  def start_link(opts) do
    client_name = opts[:client_name]

    if !client_name do
      raise(ArgumentError, message: ":client_name option is required")
    end

    Supervisor.start_link(__MODULE__, opts, opts)
  end

  @doc """
  Starts a client with the given `opts`
  Does not link to the current process.

  Allowable values for `opts`

  `:client_name` The name the client will be registered under.
  Can be any term, it does NOT have to follow erlang naming rules. Default: `Kurento.Client`

  `:host` The host of the Kurento server. Default: `"localhost"`

  `:port` The port of the Kurento server. Default: `8888`

  `:path` The uri of the Kurento Server. Default: `"/kurento"`

  Returns the registered name which MUST be used for future calls
  """
  def start_client(opts \\ []) do
    client_name = Keyword.get(opts, :client_name, Kurento.Client)
    opts = Keyword.put(opts, :client_name, client_name)
    {:ok, sup} = Kurento.Client.start_link(opts)
    Process.unlink(sup)
    client_name
  end

  @impl true
  @doc false
  def init(opts) do
    table = :ets.new(:kstate, [:set, :public, {:read_concurrency, true}])

    client_name = opts[:client_name]
    socket_opts = [table: table, client: client_name, name: via(client_name)]

    children = [
      {Kurento.RPCSocket, Keyword.merge(opts, socket_opts)}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  @doc """
  Gets a `Kurento.Remote.ServerManager` for this connection
  """
  @spec get_server_manager(client()) :: %Kurento.Remote.ServerManager{}
  def get_server_manager(client) do
    %Kurento.Remote.ServerManager{client: client, id: "manager_ServerManager"}
  end

  @doc """
  Pings the server, testing the connection
  """
  @spec ping(client()) :: {:error, Kurento.CallError.t()} | {:ok, String.t()}
  def ping(client) do
    request = Kurento.RPCRequest.create("ping")

    case Kurento.Client.invoke(client, request) do
      {:ok, result} -> {:ok, result["value"]}
      {:error, error} -> {:error, Kurento.CallError.from_map(error)}
    end
  end

  @doc false
  def invoke(client, request) do
    name = via(client)

    GenServer.call(name, {:call, request})
  end

  @doc """
  Release any `Kurento.Remote.*` object
  """
  def release(kobject) do
    params = %{object: Kurento.Remote.MediaObject.to_param(kobject)}
    client = kobject.client
    request = Kurento.RPCRequest.create("release", params)
    result = Kurento.Client.invoke(client, request)

    case(result) do
      {:ok, _} ->
        :ok

      {:error, err} ->
        Kurento.CallError.from_map(err)
    end
  end
end
